/*
 * TEAM 3 INDUSTRIAL EXPERIENCE PROJECT
 * KPMG Student Allocation System Created by Monash University South Africa's IE Team 3
 * 2016 (c) KPMG & Monash University
 */
package IE_Project;

import java.util.ArrayList;
import java.util.HashMap;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.DoubleProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 *
 * @author TEAM 3
 */
public class StageController extends StackPane {
    private HashMap<String, Node> stages = new HashMap<>();
    private ArrayList<Object> savedControllers = new ArrayList<>();

    public StageController() {
        super();
    }

    //Adds a screen to the stage hashmap
    public void addStage(String stageName, Node stage) {
        stages.put(stageName, stage);
    }

    //Returns the scene with the corresponding found within the hashmap
    public Node getStage(String stageName) {
        System.out.println("Hashmap:\n"+stages);
        return stages.get(stageName);
    }

    public Object getScreenControllers(int i) {
        return savedControllers.get(i);
    }

    /*Loads the corresponding fxml file, adds the given screen to the hashmap and
    finally injects the screenPane into the controller.*/
    public boolean loadStage(String stageName, String stageResource) {
        try {
            FXMLLoader resourceLoader = new FXMLLoader(getClass().getResource(stageResource));
            Parent loadScreen = (Parent) resourceLoader.load();
            ControlledStage myScreenControler = ((ControlledStage) resourceLoader.getController());
            savedControllers.add(myScreenControler);
            System.out.println("===== Stage Controller Size Updated =====" +
                               "\nNew Scene Loaded\t: " + stageName +
                               "\nTotal Scenes Loaded\t: " + savedControllers.size() +
                               "\n=========================================");

            myScreenControler.setStageParent(this);
            addStage(stageName, loadScreen);
            return true;
        }catch(Exception e) {
            System.err.println("ERROR: " + e.getMessage());
            return false;
        }
    }

    /*This method tries to display the screen that was called using its corresponding name.
    It makes sure the screen has been already loaded, if there is more than one screen to choose from 
    the new screen added to the stackpane, after which the current screen is removed. If there isn't 
    any screen being displayed, the new screen is just added to the root.*/
    public boolean setStage(final String stageName) {
        if(stages.get(stageName) != null) {
            final DoubleProperty opacity = opacityProperty();

            //Check if there is more than one screen 
            if(!getChildren().isEmpty()){
                Timeline fade = new Timeline(   new KeyFrame(Duration.ZERO, new KeyValue(opacity,1.0)),
                        new KeyFrame(new Duration(50), new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent t) {
                                //remove the currently displayed screen
                                getChildren().remove(0);
                                //add the new screen which should be displayed
                                getChildren().add(0, stages.get(stageName));
                                Timeline fadeIn = new Timeline( new KeyFrame(Duration.ZERO,
                                        new KeyValue(opacity, 0.0)),
                                        new KeyFrame(new Duration(25),
                                                new KeyValue(opacity, 1.0)));
                                fadeIn.play();
                            }
                        },
                                new KeyValue(opacity, 0.0)));
                fade.play();
            } else {
                //If no other screens are being displayed, then this screen will be displayed first 
                setOpacity(0.0);
                getChildren().add(stages.get(stageName));
                Timeline fadeIn = new Timeline( new KeyFrame(Duration.ZERO,
                        new KeyValue(opacity, 0.0)),
                        new KeyFrame(new Duration(100),
                                new KeyValue(opacity, 1.0)));
                fadeIn.play();
            }
            return true;
        } else {
            System.err.println("ERROR: SCREEN \"" + stageName + "\" HASN'T BEEN LOADED.");
            return false;
        }
    }

    //This method will remove the screen with the corresponding name from the stages hashmap
    public boolean removeStage(String stageName) {
        if(stages.remove(stageName) == null) {
            System.err.println("ERROR: SCREEN \"" + stageName + "\" DOESN'T EXIST.");
            return false;
        } else {
            return true;
        }
    }

}
