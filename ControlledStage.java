/*
 * TEAM 3 INDUSTRIAL EXPERIENCE PROJECT
 * KPMG Student Allocation System Created by Monash University South Africa's IE Team 3
 * 2016 (c) KPMG & Monash University
 */
package IE_Project;

/**
 *
 * @author TEAM 3
 */
public interface ControlledStage {
    
    //This method will allow the injection of the Parent ScreenPane
    public void setStageParent(StageController stagePage);  
}
