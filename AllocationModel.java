package IE_Project;
/*
lpsolve citation data
        ----------------------
        Description     : Open source (Mixed-Integer) Linear Programming system
        Language        : Multi-platform, pure ANSI C / POSIX source code, Lex/Yacc based parsing
        Official name   : lp_solve (alternatively lpsolve)
        Release data    : Version 5.1.0.0 dated 1 May 2004
        Co-developers   : Michel Berkelaar, Kjell Eikland, Peter Notebaert
        Licence terms   : GNU LGPL (Lesser General Public Licence)
        Citation policy : General references as per LGPL
        Module specific references as specified therein
*/
/**
 * Created by Katleho Khunyeli on 2016-08-13.
 */

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import lpsolve.LpSolve;
import lpsolve.LpSolveException;
import java.util.ArrayList;


public class AllocationModel {
    private LpSolve model;
    private int numVariables;
    private int[] variableIndexes;                                          //the indexes for the solver start at 1, and not 0
    private double[] constraintCoefficients;
    private TableView studentTable;
    private TableView businessUnitTable;
    private TableView criteraTable;
    private int numBusinessUnits;
    private String[] modelVariables;
    private ObservableList<ObservableList<ArrayList<String>>> businessUnitLists;
    private double tollerance;
    private double[] objCoefficients;
    private ArrayList<double[]> choiceConstraints;
    private ArrayList<Integer> choiceRanks;

    public AllocationModel(TableView studentTable, TableView businessUnitTable,TableView criteraTable,double tollerance){
        this.studentTable = studentTable;
        this.businessUnitTable = businessUnitTable;
        this.criteraTable = criteraTable;
        numBusinessUnits = businessUnitTable.getItems().size();
        modelVariables = createModelVariables();
        numVariables = modelVariables.length;
        variableIndexes = new int[numVariables];
        constraintCoefficients = new double[numVariables];
        this.tollerance = tollerance;
        objCoefficients = new double[numVariables];
        for(int i = 0; i < objCoefficients.length; i++) {
            objCoefficients[i] = 1;
        }

        try{
            constructModel();

            nameVariables(modelVariables);
            setGeneralAllocationConstraints();
            setBusinessUnitConstraints();
        }
        catch (LpSolveException e){
            e.printStackTrace();
        }

    }

    public AllocationModel(int numVariables){
        this.studentTable = studentTable;
        this.businessUnitTable = businessUnitTable;
        this.criteraTable = criteraTable;
        numBusinessUnits =3;
        modelVariables = createModelTestingVariable();
        this.numVariables = numVariables;
        variableIndexes = new int[numVariables];
        constraintCoefficients = new double[numVariables];

        try{
            constructModel();
            nameVariables(modelVariables);
            setGeneralAllocationConstraints();
            setBusinessUnitConstraints();
        }
        catch (LpSolveException e){
            e.printStackTrace();
        }
    }

    public void buildEntireModel(double tollerance) throws LpSolveException{
        this.tollerance = tollerance;
        constructModel();
        nameVariables(modelVariables);
        setGeneralAllocationConstraints();
        setBusinessUnitConstraints();
        setExclusionConstraints();
        setAllCriteriaConstraints();
    }

    public ObservableList<ObservableList<ArrayList<String>>> getBusinessUnitList(){
        return businessUnitLists;
    }
    /*
    public static void main(String[] args) {

        AllocationModel simpleExampleSolver = new AllocationModel(18);
        try{
            simpleExampleSolver.constructModel();
            simpleExampleSolver.nameVariables(new String[]{"s1-1","s1-2","s1-3","s2-1","s2-2","s2-3","s3-1","s3-2","s3-3","s4-1","s4-2","s4-3","s5-1","s5-2","s5-3","s6-1","s6-2","s6-3"});

            //Gender data information
            simpleExampleSolver.addConstraint(new double[]{1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,1,1,1},4,1);         //male
            simpleExampleSolver.addConstraint(new double[]{0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0},2,1);         //female

            //Ratio and Tollerance
            double t = 0.525;
            double mR = 0.5;
            double fR = 0.5;

            //A student must assigned to exactly one business unit
            simpleExampleSolver.addConstraint(new double[]{1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},1,1);         //student 1 can only be assigned to one business unit
            simpleExampleSolver.addConstraint(new double[]{0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0},1,1);         //student 2 can only be assigned to one business unit
            simpleExampleSolver.addConstraint(new double[]{0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0},1,1);         //student 3 can only be assigned to one business unit
            simpleExampleSolver.addConstraint(new double[]{0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0},1,1);         //student 4 can only be assigned to one business unit
            simpleExampleSolver.addConstraint(new double[]{0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0},1,1);         //student 5 can only be assigned to one business unit
            simpleExampleSolver.addConstraint(new double[]{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1},1,1);         //student 6 can only be assigned to one business unit-(mR-t)

            //BusinessUnit Constraints
            simpleExampleSolver.addConstraint(new double[]{1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0},1,2);         //students in BU 1 must be greater than 0
            simpleExampleSolver.addConstraint(new double[]{0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0},1,2);         //students in BU 2 must be greater than 0
            simpleExampleSolver.addConstraint(new double[]{0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1},1,2);         //students in BU 3 must be greater than 0



            //Gender Ratio constraints
            simpleExampleSolver.addConstraint(new double[]{1-(mR+t),0,0,1-(mR+t),0,0,1-(mR+t),0,0,-(mR+t),0,0,-(mR+t),0,0,1-(mR+t),0,0},0,0);         //male students assigned to business unit 1     ((3maleStudent/6totoalStudents)*3studentCapacity) = 2maleStudents))
            simpleExampleSolver.addConstraint(new double[]{-(fR+t),0,0,-(fR+t),0,0,-(fR+t),0,0,1-(fR+t),0,0,1-(fR+t),0,0,-(fR+t),0,0},0,0);                                 //female students assigned to business unit 1   (3studentCapacity - 2maleSudents = 1femaleStudent))
            simpleExampleSolver.addConstraint(new double[]{0,1-(mR+t),0,0,1-(mR+t),0,0,1-(mR+t),0,0,0,-(mR+t),0,0,-(mR+t),0,1-(mR+t),0},0,0);         //male students assigned to business unit 2     ((3maleStudent/6totoalStudents)*2studentCapacity) = 1))
            simpleExampleSolver.addConstraint(new double[]{0,-(fR+t),0,0,-(fR+t),0,0,-(fR+t),0,0,1-(fR+t),0,0,1-(fR+t),0,0,-(fR+t),0},0,0);                                 //female students assigned to business unit 2   (2studentCapacity - 1maleSudents = 1femaleStudent)

            simpleExampleSolver.addConstraint(new double[]{1-(mR-t),0,0,1-(mR-t),0,0,1-(mR-t),0,0,-(mR+t),0,0,-(mR+t),0,0,1-(mR-t),0,0},0,2);         //male students assigned to business unit 1     ((3maleStudent/6totoalStudents)*3studentCapacity) = 2maleStudents))
            simpleExampleSolver.addConstraint(new double[]{-(fR-t),0,0,-(fR-t),0,0,-(fR-t),0,0,1-(fR-t),0,0,1-(fR-t),0,0,-0.1,0,0},0,2);              //female students assigned to business unit 1   (3studentCapacity - 2maleSudents = 1femaleStudent))
            simpleExampleSolver.addConstraint(new double[]{0,1-(mR-t),0,0,1-(mR-t),0,0,1-(mR-t),0,0,0,-(mR-t),0,0,-(mR-t),0,1-(mR-t),0},0,2);         //male students assigned to business unit 2     ((3maleStudent/6totoalStudents)*2studentCapacity) = 1))
            simpleExampleSolver.addConstraint(new double[]{0,-(fR-t),0,0,-(fR-t),0,0,-(fR-t),0,0,1-(fR-t),0,0,1-(fR-t),0,0,-(fR-t),0},0,2);            //female students assigned to business unit 2   (2studentCapacity - 1maleSudents = 1femaleStudent)


            //Gender Ratio copy2 constraints
            simpleExampleSolver.addConstraint(new double[]{0.4,0,0,0.4,0,0,0.4,0,0,-0.6,0,0,-0.6,0,0,0.4,0,0},0,0);         //male students assigned to business unit 1     ((3maleStudent/6totoalStudents)*3studentCapacity) = 2maleStudents))
            simpleExampleSolver.addConstraint(new double[]{-0.6,0,0,-0.6,0,0,-0.6,0,0,0.4,0,0,0.4,0,0,-0.6,0,0},0,0);         //female students assigned to business unit 1   (3studentCapacity - 2maleSudents = 1femaleStudent))
            simpleExampleSolver.addConstraint(new double[]{0,0.4,0,0,0.4,0,0,0.4,0,0,0,-0.6,0,0,-0.6,0,0.4,0},0,0);         //male students assigned to business unit 2     ((3maleStudent/6totoalStudents)*2studentCapacity) = 1))
            simpleExampleSolver.addConstraint(new double[]{0,-0.6,0,0,-0.6,0,0,-0.6,0,0,0.4,0,0,0.4,0,0,-0.6,0},0,0);         //female students assigned to business unit 2   (2studentCapacity - 1maleSudents = 1femaleStudent)

            simpleExampleSolver.addConstraint(new double[]{0.6,0,0,0.6,0,0,0.6,0,0,-0.4,0,0,-0.4,0,0,0.6,0,0},0,2);         //male students assigned to business unit 1     ((3maleStudent/6totoalStudents)*3studentCapacity) = 2maleStudents))
            simpleExampleSolver.addConstraint(new double[]{-0.4,0,0,-0.4,0,0,-0.4,0,0,0.6,0,0,0.6,0,0,-0.4,0,0},0,2);         //female students assigned to business unit 1   (3studentCapacity - 2maleSudents = 1femaleStudent))
            simpleExampleSolver.addConstraint(new double[]{0,0.6,0,0,0.6,0,0,0.6,0,0,0,-0.4,0,0,-0.4,0,0.6,0},0,2);         //male students assigned to business unit 2     ((3maleStudent/6totoalStudents)*2studentCapacity) = 1))
            simpleExampleSolver.addConstraint(new double[]{0,-0.4,0,0,-0.4,0,0,-0.4,0,0,0.6,0,0,0.6,0,0,-0.4,0},0,2);         //female students assigned to business unit 2   (2studentCapacity - 1maleSudents = 1femaleStudent)



            //Gender Ratio Copy constraints
            simpleExampleSolver.addConstraint(new double[]{0.5,0,0,0.5,0,0,0.5,0,0,-0.5,0,0,-0.5,0,0,0.5,0,0},0,0);         //male students assigned to business unit 1     ((3maleStudent/6totoalStudents)*3studentCapacity) = 2maleStudents))
            simpleExampleSolver.addConstraint(new double[]{-0.5,0,0,-0.5,0,0,-0.5,0,0,0.5,0,0,0.5,0,0,-0.5,0,0},0,0);         //female students assigned to business unit 1   (3studentCapacity - 2maleSudents = 1femaleStudent))
            simpleExampleSolver.addConstraint(new double[]{0,0.5,0,0,0.5,0,0,0.5,0,0,0,-0.5,0,0,-0.5,0,0.5,0},0,0);         //male students assigned to business unit 2     ((3maleStudent/6totoalStudents)*2studentCapacity) = 1))
            simpleExampleSolver.addConstraint(new double[]{0,-0.5,0,0,-0.5,0,0,-0.5,0,0,0.5,0,0,0.5,0,0,-0.5,0},0,0);         //female students assigned to business unit 2   (2studentCapacity - 1maleSudents = 1femaleStudent)

            simpleExampleSolver.addConstraint(new double[]{0.5,0,0,0.5,0,0,0.5,0,0,-0.5,0,0,-0.5,0,0,0.5,0,0},0,2);         //male students assigned to business unit 1     ((3maleStudent/6totoalStudents)*3studentCapacity) = 2maleStudents))
            simpleExampleSolver.addConstraint(new double[]{-0.5,0,0,-0.5,0,0,-0.5,0,0,0.5,0,0,0.5,0,0,-0.5,0,0},0,2);         //female students assigned to business unit 1   (3studentCapacity - 2maleSudents = 1femaleStudent))
            simpleExampleSolver.addConstraint(new double[]{0,0.5,0,0,0.5,0,0,0.5,0,0,0,-0.5,0,0,-0.5,0,0.5,0},0,2);         //male students assigned to business unit 2     ((3maleStudent/6totoalStudents)*2studentCapacity) = 1))
            simpleExampleSolver.addConstraint(new double[]{0,-0.5,0,0,-0.5,0,0,-0.5,0,0,0.5,0,0,0.5,0,0,-0.5,0},0,2);


            //Student choice constraints
            simpleExampleSolver.addConstraint(new double[]{1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1},2,1);         //student 1 and 6 chose business unit one as their first choice
            simpleExampleSolver.addConstraint(new double[]{0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0},1,1);         //Student 2 chose business unit 2 as their first choice
            simpleExampleSolver.addConstraint(new double[]{0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,0,0,0},3,1);         //student 3,4,5 chose business unit 3 as their first choice


            simpleExampleSolver.addConstraint(new double[]{0,0,0,1,1,1,0,0,0,1,1,1,0,0,0,0,0,0},2,1);         //student 2,4 chose business unit 1 as second choice
            simpleExampleSolver.addConstraint(new double[]{0,0,0,0,0,0,1,1,1,0,0,0,1,1,1,0,0,0},2,1);         //student 3,5 chose business unit 2 as second choice
            simpleExampleSolver.addConstraint(new double[]{1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1},2,1);         //student 1,6 chose business unit 3 as second choice




            simpleExampleSolver.setObjectiveFunction(new double[]{1,0,0,0,1,0,0,0,1,0,0,1,0,0,1,1,0,0});    //objective fuction which seeks to maximize the number of students assigned to their first choice
                                                                                                            // (for now, must be equal to the number of students)


            simpleExampleSolver.solve();
        }
        catch (LpSolveException e){
            e.printStackTrace();
        }
    }
    */
    public boolean constructModel() throws LpSolveException{
        //constructs an initial model with 0 rows, and "numVariables" columns
        model = LpSolve.makeLp(0, numVariables );

        for(int i =0; i < numVariables;i++)
            model.setBinary(i+1,true);                                 //makes each of the variables integer types

        //model.setBFP("bfp_LUSOL");
        return model.getLp()!=0; //if model cannot be created, getLp will return 0
    }

    public void nameVariables(String[] names) throws LpSolveException{
        for(int i = 0; i < names.length;i++)
            model.setColName(i+1, names[i]);
    }

    public void addConstraint(double[] coefficients,int constraintBound,int option) throws  LpSolveException{
        for(int i = 0; i < coefficients.length;i++){
            constraintCoefficients[i] = coefficients[i];
            variableIndexes[i] = i + 1;
        }
        model.setAddRowmode(true);                                  //allows for the building of the model row by row, which is faster


        //adding the constraint to the model
        if(option==0)
            model.addConstraintex(coefficients.length,constraintCoefficients,variableIndexes,LpSolve.LE,constraintBound);
        else if(option==1)
            model.addConstraintex(coefficients.length,constraintCoefficients,variableIndexes,LpSolve.EQ,constraintBound);
        else
            model.addConstraintex(coefficients.length,constraintCoefficients,variableIndexes,LpSolve.GE,constraintBound);

    }

    public void addBusinesUnitConstraint(double[] coefficients,int constraintBound,boolean equality) throws  LpSolveException{
        for(int i = 0; i < coefficients.length;i++){
            constraintCoefficients[i] = coefficients[i];
            variableIndexes[i] = i + 1;
        }
        model.setAddRowmode(true);                                  //allows for the building of the model row by row, which is faster


        //adding the constraint to the model
        if(equality)
            model.addConstraintex(coefficients.length,constraintCoefficients,variableIndexes,LpSolve.GE,constraintBound);
        else
            model.addConstraintex(coefficients.length,constraintCoefficients,variableIndexes,LpSolve.LE,constraintBound);
    }

    public void setObjectiveFunction(double[] coefficients) throws LpSolveException{
        model.setAddRowmode(false);
        for(int i = 0; i < coefficients.length;i++){
            constraintCoefficients[i] = coefficients[i];
            variableIndexes[i] = i + 1;
        }

        model.setObjFnex(coefficients.length,constraintCoefficients,variableIndexes);
    }

    public void solve() throws LpSolveException{     //returns true if an optimal solution is found

        // set the object direction to maximize
        model.setMaxim();

        // just out of curioucity, now generate the model in lp format in file model.lp
        //model.writeLp("model.lp");

        // I only want to see important messages on screen while solving
        model.setVerbose(LpSolve.NEUTRAL);

        // Now let lpsolve calculate a solution
        //model.setScaling(LpSolve.SCALE_RANGE);
        //model.setBreakAtFirst(true);
        //model.setBreakAtValue(180.0);
        int solutionIndex = model.solve();
        //model.setSolutionlimit(3);
        if(solutionIndex == model.OPTIMAL){
            // a solution is calculated, now lets get some results

            // objective value
            //System.out.println("Objective value: " + model.getObjective());

            // variable values
            model.getVariables(constraintCoefficients);
            /*
            for(int j = 0; j < numVariables; j++){
                System.out.println(model.getColName(j + 1) + ": " + constraintCoefficients[j]);
                if((j+1)%numBusinessUnits==0)
                    System.out.println();
            }
            */
            // we are done now
            assignStudents(constraintCoefficients);                       //--must uncomment this to assign students
            //System.out.print("Tollerance: "+tollerance);
            //System.out.println("Distribution of the entire table:");

            //for(int i = 0; i < businessUnitLists.size(); i++)
            //    System.out.println("Students in Busines unit "+i+": "+businessUnitLists.get(i).size());
            //System.out.println("Model rows:" + model.getNorigRows());

        }
        else{
            //System.out.println("Tollerance: "+tollerance);
            try{
                if(tollerance < 1){
                    tollerance += 0.001;
                    buildEntireModel(tollerance);
                }
            }
            catch (IllegalStateException e){

            }
        }
    }

    private void assignStudents(double[] finalCoefficients) throws LpSolveException{
        businessUnitLists = FXCollections.observableArrayList();
        for(int i = 0; i < businessUnitTable.getItems().size();i++){
            ObservableList<ArrayList<String>> temp = FXCollections.observableArrayList();
            businessUnitLists.add(temp);
        }


        for(int j = 0; j < numVariables; j++){
            if(constraintCoefficients[j]==1.0){
                String student = model.getColName(j+1);
                int studentIndex = 0;
                int businessUnitIndex = 0;

                studentIndex = Integer.parseInt(student.substring(1,student.indexOf("-")))-1;
                businessUnitIndex = Integer.parseInt(student.charAt(student.length()-1)+"")-1;

                ArrayList<String> studentRow = new ArrayList<>();

                for (int i = 0; i < studentTable.getColumns().size(); i++)
                    studentRow.add(studentTable.getVisibleLeafColumn(i).getCellObservableValue(studentIndex).getValue().toString().trim());

                businessUnitLists.get(businessUnitIndex).add(studentRow);
            }

        }
    }

    private static double round (double value, int precision) {
        int scale = (int) Math.pow(10, precision);
        return (double) Math.round(value * scale) / scale;
    }

    private String[] createModelVariables(){
        ArrayList<String> variables = new ArrayList<>();

        for(int i =0;i < studentTable.getItems().size();i++){
            for(int j = 0; j < numBusinessUnits;j++){
                variables.add("s"+(i+1)+"-"+(j+1));
            }
        }
        int numVariables = studentTable.getItems().size() * numBusinessUnits;
        String[] returnArray = new String[numVariables];

        for(int i =0;i < variables.size();i++){
            returnArray[i] = variables.get(i);
        }

        return returnArray;
    }

    private String[] createModelTestingVariable(){
        ArrayList<String> variables = new ArrayList<>();

        for(int i =0;i < 6;i++){
            for(int j = 0; j < numBusinessUnits;j++){
                variables.add("s"+(i+1)+"-"+(j+1));
            }
        }
        int numVariables = 6 * numBusinessUnits;
        String[] returnArray = new String[numVariables];

        for(int i =0;i < variables.size();i++){
            returnArray[i] = variables.get(i);
        }

        return returnArray;
    }

    public void setAllCriteriaConstraints() throws LpSolveException{
        choiceConstraints = new ArrayList<>();
        choiceRanks = new ArrayList<>();
        String currentCriteria = "";
        Object rCOption;
        Object maxOption;
        Object rankOption;
        Boolean ratioConstraint ;
        Boolean objectiveFunction;
        int rank;

        for(int i = 0; i < criteraTable.getItems().size();i++){
            int criNameIndex = FXMLLoadScreenController.findColumnIndex(criteraTable,"Criteria");
            int choicePriorityIndex = FXMLLoadScreenController.findColumnIndex(criteraTable,"Choice Priority");
            currentCriteria = criteraTable.getVisibleLeafColumn(criNameIndex).getCellObservableValue(i).getValue().toString().trim();
            rCOption = criteraTable.getVisibleLeafColumn(choicePriorityIndex).getCellObservableValue(i).getValue();

            ratioConstraint = true;
            objectiveFunction = false;
            rank = 0;

            /*
            if(!(rCOption == null)){
                ratioConstraint  = Boolean.parseBoolean(rCOption.toString().trim());
            }

            maxOption = criteraTable.getVisibleLeafColumn(2).getCellObservableValue(i).getValue();

            if(!(maxOption == null)){
                objectiveFunction  = Boolean.parseBoolean(maxOption.toString().trim());
            }
            */


            rankOption = criteraTable.getVisibleLeafColumn(choicePriorityIndex).getCellObservableValue(i).getValue();

            if(!(rankOption == null) && !(rankOption.equals(" "))){
                rank  = Integer.parseInt(rankOption.toString().trim());
                choiceRanks.add(rank);
                ratioConstraint = false;
                if(rank == 1)
                    objectiveFunction = true;
            }

            setCriterionConstraints(currentCriteria,tollerance,ratioConstraint,objectiveFunction, rank);
        }
        if(choiceConstraints.size() > 0)
            setChoiceConstraints();
        //?????????????????????????????????????????????????????????????????????????????????????????
        setObjectiveFunction(objCoefficients);
        solve();
        //?????????????????????????????????????????????????????????????????????????????????????????
    }

    public void setCriterionConstraints(String tableColumnName, double tollerance, Boolean ratioConstraint,Boolean objectiveFunction, double rank) throws LpSolveException{
        this.tollerance = tollerance;
        int columnIndex = 0;
        int choiceColumnIndex = 0;

        ObservableList<TableColumn> columns = studentTable.getVisibleLeafColumns();
        for(TableColumn c: columns){
            if(c.getText().equalsIgnoreCase(tableColumnName)){
                columnIndex = columns.indexOf(c);

                /*
                if(!(rank == 0)){
                    String currentValue;
                    for(int i = 0; i < criteraTable.getItems().size(); i++){
                        if(!(criteraTable.getVisibleLeafColumn(1).getCellObservableValue(i).getValue() == null)){
                            currentValue = criteraTable.getVisibleLeafColumn(1).getCellObservableValue(i).getValue().toString().trim();
                            int criteriaRank = Integer.parseInt(currentValue);
                            if(criteriaRank==1){
                                String highestRankedCriteria = criteraTable.getVisibleLeafColumn(0).getCellObservableValue(i).getValue().toString().trim();
                                System.out.println("highest ranked criteria: "+highestRankedCriteria);
                                for(TableColumn col:columns){
                                    if(col.getText().equalsIgnoreCase(highestRankedCriteria))
                                        choiceColumnIndex = columns.indexOf(col);
                                }
                            }
                        }
                    }
                }*/
            }

        }


        ArrayList<String> subCriteria;

        if(rank == 0){
            subCriteria = determineSubCriteria(columnIndex);

        }
        else{
            subCriteria = determineSubCriteria();

        }
        int[] subCriteriaOccurances = determineSubCriteriaOccuranceRates(subCriteria,columnIndex,rank);

        double[][] constraintValues = new double[subCriteria.size()][studentTable.getItems().size() * numBusinessUnits];
        String currentCriteriaValue = "";



        for(int j = 0; j < subCriteria.size(); j++){
            for(int i = 0; i < studentTable.getItems().size() ; i ++){
                currentCriteriaValue = studentTable.getVisibleLeafColumn(columnIndex).getCellObservableValue(i).getValue().toString().trim();
                if (currentCriteriaValue.equalsIgnoreCase(subCriteria.get(j))){
                    for(int z = i*numBusinessUnits; z <i*numBusinessUnits + numBusinessUnits;z++ )
                        constraintValues[j][z] = 1;
                }
                else if(currentCriteriaValue.contains(subCriteria.get(j)) && rank > 0){
                    for(int z = i*numBusinessUnits; z <i*numBusinessUnits + numBusinessUnits;z++ )
                        constraintValues[j][z] = 1;
                }
            }
        }


        //for(int i = 0; i < subCriteria.size(); i++){
        //    System.out.println(subCriteria.get(i)+" : "+subCriteriaOccurances[i]);
        //}
        /*
        System.out.println("Constraint Values");
        for(int i = 0; i < constraintValues.length;i++){
            for(int j = 0; j < constraintValues[i].length;j++){
                System.out.print(constraintValues[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
        System.out.println();
        */
        if(rank == 0){
            for(int i = 0; i < constraintValues.length; i ++) {
                addConstraint(constraintValues[i], subCriteriaOccurances[i], 1);
                //model.setPresolve(LpSolve.PRESOLVE_ROWS | LpSolve.PRESOLVE_COLS | LpSolve.PRESOLVE_LINDEP, model.getPresolveloops());
            }
        }


        //System.out.println("Objective Function: "+objectiveFunction);
        //System.out.println("ratioConstraint: "+ratioConstraint);
        if(objectiveFunction){
            objCoefficients = constructObjectiveFunction(constraintValues);
        }

        if(rank > 0){
            choiceConstraints.add(constructObjectiveFunction(constraintValues));
        }
        /*
        System.out.println("Constraint Values");
        for(int i = 0; i < constraintValues.length;i++){
            for(int j = 0; j < constraintValues[i].length;j++){
                System.out.print(constraintValues[i][j] + " ");
            }
            System.out.println(subCriteriaOccurances[i]);
        }
        System.out.println();
        System.out.println();


        System.out.println("Ratio Constraint: "+ratioConstraint);
        */

        if(ratioConstraint){
            double[] criteriaRatios = determineOccuranceRatios(subCriteriaOccurances);

            double[][] ratioConstraints = getRatioConstraints(constraintValues);//,criteriaRatios,tollerance);
            int[] constraintBounds = new int[ratioConstraints.length];
            int[] businessUnitMaximums = getBusinessUnitMaximums();
            ArrayList<Integer> businessRangeIndexes= new ArrayList<>();
            ArrayList<Integer> constraintIndexes = new ArrayList<>();
            /*
            System.out.println("Ratio Constraints");
            for(int i = 0; i < ratioConstraints.length;i++){
                for(int j = 0; j < ratioConstraints[i].length; j++)
                    System.out.print(ratioConstraints[i][j]+"  ");
                System.out.println();
            }
            System.out.println();
            System.out.println();

            System.out.println("CriteriaRatios");
            for(int i = 0; i < criteriaRatios.length;i++){
                System.out.print(criteriaRatios[i]+"  ");
            }
            System.out.println();
            System.out.println();
            */
            int criRatioIndex = 0;
            int maxBuIndex = 0;

            for(int i = 0; i < ratioConstraints.length;i++){


                if(maxBuIndex == numBusinessUnits){
                    maxBuIndex =0;
                    criRatioIndex++;
                }


                //if(criRatioIndex == criteriaRatios.length)
                //criRatioIndex = 0;

                int buMaxIndex = FXMLLoadScreenController.findColumnIndex(businessUnitTable,"Maximum Students Requested");
                int businessUnitMax = Integer.parseInt(businessUnitTable.getVisibleLeafColumn(buMaxIndex).getCellObservableValue(maxBuIndex).getValue().toString().trim());          //part of previously parially working algorithm

                //String temp = Math.round(businessUnitMax*(criteriaRatios[criRatioIndex]+tollerance)) + "";

                String temp = Math.round(businessUnitMax*(criteriaRatios[criRatioIndex])) + "";
                constraintBounds[i] = Integer.parseInt(temp);

                //System.out.println("sub criteria ratio: " + criteriaRatios[criRatioIndex]);
                /*
                for(int p = 0;  p < ratioConstraints[i].length; p++ ){
                    System.out.print(ratioConstraints[i][p] + "  ");
                }
                System.out.print(constraintBounds[i]);
                System.out.println();
                */
                //addConstraint(ratioConstraints[i], constraintBounds[i], 0);                                               //was part of partially working algoritm
                //if(criteriaRatios[criRatioIndex] - tollerance >= 0) {
                //businessRangeIndexes.add(maxBuIndex);
                //constraintIndexes.add(i);
                //model.setRhRange(model.getNrows(),Math.round(tollerance*businessUnitMax));                          //sets the lower and upper bounds for a constraint
                //}

                //criRatioIndex++;
                maxBuIndex++;



            }

            adjustBoundsToMatchOccurances(constraintBounds,subCriteriaOccurances);

            maxBuIndex = 0;
            criRatioIndex = 0;
            for(int i = 0; i < ratioConstraints.length;i++){

                if(maxBuIndex == numBusinessUnits){
                    maxBuIndex =0;
                    criRatioIndex++;
                }

                //for(int p = 0;  p < ratioConstraints[i].length; p++ ){
                //    System.out.print(ratioConstraints[i][p] + "  ");
                //}
                //System.out.print(constraintBounds[i] + "  "+tollerance*constraintBounds[i]);
                //System.out.println();
                addConstraint(ratioConstraints[i], (int)Math.round(constraintBounds[i]+tollerance*constraintBounds[i]), 0);
                //if(criteriaRatios[criRatioIndex] - tollerance >= 0){
                //model.setRhRange(model.getNrows(),tollerance*constraintBounds[i]);
                //}

                maxBuIndex++;

            }

        }
    }

    private void adjustBoundsToMatchOccurances(int[] constraintBounds,int[] subcriteriaOccurance){
        int sum = 0;
        int difference = 0;
        int occranceIndex = 0;

        for(int i = 0; i < constraintBounds.length;i++){
            if(!(((i+1)+numBusinessUnits)%numBusinessUnits==0)){
                //System.out.println("constraintBounds["+i+"] "+constraintBounds[i]);
                sum += constraintBounds[i];
            }
            else{

                //System.out.println("constraintBounds["+i+"] "+constraintBounds[i]);
                sum += constraintBounds[i];
                //System.out.println("Sum: "+sum);
                difference = subcriteriaOccurance[occranceIndex] - sum;
                int indexOfBiggestBound = 0;
                int indexOfSmallestBound = 0;
                int biggestBound = 0;
                int smallestBound = 0;

                for(int j = i - (numBusinessUnits-1); j <= i; j++){
                    //System.out.println("Entered inner loop");
                    if(constraintBounds[j] > biggestBound){
                        biggestBound = constraintBounds[j];
                        indexOfBiggestBound = j;
                    }
                    else{
                        smallestBound = constraintBounds[j];
                        indexOfSmallestBound = j;
                    }
                }
                //System.out.println("Biggest Bound: "+biggestBound);
                //System.out.println("difference: "+difference);
                if(difference > 0){
                    constraintBounds[indexOfSmallestBound] += difference;
                }
                else{
                    constraintBounds[indexOfBiggestBound] += difference;
                }

                sum = 0;
                occranceIndex++;
            }

        }
    }

    public int[] getBusinessUnitMaximums(){
        int[] returnArray = new int[numBusinessUnits];
        int buMaxIndex = FXMLLoadScreenController.findColumnIndex(businessUnitTable,"Maximum Students Requested");
        //System.out.println("buMaxIndex: "+buMaxIndex);
        for(int i = 0; i < businessUnitTable.getItems().size();i++){
            returnArray[i] = Integer.parseInt(businessUnitTable.getVisibleLeafColumn(buMaxIndex).getCellObservableValue(i).getValue().toString().trim());
        }
        return returnArray;
    }

    private double[] constructObjectiveFunction(double[][] constraintValues){
        //System.out.println("Constraint Values: ");
        double[] returnArray = new double[constraintValues[0].length];
        for(int i = 0; i < constraintValues.length; i++){
            double[] leadingOnes = isolateLeadingOnes(constraintValues[i]);
            for(int j = 0; j < leadingOnes.length;j++){

                if(leadingOnes[j]==1.0 && j+i<returnArray.length){
                    returnArray[j+i] = 1.0;

                }
            }
        }

        return returnArray;
    }

    public void setChoiceConstraints() throws LpSolveException{
        int currentIndex = 0;
        int subsequentIndex = 0;
        int indexOfLowestRank = -1;
        double[] unAllocatedConstraint = new double[choiceConstraints.get(0).length];
        for(int i = 0; i < unAllocatedConstraint.length; i++)
            unAllocatedConstraint[i] = 1;

        for(int i = 0; i < choiceRanks.size() -1; i++){
            double[] temp = new double[choiceConstraints.get(0).length];
            if(choiceRanks.contains(i+1)){
                currentIndex = choiceRanks.indexOf(i+1);
                subsequentIndex = choiceRanks.indexOf(i+2);
                indexOfLowestRank = subsequentIndex;
                if(subsequentIndex<choiceConstraints.size() && subsequentIndex >=0){
                    for(int j = 0; j < temp.length; j++){
                        if(subsequentIndex != -1)
                            temp[j] = choiceConstraints.get(currentIndex)[j] - choiceConstraints.get(subsequentIndex)[j];
                        if(choiceConstraints.get(currentIndex)[j] > 0)
                            unAllocatedConstraint[j] = 0;
                    }
                }
            }
            /*
            for(int k = 0; k < temp.length; k++)
                System.out.print(temp[k] + "  ");
            System.out.println();
            */
            if(subsequentIndex != -1)
                addConstraint(temp,1,2);
        }
        double[] temp = new double[unAllocatedConstraint.length];
        for(int i = 0; i < temp.length; i++)
            temp[i] = choiceConstraints.get(indexOfLowestRank)[i] - unAllocatedConstraint[i];
        addConstraint(temp,1,2);

    }

    public void printArray(ArrayList<double[]> anArray){
        System.out.println();
        for(int i = 0; i < anArray.size(); i++){
            for(int j = 0; j < anArray.get(0).length; j++){
                System.out.print(anArray.get(i)[j]+"  ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public void printArray(double[][] anArray){
        System.out.println();
        for(int i = 0; i < anArray.length; i++){
            for(int j = 0; j < anArray[0].length; j++){
                System.out.print(anArray[i][j]+"  ");
            }
            System.out.println();
        }
        System.out.println();
    }

    private ArrayList<String> determineSubCriteria(int columnIndex) {

        ArrayList<String> subCriteria = new ArrayList<>();
        String currentValue = "";

        for(int row = 0; row < studentTable.getItems().size(); row++){
            currentValue = studentTable.getVisibleLeafColumn(columnIndex).getCellObservableValue(row).getValue().toString().trim();

            if(!subCriteria.contains(currentValue) && !currentValue.contains("NOT") && !currentValue.contains("PUT")){
                subCriteria.add(currentValue);
            }
        }
        return subCriteria;
    }

    private ArrayList<String> determineSubCriteria() {

        ArrayList<String> subCriteria = new ArrayList<>();
        String currentValue = "";

        for(int row = 0; row < businessUnitTable.getItems().size(); row++){
            currentValue = businessUnitTable.getVisibleLeafColumn(FXMLLoadScreenController.findColumnIndex(businessUnitTable,"Business Units")).getCellObservableValue(row).getValue().toString().trim();

            if(!subCriteria.contains(currentValue) && !currentValue.contains("NOT") && !currentValue.contains("PUT")){
                subCriteria.add(currentValue);
            }
        }
        return subCriteria;
    }

    private void setExclusionConstraints() throws LpSolveException{
        int notesColumnIndex = 0;
        ObservableList<TableColumn> columns = studentTable.getVisibleLeafColumns();
        for(TableColumn c: columns) {
            if (c.getText().equalsIgnoreCase("Notes")) {
                notesColumnIndex = columns.indexOf(c);
            }
        }

        String currentValue;

        for(int i = 0; i < studentTable.getItems().size();i++){
            currentValue = studentTable.getVisibleLeafColumn(notesColumnIndex).getCellObservableValue(i).getValue().toString().trim();
            if(currentValue.contains("NOT")){
                double[] constraint = new double[numVariables];
                int businessUnitNumber = determineBUNumFromString(currentValue);
                constraint[i*numBusinessUnits+businessUnitNumber] = 1;
                addConstraint(constraint,0,1);
            }
            else if(currentValue.contains("PUT")){
                double[] constraint = new double[numVariables];
                int businessUnitNumber = determineBUNumFromString(currentValue);
                constraint[i*numBusinessUnits+businessUnitNumber] = 1;
                addConstraint(constraint,1,1);
            }
        }
    }

    private void printArray(double[] anArray){
        for(double d:anArray){
            System.out.print(d + "  ");
        }
        System.out.println();
    }

    private int determineBUNumFromString(String noteExclusion){
        int businessUnitNumber = -1;
        String businessUnitName = noteExclusion.substring(4,noteExclusion.length());
        String currentValue;
        for(int i = 0; i <  businessUnitTable.getItems().size();i++){
            currentValue = businessUnitTable.getVisibleLeafColumn(0).getCellObservableValue(i).getValue().toString().trim();
            if(currentValue.equals(businessUnitName))
                businessUnitNumber = i;
        }
        return businessUnitNumber;
    }

    private int[] determineSubCriteriaOccuranceRates(ArrayList<String> subCri, int columnIndex, double rank){
        ArrayList<String> subCriteria = subCri;
        int[] occranceRates = new int[subCriteria.size()];
        String currentValue = "";

        for(int row = 0; row < studentTable.getItems().size(); row++){
            currentValue = studentTable.getVisibleLeafColumn(columnIndex).getCellObservableValue(row).getValue().toString().trim();

            if(subCriteria.contains(currentValue)){
                occranceRates[subCriteria.indexOf(currentValue)]++;
            }
            else if(rank > 0){
                for(int i = 0; i < subCriteria.size(); i++){
                    if(subCriteria.get(i).contains(currentValue) || currentValue.contains(subCriteria.get(i))){
                        occranceRates[i]++;
                    }
                }
            }
        }

        return occranceRates;
    }


    private double[] determineOccuranceRatios(int[] subCriteriaOccrances){
        int[] occurances = subCriteriaOccrances;
        double[] ratios = new double[occurances.length];
        for(int i = 0; i < occurances.length; i++){
            double occurance = occurances[i];
            ratios[i] = occurance/studentTable.getItems().size();
        }
        return ratios;
    }


    private void setGeneralAllocationConstraints()throws LpSolveException{

        double[][] constraintCoefficients = new double[studentTable.getItems().size()][modelVariables.length];
        for(int i = 0; i < constraintCoefficients.length;i++){
            for(int j = i*numBusinessUnits; j < i*numBusinessUnits + numBusinessUnits; j++){
                constraintCoefficients[i][j] = 1;
            }
        }

        for(double[] constraint: constraintCoefficients)
            addConstraint(constraint,1,1);
    }
    /*
    private double[][] getRatioConstraints(double[][] criterionConstraints, double[] subCriteriaOccurancesRatios,double tollerance){
        ArrayList<double[]> ratioArrays = new ArrayList<>();
        double[] leadingOnes = new double[criterionConstraints[0].length];

        double[][] criterionConstraintsCopy = new double[criterionConstraints.length][criterionConstraints[0].length];
        for(int i = 0; i < criterionConstraintsCopy.length; i++){
            for(int j = 0; j < criterionConstraintsCopy[i].length; j++){
                criterionConstraintsCopy[i][j] = criterionConstraints[i][j];
            }
        }

        for(int i = 0; i < criterionConstraintsCopy.length; i++){
            leadingOnes = isolateLeadingOnes(criterionConstraintsCopy[i]);

            for(int z = 0; z < numBusinessUnits;z++){
                int businessUnitMax = Integer.parseInt(businessUnitTable.getVisibleLeafColumn(6).getCellObservableValue(z).getValue().toString().trim());
                int businessUnitMin = Integer.parseInt(businessUnitTable.getVisibleLeafColumn(5).getCellObservableValue(z).getValue().toString().trim());
                int businessUnitCapacity = (businessUnitMax + businessUnitMin)/2;

                if (z == 0){
                    double[] tempMax = leadingOnes.clone();
                    double[] tempMin = leadingOnes.clone();
                    for(int d = 0; d < leadingOnes.length;d++){
                        tempMax[d] -= (subCriteriaOccurancesRatios[i]*businessUnitCapacity + tollerance);
                        //tempMin[d] -= (subCriteriaOccurancesRatios[i]*businessUnitCapacity - tollerance);
                    }

                    ratioArrays.add(tempMax);
                    ratioArrays.add(tempMin);
                }
                else{

                    leadingOnes = shiftOneRight(leadingOnes);
                    double[] tempMax = leadingOnes.clone();
                    double[] tempMin = leadingOnes.clone();

                    for(int d = 0; d < leadingOnes.length;d++){
                        tempMax[d] -= (subCriteriaOccurancesRatios[i]*businessUnitCapacity + tollerance);
                        //tempMin[d] -= (subCriteriaOccurancesRatios[i]*businessUnitCapacity - tollerance);
                    }

                    ratioArrays.add(tempMax);
                    //ratioArrays.add(tempMin);
                }
            }
        }

        double[][] returnArray = new double[ratioArrays.size()][criterionConstraints[0].length];

        for(int i = 0; i < returnArray.length;i++){
            for(int j =0; j < returnArray[i].length; j ++){
                returnArray[i][j] = ratioArrays.get(i)[j];
            }

        }

        return returnArray;
    }
    */
    //////////////////////////////////////////////////////////////////////////////////////////////////////////Method backup///////////////////////////////////////////////////////////////////////////////////////////

    private double[][] getRatioConstraints(double[][] criterionConstraints){// double[] subCriteriaOccurancesRatios,double tollerance){
        ArrayList<double[]> ratioArrays = new ArrayList<>();
        double[] leadingOnes = new double[criterionConstraints[0].length];

        double[][] criterionConstraintsCopy = new double[criterionConstraints.length][criterionConstraints[0].length];
        for(int i = 0; i < criterionConstraintsCopy.length; i++){
            for(int j = 0; j < criterionConstraintsCopy[i].length; j++){
                criterionConstraintsCopy[i][j] = criterionConstraints[i][j];
            }
        }

        for(int i = 0; i < criterionConstraintsCopy.length; i++){
            leadingOnes = isolateLeadingOnes(criterionConstraintsCopy[i]);

            for(int z = 0; z < numBusinessUnits;z++){

                if (z == 0){
                    double[] temp = leadingOnes.clone();
                    double[] tempMin = leadingOnes.clone();

                    /*
                    for(int d = 0; d < leadingOnes.length;d++){

                        if(leadingOnes[d]==1) {
                            tempMax[d] -= round((subCriteriaOccurancesRatios[i] + tollerance),1);
                            tempMin[d] -= round((subCriteriaOccurancesRatios[i] - tollerance),1);
                        }
                    }

                    for(int o = z; o < leadingOnes.length; o +=numBusinessUnits){
                        if(leadingOnes[o]==0){
                            tempMax[o] -= round((subCriteriaOccurancesRatios[i] + tollerance),1);
                            tempMin[o] -= round((subCriteriaOccurancesRatios[i] - tollerance),1);
                        }
                    }
                    */
                    ratioArrays.add(temp);
                    //ratioArrays.add(tempMin);
                }
                else{

                    leadingOnes = shiftOneRight(leadingOnes);
                    double[] temp = leadingOnes.clone();
                    double[] tempMin = leadingOnes.clone();
                    /*
                    for(int d = 0; d < leadingOnes.length;d++){

                        if(leadingOnes[d]==1) {
                            tempMax[d] -= (subCriteriaOccurancesRatios[i] + tollerance);
                            tempMin[d] -= (subCriteriaOccurancesRatios[i] - tollerance);
                        }
                    }

                    for(int o = z; o < leadingOnes.length; o +=numBusinessUnits){
                        if(leadingOnes[o]==0){
                            tempMax[o] -= (subCriteriaOccurancesRatios[i] + tollerance);
                            tempMin[o] -= (subCriteriaOccurancesRatios[i] - tollerance);
                        }
                    }
                    */
                    ratioArrays.add(temp);
                    //ratioArrays.add(tempMin);
                }
            }
        }

        double[][] returnArray = new double[ratioArrays.size()][criterionConstraints[0].length];

        for(int i = 0; i < returnArray.length;i++){
            for(int j =0; j < returnArray[i].length; j ++){
                returnArray[i][j] = ratioArrays.get(i)[j];
            }

        }

        return returnArray;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////Method backup End///////////////////////////////////////////////////////////////////////////////////////////
    private double[] isolateLeadingOnes(double[] numArray){
        double[] arrayOfNumbers = new double[numArray.length];
        for(int i = 0; i < arrayOfNumbers.length;i++)
            arrayOfNumbers[i] = numArray[i];

        ArrayList<Integer> indexes = new ArrayList<>();
        for (int i = 0; i < arrayOfNumbers.length;i++){
            if(arrayOfNumbers[i]==1.0){
                for(int z = 1; z < numBusinessUnits;z++)
                    arrayOfNumbers[i + z] = 0;
            }
        }
        return arrayOfNumbers;
    }

    private double[] shiftOneRight(double[] numArray){
        ArrayList<Double> arrayOfNumbers = new ArrayList<>();
        double temp = numArray[numArray.length-1];
        arrayOfNumbers.add(temp);
        for(int i = 0; i < numArray.length - 1;i++)
            arrayOfNumbers.add(numArray[i]);

        double[] returnArray = new double[arrayOfNumbers.size()];
        for(int i =0; i < returnArray.length;i++){
            returnArray[i] = arrayOfNumbers.get(i);
        }
        return returnArray;
    }

    public void setBusinessUnitConstraints()throws LpSolveException{
        int[] businessUnitMinimums = new int[numBusinessUnits];
        int[] businessUnitMaximums = new int[numBusinessUnits];
        double[][] buMinMaxConstraints = new double[numBusinessUnits*2][numVariables];

        for(int i = 0; i < numBusinessUnits; i++){
            //businessUnitMinimums[i] = Integer.parseInt(businessUnitTable.getVisibleLeafColumn(5).getCellObservableValue(i).getValue().toString().trim());

            businessUnitMaximums[i] = Integer.parseInt(businessUnitTable.getVisibleLeafColumn(5).getCellObservableValue(i).getValue().toString().trim());
        }

        double[] buConstraint = new double[numVariables];
        for(int i = 0; i < buConstraint.length; i += numBusinessUnits){
            buConstraint[i] = 1.0;
        }

        int boundaryIndex = 0;
        for(int i = 0; i < numBusinessUnits;i++){

            if(i==0){
                double[] minTemp = buConstraint.clone();
                double[] maxTemp = buConstraint.clone();
                buMinMaxConstraints[i] = minTemp;
                buMinMaxConstraints[i+numBusinessUnits] = maxTemp;
            }
            else{
                buConstraint = shiftOneRight(buConstraint);
                double[] minTemp = buConstraint.clone();
                double[] maxTemp = buConstraint.clone();
                buMinMaxConstraints[i] = minTemp;
                buMinMaxConstraints[i+numBusinessUnits] = maxTemp;
            }
        }
        /*
        System.out.println("buMinMaxConstraints");
        for(int i =0; i < buMinMaxConstraints.length; i++){
            if(boundaryIndex == numBusinessUnits)
                boundaryIndex = 0;

            for(int j = 0; j <buMinMaxConstraints[i].length;j++){
                System.out.print(buMinMaxConstraints[i][j] + " ");
            }
            if(i<4)
                System.out.println(businessUnitMinimums[boundaryIndex]);
            else
                System.out.println(businessUnitMaximums[boundaryIndex]);
            boundaryIndex++;
        }
        */
        for(int i = 0; i < buMinMaxConstraints.length; i++){
            if(boundaryIndex == numBusinessUnits)
                boundaryIndex = 0;

            if(i < 4) {
                //addBusinesUnitConstraint(buMinMaxConstraints[i], businessUnitMinimums[boundaryIndex], true);
            }
            else
                addBusinesUnitConstraint(buMinMaxConstraints[i],businessUnitMaximums[boundaryIndex],false);

            boundaryIndex++;
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Andre and Jenovic//
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private ArrayList<String> getNames (ObservableList<ArrayList<String>> table, int column) {

        ArrayList<String> uniqueValues = new ArrayList<String>();
        for (int row=0;row<table.size();row++) {
            boolean flag = false;
            for (int j=0;j<uniqueValues.size();j++) {
                if (table.get(row).get(column).equals(uniqueValues.get(j))) {
                    flag = true;
                }
            }
            if (flag == false) {
                uniqueValues.add(table.get(row).get(column));
            }
        }
        return uniqueValues;
    }

    private ArrayList<Float> getDistribution (ObservableList<ArrayList<String>> table, int column, ArrayList<String> uniqueValues) {


        ArrayList<Float> distributionCounter = new ArrayList<Float>();
        for(int i = 0; i<uniqueValues.size(); i++){
            distributionCounter.add(0f);
        }
        for (int row=0;row<table.size();row++) {
            boolean flag = false;
            for (int j=0;j<uniqueValues.size();j++) {
                if (table.get(row).get(column).equals(uniqueValues.get(j))) {
                    flag = true;
                    distributionCounter.set(j, distributionCounter.get(j)+1f);
                }
            }
        }
        for (int j=0;j<distributionCounter.size();j++) {
            distributionCounter.set(j, distributionCounter.get(j)/table.size()*100);
        }
        return distributionCounter;
    }

    public ObservableList<ArrayList<String>> obtainColumnData(){
        ArrayList<String> returnArray = new ArrayList<>();
        ObservableList<ArrayList<String>> tempColumn = FXCollections.observableArrayList();
        for(int row = 0; row < studentTable.getItems().size(); row++){
            //tempColumn = FXCollections.observableArrayList();

            //int colName = columnName;//studentTable.getVisibleLeafColumn(columnName).getText();
            //if(colName.equals("Business Unit Preference Option1")){
            returnArray.clear();
            for(int col = 0; col < studentTable.getColumns().size(); col++){
                String currentValue = studentTable.getVisibleLeafColumn(col).getCellObservableValue(row).getValue().toString().trim();
                returnArray.add(currentValue);

                //System.out.println(returnArray.add(currentValue));
            }
            tempColumn.add(new ArrayList<String>(returnArray));
            // }
        }

        return tempColumn;
    }
}