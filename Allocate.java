package IE_Project;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;
import javafx.scene.control.TableView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andre on 2016-07-27.
 */
public class Allocate {
    private TableView businessUnitTable;
    private TableView studentTable;
    private int numHeaders;
    private int totalStudents;
    private ListView listCriteria;

    public Allocate(TableView businessUnitTable, TableView studentTable, int numHeaders, int totalStudents, ListView listCriteria) {
        this.businessUnitTable = businessUnitTable;
        this.studentTable = studentTable;
        this.numHeaders = numHeaders;
        this.totalStudents = totalStudents;
        this.listCriteria = listCriteria;
    }

    public ObservableList<ObservableList<ArrayList<String>>> runAllocation() {
        ObservableList<ObservableList<ArrayList<String>>> bUnitList = FXCollections.observableArrayList();
        ObservableList<ArrayList<String>> leftOversList = FXCollections.observableArrayList();
        int bUnitNum = businessUnitTable.getItems().size();
        ArrayList<String> bUnitNames = new ArrayList<>();
        ArrayList<String> bUnitMax = new ArrayList<>();
        ArrayList<String> currentValueList = new ArrayList<>();

        for(int i = 0; i < bUnitNum; i++){
            ObservableList<ArrayList<String>> temp = FXCollections.observableArrayList();
            bUnitList.add(temp);
        }

        for(int i = 0; i< bUnitNum; i++){
            String rowName = businessUnitTable.getVisibleLeafColumn(0).getCellObservableValue(i).getValue().toString().trim();
            String maxVal = businessUnitTable.getVisibleLeafColumn(6).getCellObservableValue(i).getValue().toString().trim();
            bUnitNames.add(rowName);
            bUnitMax.add(maxVal);
        }

        int columnValue, columnValue2, columnValue3, columnValue4;
        columnValue =  columnValue2 =  columnValue3 = columnValue4 = 0;
        for(int col = 0; col < numHeaders; col++){
            String colName = studentTable.getVisibleLeafColumn(col).getText();
            if(colName.equals("Business Unit Preference Option1")){
                for(int row = 0; row < totalStudents; row++){
                    String currentValue = studentTable.getVisibleLeafColumn(col).getCellObservableValue(row).getValue().toString().trim();
                    currentValueList.add(currentValue);
                }
            }

            if(colName.equals("Business Unit Preference Option2")){
                columnValue = col;
                //System.out.println("column value 1: " + columnValue);
            }

            if(colName.equals("Business Unit Preference Option3")){
                columnValue2 = col;
                //System.out.println("column value 1: " + columnValue2);
            }

            if(colName.equals("Please indicate your Gender")){
                columnValue3 = col;
                //System.out.println("column value 1: " + columnValue3);
            }

            if(colName.equals("Please indicate your Race")){
                columnValue4 = col;
                //System.out.println("column value 4: " + columnValue4);
            }
        }

        for(int j=0; j <currentValueList.size(); j++ ){
            for(int i = 0; i < bUnitNames.size(); i++){
                if(currentValueList.get(j).equals(bUnitNames.get(i))){
                    if(bUnitList.get(i).size() < Integer.parseInt(bUnitMax.get(i))){
                        ArrayList<String> rowValue = new ArrayList<>();
                        for(int col = 0; col < numHeaders; col++){
                            String currentValue = studentTable.getVisibleLeafColumn(col).getCellObservableValue(j).getValue().toString().trim();
                            rowValue.add(currentValue);
                        }
                        bUnitList.get(i).add(rowValue);
                    }else{
                        ArrayList<String> leftOvers = new ArrayList<>();
                        for(int col = 0; col < numHeaders; col++){
                            String currentValue = studentTable.getVisibleLeafColumn(col).getCellObservableValue(j).getValue().toString().trim();
                            leftOvers.add(currentValue);
                        }
                        leftOversList.add(leftOvers);
                    }

                }
            }
        }
        for(int row = 0; row < leftOversList.size(); row++){
            for(int i = 0; i < bUnitNames.size(); i++){
                if(leftOversList.get(row).get(columnValue).substring(0,3).trim().equals(bUnitNames.get(i))){
                    if(bUnitList.get(i).size() < Integer.parseInt(bUnitMax.get(i))){
                        bUnitList.get(i).add(leftOversList.get(row));
                        leftOversList.remove(leftOversList.get(row));
                    }
                }else if(leftOversList.get(row).get(columnValue).contains(bUnitNames.get(i))){
                    int start = leftOversList.get(row).get(columnValue).lastIndexOf(bUnitNames.get(i));
                    int length = bUnitNames.get(i).length();
                    if(leftOversList.get(row).get(columnValue).substring(start,start + length).equals(bUnitNames.get(i))){
                        if(bUnitList.get(i).size() < Integer.parseInt(bUnitMax.get(i))){
                            bUnitList.get(i).add(leftOversList.get(row));
                            leftOversList.remove(leftOversList.get(row));
                        }
                    }
                }
            }
        }
        for(int row = 0; row < leftOversList.size(); row++){
            for(int i = 0; i < bUnitNames.size(); i++){
                if(leftOversList.get(row).get(columnValue2).substring(0,3).trim().equals(bUnitNames.get(i))){
                    if(bUnitList.get(i).size() < Integer.parseInt(bUnitMax.get(i))){
                        bUnitList.get(i).add(leftOversList.get(row));
                        leftOversList.remove(leftOversList.get(row));
                    }
                }else if(leftOversList.get(row).get(columnValue2).contains(bUnitNames.get(i))){
                    int start = leftOversList.get(row).get(columnValue2).lastIndexOf(bUnitNames.get(i));
                    int length = bUnitNames.get(i).length();
                    if(leftOversList.get(row).get(columnValue2).substring(start,start + length).equals(bUnitNames.get(i))){
                        if(bUnitList.get(i).size() < Integer.parseInt(bUnitMax.get(i))){
                            bUnitList.get(i).add(leftOversList.get(row));
                            leftOversList.remove(leftOversList.get(row));
                        }
                    }
                }
            }
        }
        for(int i = 0; i < bUnitList.size(); i++){
            if(bUnitList.get(i).size() < Integer.parseInt(bUnitMax.get(i))){
                for(int j = 0; j <leftOversList.size(); j++ ){
                    bUnitList.get(i).add(leftOversList.get(j));
                }
            }
        }
        for(int j = 0; j <leftOversList.size(); j++ ){
            leftOversList.remove(leftOversList.get(j));
            j--;
        }
        /*ArrayList<String> listCriteria = new ArrayList<String>();
        listCriteria.add("Please indicate your Gender");
        listCriteria.add("Please indicate your Race");*/

        System.out.println("Distribution Before Algorithm:");
        printDistribution(bUnitList);
        System.out.println("Distribution of the entire table:");
        printEntireDistribution();
        System.out.println("==================================");
        ArrayList<String> criteriaNames = new ArrayList<>(getNames(obtainColumnData(),columnValue4));

        criteriaNames.clear();
        criteriaNames = new ArrayList<>(getNames(obtainColumnData(),columnValue3));
        int iterationCount = 0;
        int[] anArray;
        anArray = highAndLowDistributions(bUnitList,criteriaNames);
        int distributionHigh = anArray[0];
        int nameWithHighestDistribution = anArray[1];
        int businessUnitHigh = anArray[2];
        int distributionLow = anArray[3];
        int nameWithLowestDistribution = anArray[4];
        int businessUnitLow = anArray[5];
        int aColumnValue = anArray[6];
        float targetDistribution = getDistribution(obtainColumnData(), aColumnValue, criteriaNames).get(nameWithHighestDistribution);
        //System.out.println("Gender Target Distribution: " +getDistribution(obtainColumnData(), columnValue3, criteriaNames).get(0));

        do {

            boolean flag = false;
            int studentCounter = bUnitList.get(businessUnitHigh).size();


            while (flag == false && studentCounter != 0) {
                studentCounter--;
                if(bUnitList.get(businessUnitHigh).get(studentCounter).get(aColumnValue).equals(criteriaNames.get(nameWithHighestDistribution))){
                    flag = true;
                }
            }
            flag = false;
            int studentCounter2 = bUnitList.get(businessUnitLow).size();


            while (flag == false && studentCounter2 != 0) {
                studentCounter2--;
                if(bUnitList.get(businessUnitLow).get(studentCounter2).get(aColumnValue).equals(criteriaNames.get(nameWithLowestDistribution))){
                    flag = true;
                }
            }
            //System.out.println("---------------------------------------------");
            System.out.println("Adding Student to businessUnitHigh: "+businessUnitHigh+", "+bUnitList.get(businessUnitLow).get(studentCounter2));
            bUnitList.get(businessUnitHigh).add(0,bUnitList.get(businessUnitLow).get(studentCounter2));
            System.out.println("Adding Student to businessUnitLow: "+businessUnitLow+", "+bUnitList.get(businessUnitHigh).get(studentCounter+1));
            bUnitList.get(businessUnitLow).add(0,bUnitList.get(businessUnitHigh).get(studentCounter+1));
            bUnitList.get(businessUnitHigh).remove(studentCounter+1);
            bUnitList.get(businessUnitLow).remove(studentCounter2+1);

            System.out.println("number of iterations: " + iterationCount++);
            System.out.println("DistributionHigh: "+distributionHigh+", DistributionLow: "+distributionLow+", targetDistribution: "+targetDistribution+", targetDistributionHigh: "+(targetDistribution + 5)+", targetDistributionLow: "+ (targetDistribution - 5));
            printDistribution(bUnitList);
            System.out.println("---------------------------------------------");

            anArray = highAndLowDistributions(bUnitList,criteriaNames);
            distributionHigh = anArray[0];
            nameWithHighestDistribution = anArray[1];
            businessUnitHigh = anArray[2];
            distributionLow = anArray[3];
            nameWithLowestDistribution = anArray[4];
            businessUnitLow = anArray[5];
            aColumnValue = anArray[6];
            targetDistribution = getDistribution(obtainColumnData(), aColumnValue, criteriaNames).get(nameWithHighestDistribution);
        } while (distributionHigh > targetDistribution +5 || distributionLow < targetDistribution - 5);
        System.out.println("Distribution of the entire table:");
        printEntireDistribution();
        printDistribution(bUnitList);
        return bUnitList;
    }

    private int[] highAndLowDistributions(ObservableList<ObservableList<ArrayList<String>>> bUnitList, ArrayList<String> criteriaNames) {
        int[] anArray = new int[7];
        anArray[0] = anArray[1] = anArray[3] = anArray[4] = anArray[5] = 0;
        int aColumnSaved = 0;
        int aColumnValue = 0;
        //float targetDistribution = 0;
        float distributionHigh = 0;
        float distributionLow = 100;
        boolean flag = true;
        int savedCriteriaNamesSize = 0;
        float totalTargetDistribution = 0;
        float totalTargetDistribution2 = 0;
        for (int i = 0; i < listCriteria.getItems().size(); i++) {
            criteriaNames.clear();


            for (int col = 0; col < numHeaders; col++) {
                String colName = studentTable.getVisibleLeafColumn(col).getText();
                if (colName.equals(listCriteria.getItems().get(i))) {
                    aColumnValue = col;
                    //System.out.println("the column value: " + aColumnValue);
                }
            }
            criteriaNames.addAll(getNames(obtainColumnData(), aColumnValue));
            //criteriaNames.clear();
            //ArrayList<String> criteriaNames = new ArrayList<>(getNames(obtainColumnData(),columnValue3));

            for (int j = 0; j < criteriaNames.size(); j++) {


                //int businessUnitHigh = 0;
                //int businessUnitLow = 0;
                for (int k = 0; k < bUnitList.size(); k++) {
                    float currentDistribution = getDistribution(bUnitList.get(k), aColumnValue, criteriaNames).get(j);
                    //System.out.println("- distribution: "+getDistribution(obtainColumnData(), aColumnValue, criteriaNames).get(j));
                    if ((currentDistribution-getDistribution(obtainColumnData(), aColumnValue, criteriaNames).get(j))/*criteriaNames.size()*/  > (distributionHigh-totalTargetDistribution)/*savedCriteriaNamesSize*/) {
                        distributionHigh = currentDistribution;
                        //System.out.println("DISTRIBUTION HIGH: " + distributionHigh);
                        anArray[0] = (int) distributionHigh;
                        anArray[1] = j;
                        anArray[2] = k;
                        aColumnSaved = aColumnValue;
                        distributionLow = 100;
                        flag = true;
                        savedCriteriaNamesSize = criteriaNames.size();
                        totalTargetDistribution = getDistribution(obtainColumnData(), aColumnValue, criteriaNames).get(j);
                        //System.out.println("totalTargetDistribution: "+totalTargetDistribution);
                    }

                }
            }

            if (flag == true) {

                for (int k = 0; k < bUnitList.size(); k++) {
                    float currentDistribution = getDistribution(bUnitList.get(k), aColumnSaved, criteriaNames).get(anArray[1]);
                    //System.out.println("Current Distribution: " + currentDistribution);
                    if (currentDistribution < distributionLow) {
                        distributionLow = currentDistribution;
                        //System.out.println("DISTRIBUTION LOW: " + distributionLow);
                        anArray[3] = (int) distributionLow;
                        //anArray[4] = j;
                        anArray[5] = k;
                        flag = false;
                    }
                }
                //}
            }

            criteriaNames.clear();
            criteriaNames.addAll(getNames(obtainColumnData(), aColumnSaved));
            int businessUnitLow = 0;
            for (int j = 0; j < criteriaNames.size(); j++) {

                //for (int k = 0; k < bUnitList.size(); k++) {
                float currentDistribution = getDistribution(bUnitList.get(anArray[5]), aColumnValue, criteriaNames).get(j);
                if ((currentDistribution-getDistribution(obtainColumnData(), aColumnValue, criteriaNames).get(j))/*criteriaNames.size()*/ > (businessUnitLow-totalTargetDistribution2)/*savedCriteriaNamesSize*/ && j != anArray[1]) {
                    businessUnitLow = (int) currentDistribution;
                    //System.out.println("Business unit Low: " + businessUnitLow);
                    //anArray[3] = (int) distributionLow;
                    totalTargetDistribution2 = getDistribution(obtainColumnData(), aColumnValue, criteriaNames).get(j);
                    anArray[4] = j;
                }
                //}
            }
            //System.out.println("-------------Iteration finished-------------");
        }
        anArray[6] = aColumnSaved;
        return anArray;
    }

    private ArrayList<String> getNames (ObservableList<ArrayList<String>> table, int column) {

        ArrayList<String> uniqueValues = new ArrayList<String>();
        for (int row=0;row<table.size();row++) {
            boolean flag = false;
            for (int j=0;j<uniqueValues.size();j++) {
                if (table.get(row).get(column).equals(uniqueValues.get(j))) {
                    flag = true;
                }
            }
            if (flag == false) {
                uniqueValues.add(table.get(row).get(column));
            }
        }
        return uniqueValues;
    }

    private ArrayList<Float> getDistribution (ObservableList<ArrayList<String>> table, int column, ArrayList<String> uniqueValues) {


        ArrayList<Float> distributionCounter = new ArrayList<Float>();
        for(int i = 0; i<uniqueValues.size(); i++){
            distributionCounter.add(0f);
        }
        for (int row=0;row<table.size();row++) {
            boolean flag = false;
            for (int j=0;j<uniqueValues.size();j++) {
                if (table.get(row).get(column).equals(uniqueValues.get(j))) {
                    flag = true;
                    distributionCounter.set(j, distributionCounter.get(j)+1f);
                }
            }
        }
        for (int j=0;j<distributionCounter.size();j++) {
            distributionCounter.set(j, distributionCounter.get(j)/table.size()*100);
        }
        return distributionCounter;
    }

    public ObservableList<ArrayList<String>> obtainColumnData(){
        ArrayList<String> returnArray = new ArrayList<>();
        ObservableList<ArrayList<String>> tempColumn = FXCollections.observableArrayList();
        for(int row = 0; row < totalStudents; row++){
            //tempColumn = FXCollections.observableArrayList();

            //int colName = columnName;//studentTable.getVisibleLeafColumn(columnName).getText();
            //if(colName.equals("Business Unit Preference Option1")){
            returnArray.clear();
            for(int col = 0; col < numHeaders; col++){
                String currentValue = studentTable.getVisibleLeafColumn(col).getCellObservableValue(row).getValue().toString().trim();
                returnArray.add(currentValue);

                //System.out.println(returnArray.add(currentValue));
            }
            tempColumn.add(new ArrayList<String>(returnArray));
            // }
        }

        return tempColumn;
    }

    public void printDistribution(ObservableList<ObservableList<ArrayList<String>>> bUnitList) {
        System.out.println("-----Print Distribution-----");

        for (int y=0;y<listCriteria.getItems().size();y++) {
            for (int z=0;z<studentTable.getColumns().size();z++) {
                for (int x=0;x<bUnitList.size();x++) {
                    if(studentTable.getVisibleLeafColumn(z).getText().equals(listCriteria.getItems().get(y))) {
                        System.out.println("BU " + x + ": " + listCriteria.getItems().get(y) + ": " + getDistribution(bUnitList.get(x), z, getNames(obtainColumnData(), z)));
                    }
                }
            }
        }
    }

    public void printEntireDistribution() {
        for (int z=0;z<studentTable.getColumns().size();z++) {
            for (int y=0;y<listCriteria.getItems().size();y++) {
                if (studentTable.getVisibleLeafColumn(z).getText().equals(listCriteria.getItems().get(y))) {
                    System.out.println("\""+studentTable.getVisibleLeafColumn(z).getText() + "\" Target Distribution:\t\t\t " + getDistribution(obtainColumnData(), z, getNames(obtainColumnData(), z)));
                }
            }
        }
    }
}