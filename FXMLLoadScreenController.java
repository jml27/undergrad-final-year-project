/*
 * TEAM 3 INDUSTRIAL EXPERIENCE PROJECT
 * KPMG Student Allocation System Created by Monash University South Africa's IE Team 3
 * 2016 (c) KPMG & Monash University
 */
package IE_Project;

import com.sun.javafx.scene.control.skin.TableHeaderRow;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.layout.*;
import javafx.stage.Modality;
import javafx.stage.Stage;
import lpsolve.LpSolveException;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.*;

import static IE_Project.IEProjectMain.resultsScreen;
import static IE_Project.IEProjectMain.resultsScreenFile;

/**
 * FXML Controller class
 *
 * @author TEAM 3
 */
public class FXMLLoadScreenController implements Initializable, ControlledStage {

    @FXML private Button btnLoadData, btnAddStudent, btnDeleteStudent, sideBarButton, searchButton, closeSearchBtn;
    @FXML private Button btnAddCriteria, btnDeleteCriteria, btnReturnAllocation, btnStartAllocation;
    @FXML private Button btnAddBusinessUnit, btnDeleteBusinessUnit;
    @FXML private TableView studentTable, businessUnitTable, criteriaTable;
    @FXML private ComboBox cmbFilter;
    @FXML private TextField tfSearch;
    @FXML private TextArea loadScreenOutput;
    @FXML private MenuBar fileMenu;
    @FXML private Menu tasksMenu;
    @FXML private MenuItem saveButton, saveAsButton, exportButton, btnBackToResults;
    @FXML private BorderPane borderPane;
    @FXML private ToolBar searchToolBar;
    @FXML private Accordion loadScreenAccordion;
    @FXML private TitledPane actionsPane, businessUnitPane, criteriaPane, studentPane, outputPane;
    @FXML private VBox nodeContainer;
    @FXML private ToolBar mainToolBar;
    @FXML public GridPane progressGrid;

    private StageController projectController;
    private DialogueCreator dialogueCreator;
    private TableColumnCreator tableColumnCreator;
    private SlideBarController topFlapBar, leftFlapBar;
    private final ProgressIndicator progressIndicator = new ProgressIndicator();
    private final Image menuIcon = new Image(getClass().getResourceAsStream("menu-icon.png"));
    private final Image searchIcon = new Image(getClass().getResourceAsStream("search-icon.png"));
    private final Image closeSearchIcon = new Image(getClass().getResourceAsStream("close-icon.png"));

    private static ArrayList<Integer> criteriaColumnList = new ArrayList<Integer>();
    private boolean resultScreenLoaded, isDataLoaded, hasBeenSaved;
    private int totalStudents, totalStudentsRequested, numHeaders, totalBusUnits, totalCriteria;
    private String [] busUnames;
    private ArrayList<String> checkBusinessU = new ArrayList<>();
    private ArrayList<String> bUnits = new ArrayList<>();
    private ArrayList<String> filterBUnits = new ArrayList<>();
    private ArrayList<String> checkCriteria = new ArrayList<>();
    private ArrayList<Integer> averageValues = new ArrayList<>();
    private ListView<String> listCriteria;
    private ListView<String> listBusUnits;
    private ObservableList<String> dataCriteria;
    private ObservableList<String> dataBusUnits;
    private ObservableList<SimpleStringProperty> averageData, quartileData, noteData, studentPopulation, extraNoteData;
    private String [] headerValues;


    @Override
    public void setStageParent(StageController stageParent){
        projectController = stageParent;
    }

    /**
     * Initializes the controller.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //Variable initialization
        resultScreenLoaded = false;
        dialogueCreator = new DialogueCreator(new Image(getClass().getResourceAsStream("kpmg_icon.png")));
        tableColumnCreator = new TableColumnCreator();

        //Sidebar Setup
        topFlapBar = new SlideBarController(34, searchButton, Pos.TOP_LEFT, searchToolBar);
        loadScreenAccordion.setExpandedPane(actionsPane);
        leftFlapBar = new SlideBarController(204, sideBarButton, Pos.BASELINE_LEFT, loadScreenAccordion);
        searchButton.setGraphic(new ImageView(searchIcon));
        sideBarButton.setGraphic(new ImageView(menuIcon));
        borderPane.setTop(topFlapBar);
        borderPane.setLeft(leftFlapBar);
        closeSearchBtn.setGraphic(new ImageView(closeSearchIcon));

        //Table setup & preparation
        prepareTable(studentTable);
        prepareTable(businessUnitTable);
        prepareTable(criteriaTable);

        // Filter setup
        cmbFilter.getItems().addAll("No Filter", "Missing Values", "Male", "Female");

        if (IEProjectMain.projectManager.isProjectReady) {
            // Prepare the load screen with the given project
            isDataLoaded = true;
            hasBeenSaved = true;

            ArrayList<TableView> theTables = new ArrayList<>();
            theTables.add(studentTable);
            theTables.add(businessUnitTable);
            theTables.add(criteriaTable);

            IEProjectMain.projectManager.loadProject(IEProjectMain.projectManager.getProject(),
                    theTables, loadScreenOutput);
            setTotals();

            setButtonStates(!isDataLoaded);
        } else {
            //Variable initialization
            isDataLoaded = false;
            hasBeenSaved = false;

            //General application setup
            setButtonStates(!isDataLoaded);
        }
        setupFileDropListener();
        btnReturnAllocation.setDisable(true);
        studentTable.toFront();
    }

    private void setButtonStates(boolean state) {
        tasksMenu.setDisable(state);
        btnAddBusinessUnit.setDisable(state);
        btnAddCriteria.setDisable(state);
        btnAddStudent.setDisable(state);
        btnDeleteBusinessUnit.setDisable(state);
        btnDeleteCriteria.setDisable(state);
        btnDeleteStudent.setDisable(state);
        btnStartAllocation.setDisable(state);
        saveButton.setDisable(state);
        saveAsButton.setDisable(state);
        exportButton.setDisable(state);
        cmbFilter.setDisable(state);
        searchButton.setDisable(state);
    }

    private void setupFileDropListener() {
        nodeContainer.setOnDragOver(event -> {
            Dragboard db = event.getDragboard();
            if (db.hasFiles()) {
                event.acceptTransferModes(TransferMode.COPY);
            } else {
                event.consume();
            }
        });

        // Dropping over surface
        nodeContainer.setOnDragDropped(event -> {
            Dragboard db = event.getDragboard();
            boolean success = false;
            if (db.hasFiles()) {
                if (db.getFiles().size() == 1) {
                    success = true;
                    File theFile = db.getFiles().get(0);
                    String fileExtension = theFile.getName().substring(theFile.getName().indexOf("."));
                    if (fileExtension.equals(".csv")) {
                        System.out.println("CSV FILE DROPPED INTO THE APPLICATION");
                    } else if (fileExtension.equals(".alloc")) {
                        System.out.println("ALLOC FILE DROPPED INTO THE APPLICATION");
                    } else {
                        dialogueCreator.createErrorDialogue("Unrecognized File Type", "The file you dropped is not " +
                                "compatible with the application.\n" +
                                "\nFile Selected\t\t\t: " + theFile.getName() +
                                "\nSupported File Types\t: Comma Separated Values (.csv); Allocation Projects (.alloc)");
                    }
                } else {
                    success = false;
                    dialogueCreator.createErrorDialogue("Too Many Files Selected", "Please only select 1 file to" +
                            " load when making use of the Drag 'n' Drop feature.");
                }
            }
            event.setDropCompleted(success);
            event.consume();
        });
    }


    //NAVIGATION
    @FXML
    private void viewResultsScreen(ActionEvent event) throws IOException {
        performAllocation();
    }

    @FXML
    private void backToResultsScreen(ActionEvent event) throws IOException {
        projectController.setStage(resultsScreen);
    }


    //LOAD DATA ACTIONS
    @FXML
    private void loadData(ActionEvent event) throws IOException {
        System.out.println("[Load Data File Action Started]");
        boolean success = IEProjectMain.projectManager.loadData(isDataLoaded, studentTable, loadScreenOutput,
                btnLoadData.getScene().getWindow());
        if (success) {
            isDataLoaded = true;
            hasBeenSaved = false;
            prepareTable(businessUnitTable);
            prepareTable(criteriaTable);
            setButtonStates(!isDataLoaded);
            resetVariables();
            studentPopulation = studentTable.getItems();
            setTotals();
            loadScreenAccordion.setExpandedPane(actionsPane);
            studentTable.toFront();
        }
        System.out.println("[Load Data File Action Completed]");
    }

    @FXML
    private void loadExistingProject(ActionEvent event) throws IOException {
        System.out.println("[Load Existing Project Action Started]");
        ArrayList<TableView> theTables = new ArrayList<>();
        theTables.add(studentTable);
        theTables.add(businessUnitTable);
        theTables.add(criteriaTable);
        boolean success = IEProjectMain.projectManager.loadProject(isDataLoaded, theTables, loadScreenOutput,
                btnLoadData.getScene().getWindow());
        if (success) {
            isDataLoaded = true;
            setButtonStates(!isDataLoaded);
            resetVariables();
            studentPopulation = studentTable.getItems();
            setTotals();
        }
        System.out.println("[Load Existing Project Action Completed]");
    }

    private void resetVariables() {
        averageValues = new ArrayList<>();

        // Business Units
        busUnames = null;
        bUnits = new ArrayList<>();
        filterBUnits = new ArrayList<>();
        checkBusinessU = new ArrayList<>();
        listBusUnits = new ListView<>();
        dataBusUnits = null;

        // Criteria
        checkCriteria = new ArrayList<>();
        listCriteria = new ListView<>();
        dataCriteria = null;
    }

    private void setTotals() {
        numHeaders = studentTable.getColumns().size();
        totalStudents = studentTable.getItems().size();
        totalBusUnits = businessUnitTable.getItems().size();
        totalCriteria = criteriaTable.getItems().size();
        headerValues = new String[numHeaders];
        for (int i = 0; i < numHeaders; i++) headerValues[i] = studentTable.getVisibleLeafColumn(i).getText();
        if (totalBusUnits != 0) {
            setBusUnitData();
            loadScreenOutput.appendText("\nTotal Business Units Loaded\t\t\t: " + totalBusUnits);
        }
        if (totalCriteria != 0) {
            setCritData();
            loadScreenOutput.appendText("\nTotal Evaluation Criteria Loaded\t\t: " + totalCriteria);
        }
        loadScreenOutput.appendText("\nTotal Student Columns Loaded\t\t: " + numHeaders);
        loadScreenOutput.appendText("\nTotal Student Records Loaded\t\t: " + totalStudents);
    }

    private void setBusUnitData() {
        checkBusinessU.clear();
        busUnames = detectBusinessUnits();
        TableColumn businessUnits = businessUnitTable.getVisibleLeafColumn(0);
        for (int i = 0; i < businessUnitTable.getItems().size(); i++) {
            checkBusinessU.add((String) businessUnits.getCellData(i));
        }
        dataBusUnits = FXCollections.observableArrayList(checkBusinessU);
        listBusUnits = new ListView<>();
        listBusUnits.setItems(dataBusUnits);

        int notesColumnIndex = 0;
        ObservableList<TableColumn> columns = studentTable.getVisibleLeafColumns();
        for(TableColumn c: columns) {
            if (c.getText().equalsIgnoreCase("Notes")) {
                notesColumnIndex = columns.indexOf(c);
            }
        }

        TableColumn notesCol = (TableColumn) studentTable.getColumns().get(notesColumnIndex);
        ArrayList<String> notesOptions = new ArrayList<>();
        notesOptions.add("");
        notesOptions.add("AA Clerk");
        notesOptions.add("Thuthuka");
        notesOptions.add("Top 10");
        notesOptions.add("Sasol");
        notesOptions.add("Other");

        for(String exclusion: dataBusUnits)
            notesOptions.add("PUT "+exclusion);

        for(String exclusion: dataBusUnits)
            notesOptions.add("NOT "+exclusion);

        String[] completeComboOptions = new String[notesOptions.size()];

        for(int i = 0; i < completeComboOptions.length; i++)
            completeComboOptions[i] = notesOptions.get(i);
        notesCol.setCellFactory(ComboBoxTableCell.forTableColumn(completeComboOptions));

    }

    private void setCritData() {
        checkCriteria.clear();
        TableColumn criteria = criteriaTable.getVisibleLeafColumn(0);
        for (int i = 0; i < criteriaTable.getItems().size(); i++) {
            checkCriteria.add((String) criteria.getCellData(i));
        }
        dataCriteria = FXCollections.observableArrayList(checkCriteria);
        listCriteria = new ListView<>();
        listCriteria.setItems(dataCriteria);
        for (int i = 0; i < listCriteria.getItems().size(); i++) {
            for (int j = 0; j < studentTable.getColumns().size(); j++) {
                if (studentTable.getVisibleLeafColumn(j).getText().equals(listCriteria.getItems().get(i))) {
                    criteriaColumnList.add(j);
                }
            }
        }
        int priorityColumnIndex = 0;
        ObservableList<TableColumn> columns = criteriaTable.getVisibleLeafColumns();
        for (TableColumn c :columns) {
            if (c.getText().equalsIgnoreCase("Choice Priority"))
                priorityColumnIndex = columns.indexOf(c);
        }
        TableColumn priorityCol = (TableColumn) criteriaTable.getColumns().get(priorityColumnIndex);
        ArrayList<String> priorityOptions = new ArrayList<>();
        for (int i = 0; i < dataBusUnits.size(); i++) {
            if (i == 0)
                priorityOptions.add("");
            else
                priorityOptions.add(i + "");
        }
        String[] completePriorityOptions = new String[priorityOptions.size()];
        for (int i = 0; i < completePriorityOptions.length; i++)
            completePriorityOptions[i] = priorityOptions.get(i);
        priorityCol.setCellFactory(ComboBoxTableCell.forTableColumn(completePriorityOptions));
        criteriaSetupThing();
    }

    private void prepareTable(TableView aTable) {
        aTable.getItems().clear();
        aTable.getColumns().clear();
        aTable.setPlaceholder(new Label("Import a data file."));
        aTable.setEditable(true);
    }

    @FXML
    private void exportData() throws IOException {
        System.out.println("[EXPORT STUDENT DATA ACTION STARTED]");
        IEProjectMain.projectManager.exportData(studentTable, loadScreenOutput, nodeContainer.getScene().getWindow());
        System.out.println("[EXPORT STUDENT DATA ACTION COMPLETED]");
    }


    //ALLOCATION
    private void performAllocation() {
        if ((checkBusinessU.size() == 0)) {
            dialogueCreator.createErrorDialogue("No Business Units Have been Defined", "Please Add some Business Units before " +
                    "attempting to start the allocation process.");
            viewBusinessUnits();
        } else if (checkCriteria.size() == 0) {
            dialogueCreator.createErrorDialogue("No Evaluation Criteria Have been Defined", "Please Add some Evaluation Criteria " +
                    "before attempting to start the allocation process.");
            viewCriteria();
        } else if(checkChoicePriority()) {
            dialogueCreator.createErrorDialogue("No Choice Priority Indicated for any criteria", "Please indicate which criteria constitute the student " +
                    "choice preferences");
            viewCriteria();
        }else if (checkTotalStudentsRequested()) {
            loadScreenAccordion.setExpandedPane(actionsPane);
            studentTable.toFront();
            Optional<ButtonType> result = dialogueCreator.createConfirmDialogue("Start Student Allocation?",
                    "Are you sure that you want to start the allocation process?\n" +
                    "\nNote that you won't be able to use the application whilst the allocations are being performed.");
            if (result.get() == ButtonType.OK){
                if (!resultScreenLoaded) {
                    IEProjectMain.mainContainer.loadStage(resultsScreen, resultsScreenFile);
                    resultScreenLoaded = true;
                }
                progressGrid.add(progressIndicator,2,2);
                progressGrid.toFront();

                Runnable runnable = () -> {
                    topFlapBar.hidePanel();
                    leftFlapBar.hidePanel();
                    fileMenu.setDisable(true);
                    outputPane.setExpanded(false);
                    mainToolBar.setDisable(true);
                    outputPane.setDisable(true);
                    businessUnitTable.setVisible(false);
                    criteriaTable.setVisible(false);
                    studentTable.setDisable(true);
                    btnBackToResults.setDisable(false);
                    btnReturnAllocation.setDisable(false);
                    AllocationModel model = new AllocationModel(studentTable,businessUnitTable,criteriaTable,0.0);

                    try{
                        model.buildEntireModel(0.0);
                        FXMLResultsScreenController aController = (FXMLResultsScreenController) projectController.getScreenControllers(2);
                        ArrayList<String> array = new ArrayList<>();
                        for (int i = 0; i < studentTable.getColumns().size(); i++) {
                            array.add(studentTable.getVisibleLeafColumn(i).getText());
                        }
                        TableView table = studentTable;
                        TableView cTable = criteriaTable;
                        ObservableList<ObservableList<ArrayList<String>>> finalClassLists = model.getBusinessUnitList();
                        if(!(finalClassLists == null)){
                            aController.loadClassLists(finalClassLists, array, dataBusUnits, checkCriteria,table,cTable);
                            aController.createGraphs(dataBusUnits,listCriteria,criteriaColumnList);
                            aController.getTables(table,dataBusUnits);
                            projectController.setStage(IEProjectMain.resultsScreen);
                        }
                        else{
                            Platform.runLater(() -> {
                                dialogueCreator.createErrorDialogue("No Solution Found", "Tips:\n\n1) Ensure that " +
                                        "the sum of the business unit maximums adds up to the total number of " +
                                        "students, i.e. "+studentTable.getItems().size() + "\n\n2) Do not select subcriteria that are applicable to a very " +
                                        "small proportion of the student population." +"\n\n3) Ensure that there is a choice priority number in the criteria table corresponding "+
                                "to each choice preferemce option");
                            });
                        }
                    }
                    catch (LpSolveException e){
                        e.printStackTrace();
                    }
                };
                Thread thread = new Thread(runnable);
                thread.start();

                Runnable test = () -> {
                    boolean s = true;
                    while(s) {
                        if (!thread.isAlive()) {
                            removeProgress();
                            s = false;
                        }
                    }
                };

                new Thread(test).start();
            }
        }
    }

    private boolean checkChoicePriority(){
        boolean returnBool = true;
        Object rankOption = null;
        for(int i = 0; i < criteriaTable.getItems().size(); i++){
            rankOption = criteriaTable.getVisibleLeafColumn(1).getCellObservableValue(i).getValue();
            if(!(rankOption == null) && !(rankOption.equals(" "))){
                returnBool = false;
                break;
            }
        }
        return returnBool;
    }

    private void removeProgress(){
        Platform.runLater(() -> {
            fileMenu.setDisable(false);
            outputPane.setExpanded(true);
            mainToolBar.setDisable(false);
            outputPane.setDisable(false);
            businessUnitTable.setVisible(true);
            criteriaTable.setVisible(true);
            studentTable.setDisable(false);
            studentTable.toFront();
            progressGrid.getChildren().remove(progressIndicator);
        });

    }


    //SAVE PROJECT
    @FXML
    private void saveProject(ActionEvent vent) throws IOException {
        System.out.println("[Save Project Action Started]");
        ArrayList<TableView> theTables = new ArrayList<>();
        theTables.add(studentTable);
        theTables.add(businessUnitTable);
        theTables.add(criteriaTable);
        boolean success = IEProjectMain.projectManager.saveProject(hasBeenSaved, theTables, loadScreenOutput,
                btnLoadData.getScene().getWindow());
        if (success)
            hasBeenSaved = true;
        System.out.println("[Save Project Action Completed]");
    }

    @FXML
    private void saveProjectAs(ActionEvent event) throws IOException {
        System.out.println("[Save Project As Action Started]");
        ArrayList<TableView> theTables = new ArrayList<>();
        theTables.add(studentTable);
        theTables.add(businessUnitTable);
        theTables.add(criteriaTable);
        boolean success = IEProjectMain.projectManager.saveProject(false, theTables, loadScreenOutput,
                btnLoadData.getScene().getWindow());
        if (success)
            hasBeenSaved = true;
        System.out.println("[Save Project As Action Completed]");
    }


    //BUSINESS UNITS
    private void loadBusinessUnits() {
        busUnames = detectBusinessUnits();
        Stage dialog = new Stage();
        dialog.initModality(Modality.APPLICATION_MODAL);

        // create a Vbox to contain the dialog content
        VBox dialogVbox = new VBox(15);
        Scene dialogScene = new Scene(dialogVbox, 500, 300);

        GridPane gridBU = new GridPane();
        //checkboxes
        CheckBox[] cbs = new CheckBox[busUnames.length];
        ScrollPane sp = new ScrollPane();
        HBox btnLayout = new HBox(10);

        //other components
        Label lab = new Label("The Following Items Were Detected");
        Label lab1 = new Label("Choose Which Business Units You Wish To Add: ");
        lab.setPadding(new Insets(10, 10, 0, 10));
        lab1.setPadding(new Insets(0,10,0,10));
        Button cancelBtn = new Button("Cancel");
        Button saveBtn = new Button ("Save");

        //set the scene and show the dialog
        dialog.setTitle("KPMG Student Allocation System - Save Business Units");
        dialog.getIcons().add(new Image(getClass().getResourceAsStream("kpmg_icon.png")));
        dialog.setScene(dialogScene);
        dialog.show();

        //Add a checkbox to a gridPane
        for(int i = 0; i< busUnames.length; i++) {
            if(checkBusinessU.contains(busUnames[i])) {
                CheckBox cb = cbs[i] = new CheckBox(busUnames[i]);
                cb.setSelected(true);
                RowConstraints row = new RowConstraints(30);
                gridBU.getRowConstraints().add(row);
                gridBU.add(cb,1,i);
            } else {
                CheckBox cb = cbs[i] = new CheckBox(busUnames[i]);
                RowConstraints row = new RowConstraints(30);
                gridBU.getRowConstraints().add(row);
                gridBU.add(cb,1,i);
            }
        }

        gridBU.setAlignment(Pos.CENTER);
        gridBU.setPadding(new Insets(10,10,10,10));
        btnLayout.getChildren().addAll(cancelBtn,saveBtn);
        sp.setContent(gridBU);

        //Add all components to the dialog
        btnLayout.setAlignment(Pos.CENTER_RIGHT);
        btnLayout.setPadding(new Insets(0,10,10,0));
        dialogVbox.getChildren().addAll(lab,lab1,sp,btnLayout);

        // Handle the cancel button of the dialog that contains the check box
        cancelBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent event) {
                dialog.close();
                int counter = 0;
                for(CheckBox c : cbs) {
                    if (c.isSelected()) {
                        counter++;
                    }
                }
                if (counter == 0) {
                    filterBUnits.clear();
                }
            }
        });

        //Handle the save button of the dialog that contains the checkbox
        saveBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent event) {
                Button btn3 = new Button("Back");
                Button saveBtn2 = new Button ("Save");
                checkBusinessU.clear();
                for(CheckBox cb: cbs) {
                    if(cb.isSelected()) {
                        String s = cb.getText();
                        if(!checkBusinessU.contains(s)) {
                            checkBusinessU.add(s);
                        }
                    }
                }

                //checks if no checkbox was selected
                if(checkBusinessU.isEmpty()) {
                    listBusUnits.setPlaceholder(new Label("No Business Unit(s) were selected."));
                    saveBtn2.setDisable(true);
                }

                Stage confirmDialog = new Stage();
                confirmDialog.initModality(Modality.APPLICATION_MODAL);

                VBox newDialogVbox = new VBox(30);
                Scene newDialogScene = new Scene(newDialogVbox, 400, 300);

                listBusUnits = new ListView<>();
                dataBusUnits = FXCollections.observableArrayList(checkBusinessU);
                listBusUnits.setItems(dataBusUnits);


                Label newLab = new Label("Selected Business Units:");
                newLab.setPadding(new Insets(10,10,10,10));
                HBox hbBU = new HBox(10);
                hbBU.getChildren().addAll(btn3,saveBtn2);
                hbBU.setAlignment(Pos.CENTER_RIGHT);
                hbBU.setPadding(new Insets(10,20,20,10));

                newDialogVbox.getChildren().addAll(newLab,listBusUnits,hbBU);
                newDialogVbox.setPadding(new Insets(0,20,0,20));

                confirmDialog.setTitle("KPMG Student Allocation System - Save Business Unit");
                confirmDialog.getIcons().add(new Image(getClass().getResourceAsStream("kpmg_icon.png")));
                confirmDialog.setScene(newDialogScene);
                confirmDialog.show();

                btn3.setOnAction(new EventHandler<ActionEvent>() {

                    @Override public void handle(ActionEvent event) {
                        dialog.show();
                        confirmDialog.close();
                    }

                });

                saveBtn2.setOnAction(new EventHandler<ActionEvent>(){

                    @Override public void handle(ActionEvent event) {
                        confirmDialog.close();
                        dialog.close();
                        if (businessUnitTable.getItems().isEmpty()) {
                            initializeBusinessUnit();
                            tableColumnCreator.getBusinessUnits(businessUnitTable);
                            addExtraColumns();
                            System.out.println("New table columns: "+studentTable.getColumns().size());
                        } else {
                            int buNameIndex = findColumnIndex(businessUnitTable, "Business Units");
                            ObservableList<StringProperty> addBU;
                            ObservableList<Object> buTableData = FXCollections.observableArrayList();
                            for (int i = 0; i < cbs.length; i++) {
                                addBU = FXCollections.observableArrayList();
                                boolean addedBU = false;
                                if (cbs[i].isSelected()) {
                                    String currentValue = "";
                                    for (int j = 0; j < businessUnitTable.getItems().size(); j++) {
                                        currentValue = businessUnitTable.getVisibleLeafColumn(buNameIndex).getCellObservableValue(j).getValue().toString().trim();
                                        if (currentValue.equals(cbs[i].getText())) {
                                            buTableData.add(businessUnitTable.getItems().get(j));
                                            addedBU = true;
                                        }
                                    }
                                    if (!addedBU) {
                                        for (int j = 0; j < businessUnitTable.getColumns().size(); j++) {
                                            if (j == buNameIndex)
                                                addBU.add(new SimpleStringProperty(cbs[i].getText()));
                                            else
                                                addBU.add(new SimpleStringProperty(""));
                                        }
                                        buTableData.add(addBU);
                                    }
                                }
                            }
                            businessUnitTable.getItems().clear();
                            businessUnitTable.setItems(buTableData);
                        }
                    }
                });
                dialog.close();
            }
        });
    }

    private String[] detectBusinessUnits() {
        String bu = " ";
        for(int i = 0; i < studentTable.getVisibleLeafColumns().size(); i++)
        {
            bu = studentTable.getVisibleLeafColumn(i).getText();
            if(bu.contains("Business Unit Preference Option1") || bu.contains("BU") )
            {
                for(int k = 0; k < studentTable.getItems().size(); k++)
                {
                    bu = studentTable.getVisibleLeafColumn(i).getCellObservableValue(k).getValue().toString();
                    bUnits.add(bu);
                }
            }
        }

        //Remove Duplicates
        LinkedHashSet<String> lhs = new LinkedHashSet<>();
        lhs.addAll(bUnits);
        bUnits.clear();
        bUnits.addAll(lhs);

        if(checkBusinessU.isEmpty() && filterBUnits.isEmpty()){
            for(int i = 0; i < bUnits.size(); i++)
            {
                if(bUnits.get(i).contains("(")){
                    String bUnitAbbr = bUnits.get(i).substring(bUnits.get(i).indexOf("(")+1,bUnits.get(i).lastIndexOf(")"));
                    bUnits.set(i,bUnitAbbr);
                }

                if(bUnits.get(i).length() == 2 || bUnits.get(i).length() == 3)
                {
                    filterBUnits.add(bUnits.get(i));
                }
            }
        }

        String [] busU = new String[filterBUnits.size()];
        for(int q = 0; q < filterBUnits.size(); q++)
        {
            busU[q] = filterBUnits.get(q);
        }
        return busU;
    }

    public void initializeBusinessUnit(){
        businessUnitTable.getColumns().clear();
        businessUnitTable.getColumns().addAll(
                tableColumnCreator.createColumn(0, "Business Units"),
                tableColumnCreator.createColumn(1, "Business Unit Name"),
                tableColumnCreator.createColumn(2, "Description"),
                tableColumnCreator.createColumn(3, "Contact Name"),
                tableColumnCreator.createColumn(4, "Contact Number"),
                tableColumnCreator.createColumn(5, "Maximum Students Requested"));
        /*
        for (int i = 0; i < businessUnitTable.getColumns().size(); i++) {
            businessUnitTable.getVisibleLeafColumn(i).impl_reorderableProperty().setValue(false);
        }*/
        ObservableList<StringProperty> businessUnitInfo;
        businessUnitTable.getItems().clear();
        for (int i = 0; i < checkBusinessU.size(); i++) {
            businessUnitInfo = FXCollections.observableArrayList();
            businessUnitInfo.add(new SimpleStringProperty(checkBusinessU.get(i)));
            businessUnitInfo.add(new SimpleStringProperty(""));
            businessUnitInfo.add(new SimpleStringProperty(""));
            businessUnitInfo.add(new SimpleStringProperty(""));
            businessUnitInfo.add(new SimpleStringProperty(""));
            businessUnitInfo.add(new SimpleStringProperty(""));
            businessUnitTable.getItems().add(businessUnitInfo);
        }
    }

    @FXML
    private void addBusinessUnit(ActionEvent event) throws IOException{
        viewBusinessUnits();
        loadBusinessUnits();
    }

    @FXML
    private void viewBusinessUnits() {
        if (studentTable.getItems().isEmpty()) {
            businessUnitTable.setPlaceholder(new Label("Import a data file."));
            businessUnitTable.toFront();
        } else {
            businessUnitTable.setPlaceholder(new Label("Add Some Business Units."));
            businessUnitTable.toFront();
        }
        loadScreenAccordion.setExpandedPane(businessUnitPane);
    }

    @FXML
    private void deleteBusinessUnit(ActionEvent event) throws IOException{
        viewBusinessUnits();
        Object selectedRecord = businessUnitTable.getSelectionModel().getSelectedItem();
        int selectedIdx = businessUnitTable.getSelectionModel().getSelectedIndex();
        if (!businessUnitTable.getItems().isEmpty()){
            if (selectedRecord != null) {
                Optional<ButtonType> result = dialogueCreator.createConfirmDialogue("Delete Business Unit",
                        "Are you sure that you want to delete this Business Unit?");
                if (result.get() == ButtonType.OK){
                    businessUnitTable.getItems().remove(selectedRecord);
                    //checkBusinessU.remove(selectedIdx);
                }
                if (businessUnitTable.getItems().isEmpty()) {
                    businessUnitTable.setPlaceholder(new Label("All data has been deleted."));
                }
            } else {
                dialogueCreator.createErrorDialogue("No Business Unit Selected",
                        "Please select a Business Unit before trying to delete it.");
            }
        }else{
            dialogueCreator.createErrorDialogue("No Business Units To Delete",
                    "The Business Unit Table contains no records, therefore nothing can be deleted.");
        }
    }

    private boolean checkTotalStudentsRequested() {
        totalStudentsRequested = 0;
        TableColumn busUnitNames = businessUnitTable.getVisibleLeafColumn(findColumnIndex(businessUnitTable,
                "Business Units"));
        TableColumn studentsRequested = businessUnitTable.getVisibleLeafColumn(findColumnIndex(businessUnitTable,
                "Maximum Students Requested"));
        for (int i = 0; i < checkBusinessU.size(); i++) {
            String unitRequest = (String) studentsRequested.getCellData(i);
            if ((unitRequest.equals(null)) || (unitRequest.equals(""))) {
                dialogueCreator.createErrorDialogue(busUnitNames.getCellData(i) + " Requested No Students", "It appears that " +
                        busUnitNames.getCellData(i) + " has requested no students.\n" +
                        "\nTo correct this issue, please provide the requested number of students for:" +
                        "\n- Business Unit\t: " + busUnitNames.getCellData(i) +
                        "\n- Column\t\t: " + studentsRequested.getText() +
                        "\n- Row\t\t: " + (i+1));
                viewBusinessUnits();
                return false;
            } else if (!(unitRequest.chars().allMatch(Character::isDigit))) {
                dialogueCreator.createErrorDialogue(busUnitNames.getCellData(i) + " Contains Invalid Input",
                        busUnitNames.getCellData(i) + " did not specify its requested number of " +
                                "students as numeric value.\n" +
                                "\nTo correct this issue, please provide the requested number of students for:" +
                                "\n- Business Unit\t: " + busUnitNames.getCellData(i) +
                                "\n- Column\t\t: " + studentsRequested.getText() +
                                "\n- Row\t\t: " + (i+1));
                viewBusinessUnits();
                return false;
            } else {
                totalStudentsRequested += Integer.parseInt(unitRequest);
            }
        }
        if (totalStudentsRequested < totalStudents) {
            dialogueCreator.createErrorDialogue("Total Students Requested < Total Students in Data Set", "The maximum number of " +
                                "students requested by all business units is less than the total number of " +
                                "students found within the data file.\n" +
                                "\nTotal Students Requested\t: " + totalStudentsRequested +
                                "\nTotal Students in Data Set\t: " + totalStudents + "\n" +
                                "\nTo fix this problem, please ensure that the sum of students requested by all " +
                                "Business Units is equal to the total number of students found " +
                                "within the data file.");
            viewBusinessUnits();
            return false;
        } else {
            return true;
        }
    }


    //CRITERIA
    @FXML
    public String[] determineCriteria() {
        String[] returnArray;
        ArrayList<String> potentialCriteria;
           ArrayList<String> finalCriteria = new ArrayList<>();
           String currentValue = "";
           int maxReps = 0;
           for(int col = 0; col < numHeaders; col++){
                potentialCriteria= new ArrayList<>();
                maxReps = totalStudents;

                for(int row = 0; row < maxReps; row++){
                    currentValue = studentTable.getVisibleLeafColumn(col).getCellObservableValue(row).getValue().toString().trim();

                    if(potentialCriteria.contains(currentValue)){
                        potentialCriteria.remove(currentValue);
                    }
                    else
                        potentialCriteria.add(currentValue);
                }

                if(totalStudents - potentialCriteria.size() > 15) {
                    finalCriteria.add(studentTable.getVisibleLeafColumn(col).getText());
                }
            }
            returnArray = new String[finalCriteria.size()];
            for(int z = 0; z < finalCriteria.size(); z++)
                returnArray[z] = finalCriteria.get(z);

            return returnArray;

    }

    public void fetchCriteria(){

        //create the dialog
        Stage dialog = new Stage();
        dialog.initModality(Modality.APPLICATION_MODAL);

        // create a Vbox to contain the dialog content
        VBox dialogVbox = new VBox(15);
        Scene dialogScene = new Scene(dialogVbox, 360, 500);

        String[] names = determineCriteria();
        System.out.println(names.length);
        //checkboxes
        CheckBox[][] cbs = new CheckBox[names.length][];
        ScrollPane sp = new ScrollPane();
        HBox btnLayout = new HBox(10);
        GridPane grid = new GridPane();

        //other components
        Label lab = new Label("The Following Items Were Detected");
        Label lab1 = new Label("Choose Which Criteria You Wish To Add: ");
        lab.setPadding(new Insets(10, 10, 0, 10));
        lab1.setPadding(new Insets(0,10,0,10));
        Button btn1 = new Button("Cancel");
        Button btn2 = new Button ("Save");

        //set the scene and show the dialog
        dialog.setTitle("KPMG Student Allocation System - Choose Criteria");
        dialog.getIcons().add(new Image(getClass().getResourceAsStream("kpmg_icon.png")));
        dialog.setScene(dialogScene);
        dialog.show();

        //Add a checkboxes
        Accordion acord = new Accordion();
        CheckBox cb;
        ArrayList<String> namesArray;
        for(int i = 0; i< names.length; i++){
            int savedTableCounter = 0;
            if(checkCriteria.contains(names[i])){
                //logic
                savedTableCounter = findColumnIndex(studentTable,names[i]);
                namesArray = getNames(obtainColumnData(),savedTableCounter);
                cbs[i] = new CheckBox[namesArray.size()];
                grid = new GridPane();
                for (int y = 0; y < namesArray.size();y++) {
                    cb = cbs[i][y] = new CheckBox(namesArray.get(y));
                    cb.setSelected(true);

                    RowConstraints row = new RowConstraints(30);
                    grid.getRowConstraints().add(row);
                    grid.add(cb, 1, y);
                }
                //formatting
                formatCriteriaDialog(acord,i,names,grid);
            } else {
                //logic
                savedTableCounter = findColumnIndex(studentTable,names[i]);

                namesArray = getNames(obtainColumnData(),savedTableCounter);
                cbs[i] = new CheckBox[namesArray.size()];
                grid = new GridPane();
                for (int y = 0; y < namesArray.size();y++) {
                    cb = cbs[i][y] = new CheckBox(namesArray.get(y));
                    RowConstraints row = new RowConstraints(30);
                    grid.getRowConstraints().add(row);
                    grid.add(cb, 1, y);
                }
                //formatting
                formatCriteriaDialog(acord,i,names,grid);
            }
        }

        String[] changedTolerances = new String[cbs.length];
        for(int i = 0; i < cbs.length; i++) {
            int selectedCounter = 0;
            for (int j = 0; j < cbs[i].length; j++) {
                if(cbs[i][j].isSelected()) {
                    selectedCounter++;
                }
            }

            /*
            if(selectedCounter > 0 && criteriaTable.getVisibleLeafColumn(1).getCellObservableValue(i) != null){
                changedTolerances[i] = criteriaTable.getVisibleLeafColumn(1).getCellObservableValue(i).getValue().toString();
            } else {
                changedTolerances[i] = "";
            }
            */
        }

        grid.setAlignment(Pos.CENTER);
        grid.setPadding(new Insets(10,10,10,10));
        btnLayout.getChildren().addAll(btn1,btn2);
        sp.setContent(acord);

        //Add all components to the dialog
        btnLayout.setAlignment(Pos.CENTER_RIGHT);
        btnLayout.setPadding(new Insets(0,10,10,0));
        dialogVbox.getChildren().addAll(lab,lab1,sp,btnLayout);



        // Handle the cancel button of the dialog that contains the check box
        btn1.setOnAction(new EventHandler<ActionEvent>(){
            @Override public void handle(ActionEvent event) {
                dialog.close();
            }
        });

        //Handle the save button of the dialog that contains the checkbox
        btn2.setOnAction(new EventHandler<ActionEvent>(){
            @Override public void handle(ActionEvent event) {
                listCriteria = new ListView<>();
                Button btn3 = new Button("Back");
                Button btn4 = new Button ("Save");
                checkCriteria.clear();
                for(int i = 0; i < cbs.length; i++) {
                    int selectedCounter = 0;
                    for (int j = 0; j < cbs[i].length; j++) {
                        if(cbs[i][j].isSelected()) {
                            selectedCounter++;
                        }
                    }

                    if (selectedCounter > 0) {
                        String s = names[i];
                        if(!checkCriteria.contains(s)){
                            checkCriteria.add(s);
                            for (int j = 0; j < studentTable.getColumns().size(); j++) {
                                if (studentTable.getVisibleLeafColumn(j).getText().equals(names[i])) {
                                    criteriaColumnList.add(j);
                                }
                            }
                        }
                    }

                }

                //checks if no checkbox was selected
                int selectedCounter = 0;
                for(int i = 0; i < cbs.length; i++) {
                    for (int j = 0; j < cbs[i].length; j++) {
                        if (cbs[i][j].isSelected()) {
                            selectedCounter++;
                        }
                    }
                }

                if(selectedCounter == 0){
                    listCriteria.setPlaceholder(new Label("No Criterion(s) were selected."));
                    btn4.setDisable(true);
                }

                //creates a new dialog box of selecetd checkboxes
                Stage confirmDialog = new Stage();
                confirmDialog.initModality(Modality.APPLICATION_MODAL);

                VBox newDialogVbox = new VBox(30);
                Scene newDialogScene = new Scene(newDialogVbox, 400, 300);

                //add selecetd criteria to a listview
                dataCriteria = FXCollections.observableArrayList(checkCriteria);
                listCriteria.setItems(dataCriteria);


                Label newLab = new Label("Selected Criteria:");
                newLab.setPadding(new Insets(10,10,10,10));
                HBox newHb = new HBox(10);
                newHb.getChildren().addAll(btn3,btn4);
                newHb.setAlignment(Pos.CENTER_RIGHT);
                newHb.setPadding(new Insets(10,20,20,10));

                newDialogVbox.getChildren().addAll(newLab,listCriteria,newHb);
                newDialogVbox.setPadding(new Insets(0,20,0,20));

                confirmDialog.setTitle("KPMG Student Allocation System - Save Criteria");
                confirmDialog.getIcons().add(new Image(getClass().getResourceAsStream("kpmg_icon.png")));
                confirmDialog.setScene(newDialogScene);
                confirmDialog.show();

                btn3.setOnAction(new EventHandler<ActionEvent>(){

                    @Override public void handle(ActionEvent event) {
                        dialog.show();
                        confirmDialog.close();
                    }

                });

                btn4.setOnAction(new EventHandler<ActionEvent>(){

                    @Override public void handle(ActionEvent event) {
                        confirmDialog.close();
                        dialog.close();
                        initializeCriteria();
                        if (criteriaTable.getItems().isEmpty()) {
                            initializeCriteria();
                        } else {
                            ObservableList<StringProperty> addCriteria;
                            criteriaTable.getItems().clear();
                            for(int i = 0; i < cbs.length; i++) {
                                int selectedCounter = 0;
                                for (int j = 0; j < cbs[i].length; j++) {
                                    if (cbs[i][j].isSelected()) {
                                        selectedCounter++;
                                    }
                                }
                                if (selectedCounter > 0) {
                                    addCriteria = FXCollections.observableArrayList();
                                    addCriteria.add(new SimpleStringProperty(names[i]));
                                    //addCriteria.add(new SimpleStringProperty(changedTolerances[i]));;
                                    //addCriteria.add(new SimpleStringProperty("FALSE"));
                                    //addCriteria.add(new SimpleStringProperty("FALSE"));
                                    addCriteria.add(new SimpleStringProperty());
                                    criteriaTable.getItems().add(addCriteria);

                                    if (selectedCounter < cbs[i].length) {
                                        int savedTableCounter = 0;
                                        savedTableCounter = findColumnIndex(studentTable,names[i]);
                                        for (int z = 0;z<studentTable.getItems().size();z++) {
                                            boolean flag = false;
                                            for (int j = 0; j < cbs[i].length; j++) {
                                                if (cbs[i][j].isSelected() == false && studentTable.getVisibleLeafColumn(savedTableCounter).getCellObservableValue(z).getValue().toString().trim().equals(cbs[i][j].getText())) {
                                                    flag = true;
                                                }
                                            }
                                            if (flag == true) {
                                                setCellValue(studentTable, z, savedTableCounter,"Other");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                });

                dialog.close();
            }
        });
    }

    public void initializeCriteria(){
        criteriaTable.getColumns().clear();
        String[] priorityChoices = new String[businessUnitTable.getItems().size()];
        for(int i = 0; i < priorityChoices.length; i ++){
            if(i == 0){
                priorityChoices[i] = "";
            }
            else {
                int priority = i;
                priorityChoices[i] = priority + "";
            }
        }

        criteriaTable.getColumns().addAll(
                tableColumnCreator.createColumn(0, "Criteria"),
                tableColumnCreator.createColumn(1, "Choice Priority", priorityChoices));
        criteriaSetupThing();
        ObservableList < SimpleStringProperty > criteriaInfo;
        criteriaTable.getItems().clear();
        for(int i = 0; i<checkCriteria.size(); i++) {
            criteriaInfo = FXCollections.observableArrayList();
            SimpleStringProperty ssp = new SimpleStringProperty(checkCriteria.get(i));
            criteriaInfo.add(ssp);
            criteriaInfo.add(new SimpleStringProperty(""));
            criteriaInfo.add(new SimpleStringProperty(""));
            criteriaInfo.add(new SimpleStringProperty(""));
            criteriaTable.getItems().add(criteriaInfo);
       }

    }

    private void criteriaSetupThing() {
        int b = findColumnIndex(studentTable,"Criteria");
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                criteriaTable.getVisibleLeafColumn(b).setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<ObservableList<StringProperty>, String>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<ObservableList<StringProperty>, String> event) {

                        boolean flag = true;
                        if (event.getNewValue().equals("")) {
                            Platform.runLater(() -> dialogueCreator.createErrorDialogue("Criteria cannot be empty", "Please add a non-blank name"));

                            flag = false;
                        }
                        int b = findColumnIndex(criteriaTable,"Criteria");

                        for (int i = 0; i < criteriaTable.getItems().size(); i++) {
                            if (criteriaTable.getVisibleLeafColumn(b).getCellObservableValue(i).getValue().toString().equals(event.getNewValue()) && event.getTableView().getSelectionModel().getFocusedIndex() != i) {
                                Platform.runLater(() -> dialogueCreator.createErrorDialogue("Criteria cannot have duplicate names", "Please enter a unique criterion name"));

                                flag = false;
                            } else {
                                setCellValue(criteriaTable, event.getTableView().getSelectionModel().getFocusedIndex(), 0, event.getOldValue());
                            }
                        }

                        if (flag == true) {
                            for (int z = 0; z < studentTable.getColumns().size(); z++) {
                                if (studentTable.getVisibleLeafColumn(z).getText().equals(event.getOldValue())) {
                                    ObservableList<SimpleStringProperty> data = FXCollections.observableArrayList();
                                    for (int j = 0; j < totalStudents; j++) {
                                        data.add(new SimpleStringProperty(studentTable.getVisibleLeafColumn(z).getCellObservableValue(j).getValue().toString()));
                                    }
                                    studentTable.getColumns().remove(z, z + 1);
                                    addColumn(studentTable, event.getNewValue(), z, data);
                                }
                            }
                            setCellValue(criteriaTable, event.getTableView().getSelectionModel().getFocusedIndex(), 0, event.getNewValue());
                        }
                        criteriaTable.refresh();
                        //event.consume();
                    }
                });
            }
        });
    }

    @FXML
    private void addCriteria(ActionEvent event) throws IOException{
        if(businessUnitTable.getItems().size()==0){
            dialogueCreator.createErrorDialogue("No Business Unit Information Detected", "Please specify " +
                    "business unit information before identifying criteria ");
            viewBusinessUnits();
        }
        else{
            viewCriteria();
            fetchCriteria();
        }
    }

    @FXML
    private void viewCriteria() {
        if (studentTable.getItems().isEmpty()) {
            criteriaTable.setPlaceholder(new Label("Import a data file."));
            criteriaTable.toFront();
        } else {
            criteriaTable.setPlaceholder(new Label("Add Some Criteria."));
            criteriaTable.toFront();
        }
        loadScreenAccordion.setExpandedPane(criteriaPane);
    }

    @FXML
    public void deleteCriterion(ActionEvent event) throws IOException {
        viewCriteria();
        Object selectedRecord = criteriaTable.getSelectionModel().getSelectedItem();
        int selectedIdx = criteriaTable.getSelectionModel().getSelectedIndex();
        if (!criteriaTable.getItems().isEmpty()) {
            if(selectedRecord != null){
                Optional<ButtonType> result = dialogueCreator.createConfirmDialogue("Delete Evaluation Criterion",
                        "Are you sure that you want to delete this criteria?");
                if (result.get() == ButtonType.OK){
                    criteriaTable.getItems().remove(selectedRecord);
                    checkCriteria.remove(selectedIdx);
                }
                if (criteriaTable.getItems().isEmpty()) {
                    criteriaTable.setPlaceholder(new Label("All data has been deleted."));
                }
            }else{
                dialogueCreator.createErrorDialogue("No Criterion Selected",
                        "Please selected an Evaluation Criterion before attempting to delete one.");
            }

        } else {
            System.err.println("ERROR: TABLE EMPTY");
            dialogueCreator.createErrorDialogue("No Criteria To Delete",
                    "The Evaluation Criteria table contains no records, therefore nothing can be deleted.");
        }
    }

    private void formatCriteriaDialog(Accordion acord, int i, String[] names,GridPane grid) {
        TitledPane aPane = new TitledPane();
        aPane.setText(names[i]);

        ToolBar aToolBar = new ToolBar();
        aToolBar.getItems().add(grid);

        VBox aBox = new VBox();
        aBox.getChildren().add(aToolBar);
        aBox.setPadding(new Insets(0.0,0.0,0.0,0.0));
        aToolBar.setBackground(Background.EMPTY);

        aPane.setContent(aBox);

        acord.getPanes().add(aPane);
    }


    //STUDENT RECORDS
    @FXML
    private void addStudentRecord(ActionEvent event) throws IOException {
        viewStudentRecords();
        if (studentTable.getItems().size() > 0) {
            ObservableList<StringProperty> data = FXCollections.observableArrayList();
            for(int i=0; i<studentTable.getColumns().size(); i++){
                data.add(new SimpleStringProperty(""));
            }
            studentTable.getItems().add(data);
            totalStudents++;
            loadScreenOutput.appendText("\nNew Total Student Records\t\t\t: " + totalStudents);
            studentTable.getSelectionModel().selectLast();
            studentTable.scrollTo(studentTable.getItems().size());
        } else {
            System.err.println("ERROR: NO ROW SELECTED");
            loadScreenOutput.appendText("\nError: No Data has been Loaded.");
        }
    }

    @FXML
    private void viewStudentTable(MouseEvent event) throws IOException{
        studentTable.toFront();
    }

    @FXML
    private void viewStudentRecords() throws IOException {
        if (!studentPane.isExpanded()) {
            studentTable.toFront();
            loadScreenAccordion.setExpandedPane(studentPane);
        }
    }

    @FXML
    private void deleteStudentRecord(ActionEvent event) throws IOException {
        viewStudentRecords();
        Object selectedRecord = studentTable.getSelectionModel().getSelectedItem();
        if (selectedRecord != null) {
            Optional<ButtonType> result = dialogueCreator.createConfirmDialogue("Delete Student Record",
                    "Are Are you sure you want to delete this students record?");
            if (result.get() == ButtonType.OK){
                studentTable.getItems().remove(selectedRecord);;
                studentPopulation = studentTable.getItems();
                totalStudents--;
                loadScreenOutput.appendText("\nNew Total Student Records\t\t\t: " + totalStudents);
            }
            if (studentTable.getItems().isEmpty()) {
                studentTable.setPlaceholder(new Label("All data has been deleted."));
            }
        } else {
            System.err.println("ERROR: NO ROW SELECTED");
            dialogueCreator.createErrorDialogue("No Record Selected",
                    "You haven't selected which student record you would like to delete.");
        }
    }


    //USER MANUAL
    @FXML
    private void viewManual() {
        try {
            String inputPdf = "res/KPMG SAS User Manual.pdf";
            Path tempOutput = Files.createTempFile("TempManual", ".pdf");
            tempOutput.toFile().deleteOnExit();
            try (InputStream is = IEProjectMain.class.getClassLoader().getResourceAsStream(inputPdf)) {
                Files.copy(is, tempOutput, StandardCopyOption.REPLACE_EXISTING);
            }
            Desktop.getDesktop().open(tempOutput.toFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //CLOSING OF THE APPLICATION
    @FXML
    private void closeApp(ActionEvent event) throws IOException{
        if (!isDataLoaded || hasBeenSaved) {
            System.exit(0);
        } else {
            ButtonType btnYes = new ButtonType("Yes");
            ButtonType btnNo = new ButtonType("No");
            Optional<ButtonType> result = dialogueCreator.createCustomConfirmDialogue("This Project has not yet been saved",
                            "Would you like to save it before you exit?\n" +
                            "\nNote that any unsaved changes will be lost.", btnYes, btnNo);
            if (result.get() == btnYes) {
                ArrayList<TableView> theTables = new ArrayList<>();
                theTables.add(studentTable);
                theTables.add(businessUnitTable);
                theTables.add(criteriaTable);
                boolean success = IEProjectMain.projectManager.saveProject(hasBeenSaved, theTables, loadScreenOutput,
                        btnLoadData.getScene().getWindow());
                if (success) {
                    dialogueCreator.createInfoDialogue(true, "Project has been saved.", "The application will " +
                            "now be closed.");
                    System.exit(0);
                }
            } else if (result.get() == btnNo) {
                System.exit(0);
            }
        }
    }


    //ADD EXTRA COLUMNS
    private void addExtraColumns(){
        //----------------------notes----------------------

        noteData = FXCollections.observableArrayList();
        for(int i = 0; i < totalStudents; i++){
            noteData.add(new SimpleStringProperty(" "));
        }
        addColumn(studentTable, "Notes", 3,noteData);

        //---------------------Extra Notes-------------------
        extraNoteData = FXCollections.observableArrayList();
        for(int i = 0; i < totalStudents; i++){
            extraNoteData.add(new SimpleStringProperty(" "));
        }
        addColumn(studentTable, "Extra Notes", 4,extraNoteData);

        if(Arrays.asList(headerValues).contains("Auditing") && Arrays.asList(headerValues).contains("Financial Accounting") &&
                Arrays.asList(headerValues).contains("Management Accounting and Finance")&& Arrays.asList(headerValues).contains("Taxation")){
            //---------------------- average--------------------
            ArrayList<Integer> auditing = new ArrayList<>(); ArrayList<Integer> financialAcc = new ArrayList<>();
            ArrayList<Integer> managementAcc = new ArrayList<>(); ArrayList<Integer> taxation = new ArrayList<>();
            ArrayList<String> averageList = new ArrayList<>();
            for(int col = 0; col < numHeaders; col++){
                String colName = studentTable.getVisibleLeafColumn(col).getText();
                if(colName.equals("Auditing")){
                    for(int row = 0; row < totalStudents; row++){
                        String currentValue = studentTable.getVisibleLeafColumn(col).getCellObservableValue(row).getValue().toString().trim();
                        int value = Integer.parseInt(currentValue);
                        auditing.add(value);
                    }
                }else if(colName.equals("Financial Accounting")){
                    for(int row = 0; row < totalStudents; row++){
                        String currentValue = studentTable.getVisibleLeafColumn(col).getCellObservableValue(row).getValue().toString().trim();
                        int value = Integer.parseInt(currentValue);
                        financialAcc.add(value);
                    }
                }else if(colName.equals("Management Accounting and Finance")){
                    for(int row = 0; row < totalStudents; row++){
                        String currentValue = studentTable.getVisibleLeafColumn(col).getCellObservableValue(row).getValue().toString().trim();
                        int value = Integer.parseInt(currentValue);
                        managementAcc.add(value);
                    }
                }else if(colName.equals("Taxation")){
                    for(int row = 0; row < totalStudents; row++){
                        String currentValue = studentTable.getVisibleLeafColumn(col).getCellObservableValue(row).getValue().toString().trim();
                        int value = Integer.parseInt(currentValue);
                        taxation.add(value);
                    }
                }

            }
            for(int i = 0; i < totalStudents; i++){
                int total = (auditing.get(i) + financialAcc.get(i) + managementAcc.get(i) + taxation.get(i))/4;
                averageValues.add(total);
                String avg = Integer.toString(total);
                averageList.add(avg);
            }

            averageData = FXCollections.observableArrayList();
            for(String avg:averageList){
                averageData.add(new SimpleStringProperty(avg));
            }
            addColumn(studentTable, "Average", 15, averageData);

            //-------------------------Quartile---------------------------

            int quartileVal = 0;
            ArrayList<String> quartileList = new ArrayList<>();
            for(int i = 0; i<averageValues.size() ; i++){
                if(averageValues.get(i)<=55){
                    quartileVal = 4;
                    String quart = Integer.toString(quartileVal);
                    quartileList.add(quart);
                }else if(averageValues.get(i)<=60){
                    quartileVal = 3;
                    String quart = Integer.toString(quartileVal);
                    quartileList.add(quart);
                }else if(averageValues.get(i)<=65){
                    quartileVal = 2;
                    String quart = Integer.toString(quartileVal);
                    quartileList.add(quart);
                }else{
                    quartileVal = 1;
                    String quart = Integer.toString(quartileVal);
                    quartileList.add(quart);
                }
            }

            quartileData = FXCollections.observableArrayList();
            for(String quart:quartileList){
                quartileData.add(new SimpleStringProperty(quart));
            }
            addColumn(studentTable, "Quartile", 16, quartileData);
        }
    }

    protected void addColumn(TableView  table, String colName, int pos,ObservableList<SimpleStringProperty> data){

        ObservableList<SimpleStringProperty>  tableData = table.getItems();
        ObservableList<ObservableList>  newData = FXCollections.observableArrayList();
        ObservableList<TableColumn> newTableColumns = FXCollections.observableArrayList();

        boolean isFirstIteration = true;
        for(int row = 0; row < tableData.size(); row ++){
            ObservableList<StringProperty>  dataRow = FXCollections.observableArrayList();
            for(int col = 0; col < table.getColumns().size() + 1;col ++){
                if(col < pos){

                    dataRow.add(new SimpleStringProperty(table.getVisibleLeafColumn(col).getCellObservableValue(row).getValue().toString()));

                    if(isFirstIteration){
                        newTableColumns.add(
                                tableColumnCreator.createColumn(col,table.getVisibleLeafColumn(col).getText()));
                    }

                }
                else if(col == pos){
                    dataRow.add(data.get(row));

                    if(isFirstIteration)
                        newTableColumns.add(
                                tableColumnCreator.createColumn(col,colName));
                }
                else{
                    dataRow.add(new SimpleStringProperty(table.getVisibleLeafColumn(col-1).getCellObservableValue(row).getValue().toString()));

                    if(isFirstIteration)
                        newTableColumns.add(
                                tableColumnCreator.createColumn(col,table.getVisibleLeafColumn(col-1).getText()));
                }

            }
            isFirstIteration = false;
            newData.add(dataRow);

        }
        table.getItems().clear();
        table.getColumns().clear();
        for(TableColumn col: newTableColumns)
            col.setMinWidth(100);
        table.getColumns().addAll(newTableColumns);
        table.setItems(newData);
        studentPopulation = table.getItems();
    }

    public void getTableTotals(ArrayList<ArrayList<Float>> matrix, ArrayList<Float> list){
       ArrayList<Float> totalsArray = new ArrayList<>();

       for(int row=0; row < matrix.size() ;row++){
           float total = 0;
            for(int col=0; col < matrix.get(row).size();col++){
                for(int i = 0; i < list.size(); i++){
                     float f = list.get(i) * matrix.get(row).get(col);
                     total = total + f;
                     col++;
                }
            }
            totalsArray.add(total);
        }
       for(float f:totalsArray){
           System.out.println(f);
       }

    }

    @FXML
    private void filterTable(){
        String selectedFilterOption = cmbFilter.getSelectionModel().getSelectedItem().toString();
        ObservableList<StringProperty> filteredData = FXCollections.observableArrayList();
        studentTable.setItems(studentPopulation);

        if(selectedFilterOption.equals("No Filter"))
            studentTable.setItems(studentPopulation);

        else if(selectedFilterOption.equals("Missing Values")){
            for(int col = 0; col < numHeaders;col++){
                for(int row =0;row < studentPopulation.size();row++){
                    String currentValue = studentTable.getVisibleLeafColumn(col).getCellObservableValue(row).getValue().toString();
                    if(currentValue.equals("") && !filteredData.contains(studentPopulation.get(row)))
                        filteredData.add(studentPopulation.get(row));
                }
            }
            studentTable.setItems(filteredData);
        }

        else{
            for(int col = 0; col < numHeaders;col++){
                for(int row =0;row < studentPopulation.size();row++){
                    String currentValue = studentTable.getVisibleLeafColumn(col).getCellObservableValue(row).getValue().toString();
                    if(currentValue.equals(selectedFilterOption))
                        filteredData.add(studentPopulation.get(row));
                }
            }
            studentTable.setItems(filteredData);
        }
    }

    @FXML
    private void handleSearchActivation() {
        if (isDataLoaded) {
            if (!topFlapBar.isVisible()) {
                topFlapBar.displayPanel();
                tfSearch.requestFocus();
            } else {
                topFlapBar.hidePanel();
                tfSearch.clear();
                searchFilter(studentTable);
            }
        }
    }

    @FXML
    public void startSearch() {
        searchFilter(studentTable);
    }

    private void searchFilter(TableView aTable) {
        if(tfSearch.isFocused()&& !tfSearch.getText().equals("")) {
            ObservableList<StringProperty> filteredData = FXCollections.observableArrayList();
            studentTable.setItems(studentPopulation);
            for (int col = 0; col < aTable.getColumns().size(); col++) {
                for (int row = 0; row < aTable.getItems().size(); row++){
                    String currentValue = studentTable.getVisibleLeafColumn(col).getCellObservableValue(row).getValue().toString();
                    if(currentValue.length() >= tfSearch.getText().length()) {
                        if (tfSearch.getText().equals(currentValue.substring(0, tfSearch.getText().length()))) {
                            if(!filteredData.contains(studentPopulation.get(row))){
                                filteredData.add(studentPopulation.get(row));
                            }
                        }
                    }
                }
            }
            aTable.setItems(filteredData);
        }
        else
            aTable.setItems(studentPopulation);
    }

    /*-----Table functionality-----*/
    public static void setCellValue(TableView table, int row, int column, String value) {
        ObservableList<StringProperty> aRow = (ObservableList<StringProperty>)table.getItems().get(row);
        aRow.set(column,new SimpleStringProperty(value));
        table.refresh();
    }

    public static int findColumnIndex(TableView table, String name) {
        int savedTableCounter = 0;
        for (int z = 0; z < table.getColumns().size();z++) {
            if (table.getVisibleLeafColumn(z).getText().trim().equals(name.trim())) {
                savedTableCounter = z;
                break;
            }
        }
        return savedTableCounter;
    }

    /*-----DUPLICATED CODE-----*/
    private ArrayList<String> getNames (ObservableList<ArrayList<String>> table, int column) {

        ArrayList<String> uniqueValues = new ArrayList<String>();
        for (int row=0;row<table.size();row++) {
            boolean flag = false;
            for (int j=0;j<uniqueValues.size();j++) {
                if (table.get(row).get(column).equals(uniqueValues.get(j))) {
                    flag = true;
                }
            }
            if (flag == false) {
                uniqueValues.add(table.get(row).get(column));
            }
        }
        return uniqueValues;
    }

    public ObservableList<ArrayList<String>> obtainColumnData(){
        ArrayList<String> returnArray = new ArrayList<>();
        ObservableList<ArrayList<String>> tempColumn = FXCollections.observableArrayList();
        for(int row = 0; row < totalStudents; row++){
            //tempColumn = FXCollections.observableArrayList();

            //int colName = columnName;//studentTable.getVisibleLeafColumn(columnName).getText();
            //if(colName.equals("Business Unit Preference Option1")){
            returnArray.clear();
            for(int col = 0; col < numHeaders; col++){
                String currentValue = studentTable.getVisibleLeafColumn(col).getCellObservableValue(row).getValue().toString().trim();
                returnArray.add(currentValue);

                //System.out.println(returnArray.add(currentValue));
            }
            tempColumn.add(new ArrayList<String>(returnArray));
            // }
        }

        return tempColumn;
    }
}
