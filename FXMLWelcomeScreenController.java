/*
 * TEAM 3 INDUSTRIAL EXPERIENCE PROJECT
 * KPMG Student Allocation System Created by Monash University South Africa's IE Team 3
 * 2016 (c) KPMG & Monash University
 */
package IE_Project;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.stage.WindowEvent;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

import static IE_Project.IEProjectMain.loadScreen;
import static IE_Project.IEProjectMain.loadScreenFile;

/**
 *
 * @author TEAM 3
 */
public class FXMLWelcomeScreenController implements Initializable, ControlledStage {

    @FXML private Button btnOpenProject;

    private StageController projectController;
    private DialogueCreator dialogueCreator;
    private boolean loadScreenLoaded;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        dialogueCreator = new DialogueCreator(new Image(getClass().getResourceAsStream("kpmg_icon.png")));
        loadScreenLoaded = false;
    } 
    
    @Override
    public void setStageParent(StageController stageParent){
        projectController = stageParent;
    }
    
    @FXML
    private void startNewProject() throws IOException {
        goToLoadScreen();
    }   

    @FXML
    private void loadPrevProject() throws IOException {
        boolean success = IEProjectMain.projectManager.setProject(btnOpenProject.getScene().getWindow());
        if (success) {
            goToLoadScreen();
        }
    }

    private void  goToLoadScreen() {
        if (loadScreenLoaded) {
            projectController.setStage(loadScreen);
        } else {
            IEProjectMain.mainContainer.loadStage(loadScreen, loadScreenFile);
            projectController.setStage(loadScreen);
            loadScreenLoaded = true;
        }
        Node node = (Node) projectController.getStage("welcomeScreen");
        node.getScene().getWindow().setOnCloseRequest(event -> {
            Optional<ButtonType> result = dialogueCreator.createConfirmDialogue("Are You Sure?",
                    "Any unsaved changes will be lost.");
            if (result.get() == ButtonType.OK)
                System.exit(0);
            else
                event.consume();
        });
    }
}
