/*
 * TEAM 3 INDUSTRIAL EXPERIENCE PROJECT
 * KPMG Student Allocation System Created by Monash University South Africa's IE Team 3
 * 2016 (c) KPMG & Monash University
 */
package IE_Project;

import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.chart.PieChart;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

/**
 * FXML Controller class
 *
 * @author TEAM 3
 */
public class FXMLResultsScreenController implements Initializable, ControlledStage {

    @FXML private TextArea studentDistTextArea;
    @FXML private TabPane resultsTabPane;
    @FXML private Button backBtn;
    @FXML private GridPane progressGrid;
    @FXML private VBox mainVbox;

    private int numPrevTabs;
    private boolean tablesCreated = false;
    private ArrayList<TableView> classTables;
    private ArrayList<String> headerValues;
    private ObservableList<String> dataBusUnits;
    private ArrayList<TextArea> textAreaList;
    private ArrayList<TitledPane> titledPanesList;
    private ArrayList<ArrayList<Double>> listOfListChoices;
    private ObservableList<StringProperty> selectedRow;
    private TableView previousTable;
    private ListView<String> criteriaList;
    private ArrayList<Integer> criteriaColumns;
    private TableView studentTable;
    private ObservableList<String> dataBusinessUnits;
    private ArrayList<Float> distCounter, stuCounter;
    private ArrayList<Integer> posList;
    private ArrayList<String> statNameList, uniqueVStu, uniqueV;

    private StageController projectController;
    private DialogueCreator dialogueCreator;
    private TableColumnCreator tableColumnCreator;
    private String firstChoice, secondChoice, thirdChoice, mostPopular, fChoice, sChoice, tChoice;
    private final Image backIcon = new Image(getClass().getResourceAsStream("back-icon.png"));
    public final ProgressIndicator progressIndicator = new ProgressIndicator();

    //private ArrayList<String> uniqueV;
    //private ArrayList<Float> stuCounter;

    @Override
    public void setStageParent(StageController stageParent) {
        projectController = stageParent;
    }

    @FXML
    private void viewLoadScreen(ActionEvent event) throws IOException {
        ArrayList<String> list = new ArrayList<>();
        FXMLLoadScreenController aController = (FXMLLoadScreenController) projectController.getScreenControllers(1);
        for(int i=0; i< studentTable.getColumns().size();i++){
            String colName = studentTable.getVisibleLeafColumn(i).getText().trim();
            list.add(colName);
        }
        if(!list.contains("Final Allocated BU")){
            aController.addColumn(studentTable,"Final Allocated BU",20,finalAllocationColumn(studentTable,classTables,dataBusinessUnits));
        }
        projectController.setStage(IEProjectMain.loadScreen);
    }

    @FXML
    private void closeApp(ActionEvent event) throws IOException {
        System.exit(0);
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        backBtn.setGraphic(new ImageView(backIcon));
        dialogueCreator = new DialogueCreator(new Image(getClass().getResourceAsStream("kpmg_icon.png")));
        tableColumnCreator = new TableColumnCreator();
    }

    public void col(ArrayList<String> headerValues, TableView aTable) {
        for (int column = 0; column < headerValues.size(); column++) {
            aTable.getColumns().add(tableColumnCreator.createColumn(column, headerValues.get(column)));
        }
    }

    public void createTable(ObservableList<ObservableList<ArrayList<String>>> bUnitList, ArrayList<String> headerValues, ArrayList<String> criteriaTable, TableView studentTable, ObservableList<String> dataBusUnits,TableView critTable) {
        //VBox vBox = (VBox) projectController.getStage("resultsScreen");
        ObservableList<Node> vBoxChildren = mainVbox.getChildren();
        this.dataBusUnits = dataBusUnits;
        SplitPane aSplitPane = null;
        for (Node aNode : vBoxChildren) {
            if (aNode instanceof SplitPane) {
                aSplitPane = (SplitPane) aNode;
                break;
            }
        }
        ObservableList<Node> splitPaneChildren = aSplitPane.getItems();

        TabPane aTabPane = null;
        for (Node aNode : splitPaneChildren) {
            if (aNode instanceof TabPane) {
                aTabPane = (TabPane) aNode;
                break;
            }
        }
        Tab aTab = aTabPane.getTabs().get(1);
        //VBox
        VBox aBox = new VBox();
        aTab.setContent(aBox);
        //Accordion
        Accordion aAccordion = new Accordion();
        aBox.getChildren().add(aAccordion);
        VBox.setVgrow(aAccordion, Priority.ALWAYS);
        //TitledPane
        TitledPane aTitledPane = new TitledPane();
        //TableView
        TableView aTableView = null;
        ObservableList<StringProperty> temp;
        for (int i = 0; i < bUnitList.size(); i++) {
            aTitledPane = new TitledPane();
            aTitledPane.setText(dataBusUnits.get(i));
            aTableView = new TableView();
            prepareTable(aTableView);
            col(headerValues, aTableView);
            for (int j = 0; j < bUnitList.get(i).size(); j++) {
                temp = FXCollections.observableArrayList();
                for (int k = 0; k < bUnitList.get(i).get(j).size(); k++) {
                    temp.add(new SimpleStringProperty(bUnitList.get(i).get(j).get(k)));
                }
                aTableView.getItems().add(temp);

            }
            aTitledPane.setContent(aTableView);

            //makes titledPanes expand when something is dragged over them
            TitledPane tempTitledPane = aTitledPane;
            tempTitledPane.onDragOverProperty().set(new EventHandler<DragEvent>() {
                @Override
                public void handle(DragEvent event) {
                    tempTitledPane.setExpanded(true);
                }
            });

            //events that controls the dragging, adding and removing actions of the tables
            TableView tempTableView = aTableView;

            aTableView.setOnDragDetected(new EventHandler<MouseEvent>() { //drag
                @Override
                public void handle(MouseEvent event) {
                    // drag was detected, start drag-and-drop gesture
                    selectedRow = (ObservableList<StringProperty>) tempTableView.getSelectionModel().getSelectedItem();
                    previousTable = tempTableView;
                    if (selectedRow != null) {

                        Dragboard db = tempTableView.startDragAndDrop(TransferMode.ANY);
                        ClipboardContent content = new ClipboardContent();
                        content.putString(selectedRow.toString());
                        db.setContent(content);

                        event.consume();
                    }
                }
            });

            aTableView.setOnDragOver(new EventHandler<DragEvent>() {
                @Override
                public void handle(DragEvent event) {
                    // data is dragged over the target
                    if (event.getDragboard().hasString()) {
                        event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
                    }
                    event.consume();
                }
            });

            aTableView.setOnDragDropped(new EventHandler<DragEvent>() {
                @Override
                public void handle(DragEvent event) {
                    //dropped on target, adds adds table and potentially removes it
                    boolean success = false;
                    if (event.getDragboard().hasString() && tempTableView != previousTable) {
                        tempTableView.getItems().add(selectedRow);
                        success = true;
                    }
                    event.setDropCompleted(success);
                    event.consume();
                    if (event.isDropCompleted()) {
                        previousTable.getItems().remove(previousTable.getItems().indexOf(selectedRow), previousTable.getItems().indexOf(selectedRow) + 1);
                        tempTableView.getSelectionModel().selectLast();
                        tempTableView.scrollTo(tempTableView.getItems().size());
                        updateStatistic(studentTable, dataBusUnits,critTable);
                        createGraphs(dataBusUnits,criteriaList,criteriaColumns);
                    }
                }
            });

            classTables.add(aTableView);
            aAccordion.getPanes().add(aTitledPane);
        }
    }

    public void createStatTab(ObservableList<ObservableList<ArrayList<String>>> bUnitList, ObservableList<String> dataBusUnits) {
        System.out.println("---------------------------------------------------------------------------------");
        //Node aNode = projectController.getStage("resultsScreen");
        //VBox vBox = (VBox) projectController.getStage("resultsScreen");
        System.out.println("VBox: " + mainVbox);
        ObservableList<Node> vBoxChildren = mainVbox.getChildren();
        //Scene aScene = vBox.getScene();
        //System.out.println("Scene: "+aScene);

        SplitPane aSplitPane = null;
        for (Node aNode : vBoxChildren) {
            if (aNode instanceof SplitPane) {
                aSplitPane = (SplitPane) aNode;
                break;
            }
        }
        ObservableList<Node> splitPaneChildren = aSplitPane.getItems();

        TabPane aTabPane = null;
        for (Node aNode : splitPaneChildren) {
            if (aNode instanceof TabPane) {
                aTabPane = (TabPane) aNode;
                break;
            }
        }
        System.out.println("aTabPane: " + aTabPane);
        //TabPane aTabPane = (TabPane) vBox.lookup("#resultsTabPane");
        //System.out.println("VBox lookup: "+ vBox.lookup("#resultsTabPane"));
        Tab aTab = aTabPane.getTabs().get(0);
        //VBox
        VBox aBox = new VBox();
        aTab.setContent(aBox);
        //Accordion
        Accordion aAccordion = new Accordion();
        aBox.getChildren().add(aAccordion);
        VBox.setVgrow(aAccordion, Priority.ALWAYS);
        //TitledPane
        TitledPane aTitledPane = null;
        TitledPane studentPopulatioPane = null;

        //TableView
        TextArea txt = null;

        studentDistTextArea = new TextArea();
        studentPopulatioPane = new TitledPane();
        studentPopulatioPane.setText("Student Population");
        studentPopulatioPane.setContent(studentDistTextArea);
        aAccordion.getPanes().add(studentPopulatioPane);

        for (int i = 0; i < bUnitList.size(); i++) {
            aTitledPane = new TitledPane();
            aTitledPane.setText(dataBusUnits.get(i));
            txt = new TextArea();
            aTitledPane.setContent(txt);
            textAreaList.add(txt);
            titledPanesList.add(aTitledPane);
            aAccordion.getPanes().add(aTitledPane);
        }
    }

    public void loadClassLists(ObservableList<ObservableList<ArrayList<String>>> bUnitList, ArrayList<String> headerValues,
                               ObservableList<String> dataBusUnits, ArrayList<String> criteriaTable, TableView studentTable,TableView critTable) {
        this.headerValues = headerValues;
        classTables = new ArrayList<>();
        textAreaList = new ArrayList<>();
        titledPanesList = new ArrayList<>();
        stuCounter = new ArrayList<Float>();
        statNameList = new ArrayList<>();
        distCounter = new ArrayList<Float>();
        uniqueV = new ArrayList<>();
        uniqueVStu = new ArrayList<>();
        posList = new ArrayList<>();
        createTable(bUnitList, headerValues, criteriaTable, studentTable, dataBusUnits,critTable);
        createStatTab(bUnitList,dataBusUnits);
        updateStatistic(studentTable,dataBusUnits,critTable);
    }

    //contains all methods call for the statistics functionality
    public void updateStatistic(TableView studentTable,ObservableList<String> dataBusUnits,TableView critTable){
        studentDistTextArea.clear();
        listOfListChoices = new ArrayList<>();
        int choiceOne,choiceTwo,choiceThree;
        choiceOne=choiceTwo=choiceThree = 0;
        Object rCOption;
        int ratioConstraint ;
        String choiceOneCol, choiceTwoCol, choiceThreeCol;
        choiceOneCol= choiceTwoCol= choiceThreeCol = "";
        ArrayList<String> criteriaTable = new ArrayList<>();
        ArrayList<String> colHeader = new ArrayList<>();
        for (int k = 0; k < classTables.size(); k++) {
            for (int j = 0; j < classTables.get(k).getColumns().size(); j++) {
                String colName = classTables.get(k).getVisibleLeafColumn(j).getText();
                colHeader.add(colName);
            }
        }

        for (int i = 0; i < critTable.getItems().size() ; i++) {
            rCOption = critTable.getVisibleLeafColumn(1).getCellObservableValue(i).getValue();
            if(!(rCOption == null) && !rCOption.equals(" ")){
                ratioConstraint  = Integer.parseInt(rCOption.toString().trim());
                if(ratioConstraint == 1){
                    choiceOneCol = critTable.getVisibleLeafColumn(0).getCellObservableValue(i).getValue().toString().trim();
                }else if(ratioConstraint == 2){
                    choiceTwoCol = critTable.getVisibleLeafColumn(0).getCellObservableValue(i).getValue().toString().trim();
                }else if(ratioConstraint == 3){
                    choiceThreeCol = critTable.getVisibleLeafColumn(0).getCellObservableValue(i).getValue().toString().trim();
                }
            }
        }

        for (int i = 0; i < classTables.size(); i++) {
            for (int j = 0; j < classTables.get(i).getColumns().size(); j++) {
                String colName = classTables.get(i).getVisibleLeafColumn(j).getText();
                if (colName.equals("Business Unit Preference Option1") || colName.equals("BU1") || colName.equals(choiceOneCol)) {
                    choiceOne = colHeader.indexOf(colName);
                } else if (colName.equals("Business Unit Preference Option2") || colName.equals("BU2")|| colName.equals(choiceTwoCol)) {
                    choiceTwo = colHeader.indexOf(colName);
                } else if (colName.equals("Business Unit Preference Option3") || colName.equals("BU3")|| colName.equals(choiceThreeCol)) {
                    choiceThree = colHeader.indexOf(colName);
                }
            }
        }

        //get the first,second and third choice columns distribution
        getChoice(studentChoice(classTables, choiceOne, dataBusUnits));
        getChoice(studentChoice(classTables, choiceTwo, dataBusUnits));
        getChoice(studentChoice(classTables, choiceThree, dataBusUnits));

        //count students first choice column
        countChoice(getClassDistribution(studentTable, choiceOne, getNames(studentTable, choiceOne)), getNames(studentTable, choiceOne), studentDistTextArea, listOfListChoices, studentTable);

        for (int col = 0; col <critTable.getColumns().size() ; col++) {
            String colName = critTable.getVisibleLeafColumn(col).getText().trim();
            if(colName.equals("Criteria")){
                for (int row = 0; row <critTable.getItems().size() ; row++) {
                    String currentValue = critTable.getVisibleLeafColumn(col).getCellObservableValue(row).getValue().toString().trim();
                    criteriaTable.add(currentValue);
                }
            }
        }

        for (int i = 0; i < criteriaTable.size(); i++) {
            if (colHeader.contains(criteriaTable.get(i))) {
                int columnPos = colHeader.indexOf(criteriaTable.get(i));
                posList.add(columnPos);
                statNameList.add(criteriaTable.get(i));
            }
        }

        //prints the 1st, 2nd and 3rd choice in each class
        for (int i = 0; i < classTables.size(); i++) {
            printChoice(classTables.get(i), textAreaList.get(i), listOfListChoices.get(0).get(i), listOfListChoices.get(1).get(i), listOfListChoices.get(2).get(i));
        }

        //prints the criteria distribution in each class
        for (int l = 0; l < classTables.size(); l++) {
            for (int i = 0; i < posList.size(); i++) {
                textAreaList.get(l).appendText("\n" + "--------" + statNameList.get(i) + "------------" + "\n");
                printClassDistribution(getClassDistribution(classTables.get(l), posList.get(i), getNames(classTables.get(l), posList.get(i))), textAreaList.get(l), getNames(classTables.get(l), posList.get(i)));
            }
        }

        //prints the criteria distribution in the student population pane
        for (int i = 0; i < posList.size(); i++) {
            studentDistTextArea.appendText("\n" + "--------" + statNameList.get(i) + "------------" + "\n");
            printClassDistribution(getClassDistribution(studentTable, posList.get(i), getNames(studentTable, posList.get(i))), studentDistTextArea, getNames(studentTable, posList.get(i)));
        }

    }

    //---------------this method gets the distribution of each sub-criteria-----------------------------------------
    private ArrayList<Float> getClassDistribution(TableView table, int column, ArrayList<String> uniqueValues) {


        ArrayList<Float> distributionCounter = new ArrayList<Float>();
        for (int i = 0; i < uniqueValues.size(); i++) {
            distributionCounter.add(0f);
        }
        for (int row = 0; row < table.getItems().size(); row++) {
            boolean flag = false;
            for (int j = 0; j < uniqueValues.size(); j++) {
                if (table.getVisibleLeafColumn(column).getCellObservableValue(row).getValue().toString().trim().equals(uniqueValues.get(j))) {
                    flag = true;
                    distributionCounter.set(j, distributionCounter.get(j) + 1f);
                }
            }
        }
        for (int j = 0; j < distributionCounter.size(); j++) {
            distributionCounter.set(j, distributionCounter.get(j) / table.getItems().size() * 100);
        }
        return distributionCounter;
    }

    //---------------this method gets the name of each sub-criteria-----------------------------------------
    private ArrayList<String> getNames(TableView table, int column) {
        ArrayList<String> uniqueValues = new ArrayList<String>();
        for (int row = 0; row < table.getItems().size(); row++) {
            String rowVal = table.getVisibleLeafColumn(column).getCellObservableValue(row).getValue().toString().trim();
            boolean flag = false;
            for (int j = 0; j < uniqueValues.size(); j++) {
                if (rowVal.equals(uniqueValues.get(j))) {
                    flag = true;
                }
            }
            if (flag == false) {
                uniqueValues.add(rowVal);
            }
        }
        return uniqueValues;
    }

    //---------------this method prints the distribution of each sub-criteria-----------------------------------------
    public void printClassDistribution(ArrayList<Float> list, TextArea txt, ArrayList<String> uniqueValues) {
        //txt.appendText("----------------------------\n");
        for (int j = 0; j < list.size(); j++) {
            txt.appendText("" + uniqueValues.get(j) + ":" + " " + new DecimalFormat("#.00").format(list.get(j)) + "%" + "\n");
        }
        //txt.appendText("----------------------------\n");
    }

    //---------------this method counts the choices of each student-----------------------------------------
    public ArrayList<Double> studentChoice(ArrayList<TableView> table, int columnPos, ObservableList<String> dataBusUnits) {

        double count = 0;
        ArrayList<Double> choiceCountList = new ArrayList<>();
        choiceCountList.clear();
        for (int i = 0; i < titledPanesList.size(); i++) {

            //if(titledPanesList.get(i).getText().equals(dataBusUnits.get(i))){
            for (int row = 0; row < table.get(i).getItems().size(); row++) {

                String val = table.get(i).getVisibleLeafColumn(columnPos).getCellObservableValue(row).getValue().toString().trim();
                if (val.equals(dataBusUnits.get(i))) {
                    //System.out.println("val " + val + "=" + titledPanesList.get(i).getText());
                    count++;
                    //System.out.println("count: " + count);
                } else if (val.contains(dataBusUnits.get(i))) {
                    int start = val.lastIndexOf(dataBusUnits.get(i));
                    int length = dataBusUnits.get(i).length();
                    String value = val.substring(start, start + length);
                    if (value.equals(dataBusUnits.get(i))) {
                        count++;
                    } else {
                        System.out.println("val " + value + "!=" + titledPanesList.get(i).getText());
                    }
                }
            }
            choiceCountList.add(count);
            count = 0;
        }
        return choiceCountList;
    }

    //---------------this method gets the choices of each student-----------------------------------------
    public ArrayList<ArrayList<Double>> getChoice(ArrayList<Double> array) {
        listOfListChoices.add(array);
        return listOfListChoices;
    }

    //---------------this method prints the choices of each student-----------------------------------------
    public void printChoice(TableView table, TextArea txtArea, double varOne, double varTwo, double varThree) {
        txtArea.clear();
        txtArea.appendText("Business unit class size: " + table.getItems().size());
        txtArea.appendText("\n---------------------------------------------------------------------");
        txtArea.appendText("\nNumber of students who were allocated their first choice :" + varOne);
        txtArea.appendText("\nNumber of students who were allocated their second choice :" + varTwo);
        txtArea.appendText("\nNumber of students who were allocated their third choice :" + varThree + "\n");
    }

    public void printChoiceReport(TableView table, XWPFRun run, double varOne, double varTwo, double varThree) {
        run.setText("Business unit class size: " + table.getItems().size());
        run.addBreak();
        run.setText("\n---------------------------------------------------------------------");
        run.addBreak();
        run.setText("\nNumber of students who were allocated their first choice :" + varOne);
        run.addBreak();
        run.setText("\nNumber of students who were allocated their second choice :" + varTwo);
        run.addBreak();
        run.setText("\nNumber of students who were allocated their third choice :" + varThree + "\n");
        run.addBreak();
    }

    //---------------this method counts the choices of each student and prints the overall percentage -----------------------------------------
    public void countChoice(ArrayList<Float> array, ArrayList<String> uniqueValues, TextArea txt, ArrayList<ArrayList<Double>> list, TableView table) {
        ArrayList<Double> temp = new ArrayList<>();
        float max = array.get(0);
        double sum = 0;
        for (int i = 1; i < array.size(); i++) {
            if (array.get(i) > max) {
                max = array.get(i);
                txt.appendText("Most Popular Business Unit: " + uniqueValues.get(i) + "\n");
                mostPopular = uniqueValues.get(i);
            }
        }

        //temp.clear();
        for (int i = 0; i < list.size(); i++) {
            for (int j = 0; j < list.get(i).size(); j++) {
                sum += list.get(i).get(j);
            }
            System.out.println(sum);
            temp.add(sum);
            sum = 0;
        }
        System.out.println(table.getItems().size());
        firstChoice = String.format("%.2f%%",(temp.get(0)*100.0)/table.getItems().size());
        secondChoice = String.format("%.2f%%",(temp.get(1)*100.0)/table.getItems().size());
        thirdChoice = String.format("%.2f%%",(temp.get(2)*100.0)/table.getItems().size());

        txt.appendText("------------------------------------------\nOverall " + firstChoice +" of the entire population were allocated their first choice");
        txt.appendText("\nOverall " + secondChoice +" of the entire population were allocated their second choice");
        txt.appendText("\nOverall " + thirdChoice +" of the entire population were allocated their third choice \n");
    }

    @Override
    public String toString() {
        return "FXMLResultsScreenController{" +
                "projectController=" + projectController +
                '}';
    }

    /*-----Duplicated Code onwards-----*/
    private void prepareTable(TableView aTable) {
        aTable.getItems().clear();
        aTable.getColumns().clear();
        aTable.setPlaceholder(new Label("Import a data file."));
        aTable.setEditable(true);
    }
    
    public void createGraphs(ObservableList<String> dataBusUnits, ListView<String> criteria, ArrayList<Integer> criteriaColumnList) {
        if (tablesCreated == true){
            resultsTabPane.getTabs().remove(2,numPrevTabs);
        }

        criteriaList = criteria;
        criteriaColumns = criteriaColumnList;
        Tab aTab;
        GridPane aGridPane;

        for (int i = 0; i < classTables.size(); i++) {
            int x = 0, y = 0;
            aTab = new Tab();
            aTab.setText(dataBusUnits.get(i) + " Graphs");
            aGridPane = new GridPane();
            aGridPane.setGridLinesVisible(true);

            for (int j = 0; j < criteria.getItems().size(); j++) {
                ObservableList<PieChart.Data> distributionData = FXCollections.observableArrayList();
                String stringCriteria = criteria.getItems().get(j);
                ArrayList<String> subCriteria = getNames(classTables.get(i),criteriaColumnList.get(j));
                ArrayList<Float> distribution = getClassDistribution(classTables.get(i),criteriaColumnList.get(j),subCriteria);
                for (int k = 0; k < subCriteria.size(); k++) {
                    String stringSubCriteria = subCriteria.get(k);
                    if (stringSubCriteria.length() > 10){
                        stringSubCriteria = stringSubCriteria.substring(0,10) + "...";
                    }
                    float subDistribution = distribution.get(k);
                    distributionData.add(k, new PieChart.Data(stringSubCriteria, subDistribution));
                }
                if (x % 4 == 0) {
                    x = 0;
                    y++;
                }
                createChart(distributionData, aGridPane, dataBusUnits.get(i) + " " + stringCriteria + " Distribution", x, y, 1, 1);
                x++;
            }
            aTab.setContent(aGridPane);
            resultsTabPane.getTabs().add(i+2,aTab);
            numPrevTabs = i+3;
            tablesCreated = true;
        }
    }

    private void createChart(ObservableList<PieChart.Data> dataList, GridPane grid, String chartTitle, int posX, int posY, int spanX, int spanY) {
        final PieChart chart = new PieChart(dataList);
        chart.setTitle(chartTitle);
        chart.setLabelLineLength(10);
        chart.setLegendVisible(false);
        grid.add(chart, posX, posY, spanX, spanY);
        applyCustomColorSequence(dataList, "RoyalBlue", "DarkBlue", "Blue", "DodgerBlue", "DeepSkyBlue",
                "CornFlowerBlue", "MidnightBlue", "LightSkyBlue", "MediumTurquoise", "Navy");
        final Label caption = new Label("");
        caption.setTextFill(Color.BLACK);
        caption.setStyle("-fx-font: 14 arial;");
        caption.setPadding(new Insets(50, 0, 0, 10));
        grid.add(caption, posX, posY);

        viewPercentage(chart, caption);
    }
    
    private void applyCustomColorSequence(ObservableList<PieChart.Data> pieChartData, String... pieColors) {
        int i = 0;
        for (PieChart.Data data : pieChartData) {
            data.getNode().setStyle("-fx-pie-color: " + pieColors[i % pieColors.length] + ";");
            i++;
        }
    }

    private void viewPercentage(PieChart pieChart, Label label) {
        for (final PieChart.Data data : pieChart.getData()) {
            data.getNode().addEventHandler(MouseEvent.MOUSE_PRESSED, e -> {
                double total = 0;
                for (PieChart.Data d : pieChart.getData()) {
                    total += d.getPieValue();
                }
                label.setTranslateX(data.getChart().getTranslateX());
                label.setTranslateY(data.getChart().getTranslateY());
                String text = String.format("%.1f%%", 100 * data.getPieValue() / total);
                label.setText(text);
            });
        }
    }

    public ObservableList<SimpleStringProperty> finalAllocationColumn(TableView table,ArrayList<TableView> listTable,ObservableList<String> dataBusUnits){
        String[] list = new String[table.getItems().size()];
        ArrayList<String> studentTableNames = new ArrayList<>();
        ObservableList<SimpleStringProperty> finalAllocation = FXCollections.observableArrayList();
        String colName="";
        for(int col=0; col<table.getColumns().size();col++){
            colName = table.getVisibleLeafColumn(col).getText();
            if(colName.equals("Name") || colName.equals("First name")){
                for (int row = 0; row < table.getItems().size(); row++) {
                    String rowValue = table.getVisibleLeafColumn(col).getCellObservableValue(row).getValue().toString().trim();
                    String rowValueSec = table.getVisibleLeafColumn(col+1).getCellObservableValue(row).getValue().toString().trim();
                    String studentID = rowValue + rowValueSec;
                    studentTableNames.add(studentID);
                }
            }
        }

        for (int i = 0; i < listTable.size() ; i++) {
            for (int col = 0; col < listTable.get(i).getColumns().size(); col++) {
                colName = listTable.get(i).getVisibleLeafColumn(col).getText();
                if(colName.equals("Name") || colName.equals("First name")){
                    for (int row = 0; row < listTable.get(i).getItems().size(); row++) {
                        finalAllocation = FXCollections.observableArrayList();
                        String rowValue = listTable.get(i).getVisibleLeafColumn(col).getCellObservableValue(row).getValue().toString().trim();
                        String rowValueSec = listTable.get(i).getVisibleLeafColumn(col+1).getCellObservableValue(row).getValue().toString().trim();
                        String studentID = rowValue + rowValueSec;
                        if(studentTableNames.contains(studentID)) {
                            list[studentTableNames.indexOf(studentID)] = dataBusUnits.get(i);
                        }
                    }
                }

            }

        }

        for (int i = 0; i < list.length; i++) {
            finalAllocation.add(new SimpleStringProperty(list[i]));
        }
        return finalAllocation;
    }

    public void getTables(TableView sTable,ObservableList<String> dbTable){
        studentTable = sTable;
        dataBusinessUnits = dbTable;
    }

    @FXML
    public void saveClassLists() throws IOException
    {
        Stage secondaryStage = new Stage();
        FileChooser fileChooser = new FileChooser();

        fileChooser.setTitle("Save Class Lists");
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Excel Files", "*.xlsx"));
        fileChooser.setInitialDirectory(IEProjectMain.projectsDirectory);

        File newFile = fileChooser.showSaveDialog(secondaryStage);
        if (newFile != null) {
            progressGrid.add(progressIndicator,2,2);
            progressGrid.toFront();

            Runnable runnable = () -> {
                mainVbox.setDisable(true);
                try {
                    saveFile(classTables, headerValues, newFile);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            };
            Thread thread = new Thread(runnable);
            thread.start();

            Runnable test = () -> {
                boolean s = true;
                while(s) {
                    if (!thread.isAlive()) {
                        removeProgress(newFile);
                        s = false;
                    }
                }
            };

            new Thread(test).start();
        }
    }

    private void removeProgress(File newFile){
        Platform.runLater(() -> {
            mainVbox.setDisable(false);
            mainVbox.toFront();
            progressGrid.getChildren().remove(progressIndicator);
            dialogueCreator.createInfoDialogue(false, "", newFile.getName()+" Saved Successfully");

//            Alert alert = new Alert(Alert.AlertType.CONFIRMATION.INFORMATION);
//            alert.setTitle("Information Dialog");
//            alert.setHeaderText(null);
//            alert.setContentText(newFile.getName()+" Saved Successfully");
//
//            alert.showAndWait();
        });

    }

    @FXML
    private void saveAll() {
        try {
            saveStats();
            saveClassLists();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void saveFile(ArrayList<TableView> classTables, ArrayList<String> headerValues, File newFile) throws IOException
    {
        String header, columnValues;
        XSSFWorkbook classLists = new XSSFWorkbook();

        int headerPos = 0;

        XSSFCell cell;
        CellStyle style = classLists.createCellStyle();

        ArrayList<XSSFSheet> sheets = new ArrayList<>();
        ArrayList<XSSFRow> rows = new ArrayList<>();

        for(int bU = 0; bU < dataBusUnits.size(); bU++)
        {
            sheets.add(classLists.createSheet(dataBusUnits.get(bU)));
            rows.add(sheets.get(bU).createRow(headerPos));
        }

        for(int bU = 0; bU < dataBusUnits.size(); bU++)
        {
            for(int i = 0; i < headerValues.size(); i++)
            {
                header = headerValues.get(i);
                cell = rows.get(bU).createCell(i);
                cell.setCellValue(header);
                cell.setCellStyle(style);
                sheets.get(bU).autoSizeColumn(i);
            }
        }

        for(int bU = 0; bU < dataBusUnits.size(); bU++)
        {
            for (int aTable = 0; aTable < classTables.get(bU).getItems().size(); aTable++)
            {
                XSSFRow row = sheets.get(bU).createRow(aTable + 1);
                for (int column = 0; column < classTables.get(bU).getVisibleLeafColumns().size(); column++)
                {
                    columnValues = classTables.get(bU).getVisibleLeafColumn(column).getCellObservableValue(aTable).getValue().toString();
                    cell = row.createCell(column);
                    cell.setCellValue(columnValues);
                    cell.setCellStyle(style);
                }
            }
        }

        FileOutputStream out = new FileOutputStream(newFile);
        classLists.write(out);
        out.close();
        System.out.println(newFile.getName()+" written successfully"); //Confirmation Dialog!
    }
    @FXML
    private void saveStats() throws IOException

    {
        Stage secondaryStage = new Stage();
        FileChooser fileChooser = new FileChooser();

        fileChooser.setTitle("Save Report");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Microsoft Word Files", "*.docx"));
        fileChooser.setInitialDirectory(IEProjectMain.projectsDirectory);

        File newFile = fileChooser.showSaveDialog(secondaryStage);
        if (newFile != null) {
            saveDocFile(classTables, newFile);
        }
    }

    private void saveDocFile(ArrayList<TableView> classTables, File newFile) throws IOException
    {

        XWPFDocument wordDoc = new XWPFDocument();
        XWPFParagraph p1 = wordDoc.createParagraph();
        XWPFParagraph p2 = wordDoc.createParagraph();

        XWPFRun titleRun1 = p1.createRun();
        titleRun1.setFontFamily("Times New Roman");
        titleRun1.setBold(true);
        titleRun1.setItalic(true);
        titleRun1.setFontSize(16);

        XWPFRun run = p2.createRun();

        run.setFontFamily("Times New Roman");

        titleRun1.setText("Student Population");
        run.addBreak();
        run.setText("_________________________________________________");
        run.addBreak();

        run.addBreak();
        run.setText("Most Popular Business Unit: "+mostPopular);
        run.addBreak();
        run.addBreak();

        run.setText("Overall " + firstChoice +" of the entire population were allocated their first choice");
        run.addBreak();
        run.setText("Overall " + secondChoice +" of the entire population were allocated their second choice");
        run.addBreak();
        run.setText("Overall " + thirdChoice +" of the entire population were allocated their third choice \n");
        run.addBreak();

        for(int e = 0; e < posList.size(); e++)
        {
            uniqueVStu = getNames(studentTable, posList.get(e));
            stuCounter = getClassDistribution(studentTable, posList.get(e), uniqueVStu);
            run.addBreak();
            run.setText(statNameList.get(e));
            run.addBreak();
            run.setText("_________________________________________________");
            run.addBreak();
            for (int k = 0; k < stuCounter.size(); k++) {
                run.setText(uniqueVStu.get(k) + ": " +  new DecimalFormat("#.00").format(stuCounter.get(k))+ "%");
                run.addBreak();
            }
        }

        for(int i = 0; i < titledPanesList.size(); i++)
        {
            run.addBreak();
            run.setText("Business Unit Name: " + dataBusUnits.get(i));
            run.addBreak();
            run.setText("_________________________________________________");
            run.addBreak();

            printChoiceReport(classTables.get(i), run, listOfListChoices.get(0).get(i),listOfListChoices.get(1).get(i), listOfListChoices.get(2).get(i));

            run.setText("_________________________________________________");
            run.addBreak();


            for(int e = 0; e < posList.size(); e++)
            {
                uniqueV = getNames(classTables.get(i), posList.get(e));
                distCounter = getClassDistribution(classTables.get(i), posList.get(e), uniqueV);
                run.addBreak();
                run.setText(statNameList.get(e));
                run.addBreak();
                run.setText("_________________________________________________");
                run.addBreak();
                for (int k = 0; k < distCounter.size(); k++) {
                    run.setText(uniqueV.get(k) + ": " +  new DecimalFormat("#.00").format(distCounter.get(k))+ "%");
                    run.addBreak();
                }
            }

        }
        FileOutputStream out = new FileOutputStream(newFile);
        wordDoc.write(out);
        out.close();
        System.out.println(newFile.getName()+" written successfully");
        dialogueCreator.createInfoDialogue(false, "", newFile.getName()+" Saved Successfully");

//        Alert alert = new Alert(Alert.AlertType.CONFIRMATION.INFORMATION);
//        alert.setTitle("Information Dialog");
//        alert.setHeaderText(null);
//        alert.setContentText(newFile.getName()+" Saved Successfully");
//
//        alert.showAndWait();
    }


    //USER MANUAL
    @FXML
    private void viewManual() {
        try {
            String inputPdf = "res/KPMG SAS User Manual.pdf";
            Path tempOutput = Files.createTempFile("TempManual", ".pdf");
            tempOutput.toFile().deleteOnExit();
            try (InputStream is = IEProjectMain.class.getClassLoader().getResourceAsStream(inputPdf)) {
                Files.copy(is, tempOutput, StandardCopyOption.REPLACE_EXISTING);
            }
            Desktop.getDesktop().open(tempOutput.toFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
