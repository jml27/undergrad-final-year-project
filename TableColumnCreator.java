package IE_Project;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.util.Callback;
import javafx.util.converter.DefaultStringConverter;

import java.util.ArrayList;

/**
 * Created on 2016/09/13 @  10:48 AM.
 *
 * @author Kevin Gouws
 */
public class TableColumnCreator {
    private TableView businessUnitTable;

    public TableColumnCreator() {
        businessUnitTable = new TableView<>();
    }

    private void prepareColumn(TableColumn<ObservableList<StringProperty>, String> column, final int columnIndex,
                               String columnTitle) {
        column.setText(columnTitle);
        column.setCellValueFactory(cellDataFeatures -> {
                    ObservableList<StringProperty> values = cellDataFeatures.getValue();
                    if (columnIndex >= values.size()) {
                        return new SimpleStringProperty("");
                    } else {
                        ComboBox combo = new ComboBox();
                        combo.setItems(values);
                        return cellDataFeatures.getValue().get(columnIndex);

                    }
                });
    }

    public TableColumn<ObservableList<StringProperty>, String> createColumn(
            final int columnIndex, String columnTitle)   {
        Callback<TableColumn<ObservableList<StringProperty>, String>, TableCell<ObservableList<StringProperty>, String>> cellFactory = (TableColumn<ObservableList<StringProperty>, String> p) -> new EditableCell();
        TableColumn<ObservableList<StringProperty>, String> column = new TableColumn<>();
        String title;
        if (columnTitle == null || columnTitle.trim().length() == 0) {
            title = "Column " + (columnIndex + 1);
        } else {
            title = columnTitle;
        }
        prepareColumn(column, columnIndex, title);
//        column.setText(title);
//        column
//                .setCellValueFactory(cellDataFeatures -> {
//                    ObservableList<StringProperty> values = cellDataFeatures.getValue();
//                    if (columnIndex >= values.size()) {
//                        return new SimpleStringProperty("");
//                    } else {
//                        ComboBox combo = new ComboBox();
//                        combo.setItems(values);
//                        return cellDataFeatures.getValue().get(columnIndex);
//
//                    }
//                });
        if (title.equals("Notes")) {
            ArrayList<String> notesOptions = new ArrayList<>();
            notesOptions.add("");
            notesOptions.add("AA Clerk");
            notesOptions.add("Thuthuka");
            notesOptions.add("Top 10");
            notesOptions.add("Sasol");
            notesOptions.add("Other");
            //FXMLLoadScreenController loadScreen = new FXMLLoadScreenController();
            ArrayList<String> businessUnitExclusions = determineBUExclusions();

            for(String exclusion: businessUnitExclusions)
                notesOptions.add(exclusion);

            String[] completeComboOptions = new String[notesOptions.size()];

            for(int i = 0; i < completeComboOptions.length; i++)
                completeComboOptions[i] = notesOptions.get(i);
            column.setCellFactory(ComboBoxTableCell.forTableColumn(completeComboOptions));
            //column.setCellFactory(ComboBoxTableCell.forTableColumn("AA Clerk","Thuthuka", "Top 10","Sasol","Other"));
        } else {
            column.setCellFactory(cellFactory);
        }
        return column;
    }

    public TableColumn<ObservableList<StringProperty>, String> createColumn(
            final int columnIndex, String columnTitle, String[] choicePriorities)   {

        TableColumn<ObservableList<StringProperty>, String> column = new TableColumn<>();
        String title;
        if (columnTitle == null || columnTitle.trim().length() == 0) {
            title = "Column " + (columnIndex + 1);
        } else {
            title = columnTitle;
        }
        prepareColumn(column, columnIndex, title);
//        column.setText(title);
//        column
//                .setCellValueFactory(cellDataFeatures -> {
//                    ObservableList<StringProperty> values = cellDataFeatures.getValue();
//                    if (columnIndex >= values.size()) {
//                        return new SimpleStringProperty("");
//                    } else {
//                        ComboBox combo = new ComboBox();
//                        combo.setItems(values);
//                        return cellDataFeatures.getValue().get(columnIndex);
//
//                    }
//                });

        column.setCellFactory(ComboBoxTableCell.forTableColumn(new DefaultStringConverter(),choicePriorities));


        return column;
    }

    public void getBusinessUnits(TableView buTable){
        businessUnitTable = buTable;
    }

    private ArrayList<String> determineBUExclusions(){
        ArrayList<String> returnArray = new ArrayList<>();
        if(businessUnitTable.getItems().size() > 0){
            for(int i = 0; i < businessUnitTable.getItems().size();i++){
                returnArray.add("PUT "+ businessUnitTable.getVisibleLeafColumn(0).getCellObservableValue(i).getValue().toString().trim());
            }
            for(int i = 0; i < businessUnitTable.getItems().size();i++){
                returnArray.add("NOT "+ businessUnitTable.getVisibleLeafColumn(0).getCellObservableValue(i).getValue().toString().trim());
            }
        }
        return returnArray;
    }
}
