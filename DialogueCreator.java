package IE_Project;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.util.Optional;

/**
 * Created on 2016/09/13 @  11:01 AM.
 *
 * @author Kevin Gouws
 */
public class DialogueCreator {
    private Image kpmgIcon;

    public DialogueCreator(Image icon) {
        kpmgIcon = icon;
    }

    public void createErrorDialogue(String errorHeader, String errorContent) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText("Error - " + errorHeader);
        alert.setHeaderText(alert.getHeaderText().toUpperCase());
        alert.setContentText(errorContent);
        ((Stage)alert.getDialogPane().getScene().getWindow()).getIcons().add(kpmgIcon);
        alert.showAndWait();
    }

    public void createInfoDialogue(boolean useHeader, String infoHeader, String infoContent) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information");
        if (useHeader) alert.setHeaderText(infoHeader);
        else alert.setHeaderText(null);
        alert.setContentText(infoContent);
        ((Stage)alert.getDialogPane().getScene().getWindow()).getIcons().add(kpmgIcon);
        alert.showAndWait();
    }

    public Optional<ButtonType> createConfirmDialogue(String conHeader, String conContent) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation");
        alert.setHeaderText(conHeader);
        alert.setContentText(conContent);
        ((Stage)alert.getDialogPane().getScene().getWindow()).getIcons().add(kpmgIcon);
        return alert.showAndWait();
    }

    public Optional<ButtonType> createCustomConfirmDialogue(String conHeader, String conContent,
                                                                   ButtonType btnOne, ButtonType btnTwo) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation");
        alert.setHeaderText(conHeader);
        alert.setContentText(conContent);
        ((Stage)alert.getDialogPane().getScene().getWindow()).getIcons().add(kpmgIcon);
        ButtonType buttonCancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
        alert.getButtonTypes().setAll(btnOne, btnTwo, buttonCancel);
        return alert.showAndWait();
    }

    public void createWarningDialogue(String warnHeader, String warnContent) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Warning");
        alert.setHeaderText(warnHeader);
        alert.setContentText(warnContent);
        ((Stage)alert.getDialogPane().getScene().getWindow()).getIcons().add(kpmgIcon);
        alert.showAndWait();
    }

}
