package IE_Project;

import javafx.animation.Animation;
import javafx.animation.Transition;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.util.Duration;

/**
 * Created on 2016/08/08 @  11:22 AM.
 *
 * @author Kevin Gouws
 */
class SlideBarController extends VBox {
    private double expandedSize;
    private Pos flapbarLocation;

    // Create an animation to hide the panel.
    private final Animation hidePanelAnim = new Transition() {
        { setCycleDuration(Duration.millis(1)); }

        @Override
        protected void interpolate(double frac) {
            final double size = getExpandedSize() * (1.0 - frac);
            setVisible(false);
            translateByPos(size);
        }
    };

    // Create an animation to show the panel.
    private final Animation showPanelAnim = new Transition() {
        { setCycleDuration(Duration.millis(1)); }

        @Override
        protected void interpolate(double frac) {
            final double size = getExpandedSize() * frac;
            translateByPos(size);
            setVisible(true);
        }
    };

    /**
     * Creates a side bar panel in a BorderPane, containing an horizontal alignment
     * of the given nodes.
     *
     * @param expandedSize The size of the panel.
     * @param controlButton The button responsible to open/close slide bar.
     * @param location The location of the panel (TOP_LEFT, BOTTOM_LEFT, BASELINE_RIGHT, BASELINE_LEFT).
     * @param nodes Nodes inside the panel.
     */
    SlideBarController(double expandedSize, final Button controlButton, Pos location, Node... nodes) {
        setExpandedSize(expandedSize);
        setVisible(false);

        // Set location
        if (location == null) {
            flapbarLocation = Pos.TOP_CENTER; // Set default location
        }
        flapbarLocation = location;

        initPosition();

        // Add nodes in the Vbox
        getChildren().addAll(nodes);

        controlButton.setOnAction((ActionEvent actionEvent) -> {
            if (showPanelAnim.statusProperty().get() == Animation.Status.STOPPED
                    && hidePanelAnim.statusProperty().get() == Animation.Status.STOPPED) {
                if (isVisible()) {
                    hidePanelAnim.play();
                } else {
                    setVisible(true);
                    showPanelAnim.play();
                }
            }
        });
    }

    /**
     * Initialize position orientation.
     */
    private void initPosition() {
        switch (flapbarLocation) {
            case TOP_LEFT:
                setPrefHeight(0);
                setMinHeight(0);
                break;
            case BOTTOM_LEFT:
                setPrefHeight(0);
                setMinHeight(0);
                break;
            case BASELINE_RIGHT:
                setPrefWidth(0);
                setMinWidth(0);
                break;
            case BASELINE_LEFT:
                setPrefWidth(0);
                setMinWidth(0);
                break;
        }
    }

    /**
     * Translate the VBox according to location Pos.
     */
    private void translateByPos(double size) {
        switch (flapbarLocation) {
            case TOP_LEFT:
                setPrefHeight(size);
                setTranslateY(-getExpandedSize() + size);
                break;
            case BOTTOM_LEFT:
                setPrefHeight(size);
                break;
            case BASELINE_RIGHT:
                setPrefWidth(size);
                break;
            case BASELINE_LEFT:
                setPrefWidth(size);
                break;
        }
    }

    public void displayPanel() {
        if (showPanelAnim.statusProperty().get() == Animation.Status.STOPPED
                && hidePanelAnim.statusProperty().get() == Animation.Status.STOPPED) {
            if (!isVisible()) {
                setVisible(true);
                showPanelAnim.play();
            }
        }
    }

    public void hidePanel() {
        if (showPanelAnim.statusProperty().get() == Animation.Status.STOPPED
                && hidePanelAnim.statusProperty().get() == Animation.Status.STOPPED) {
            if (isVisible())
                hidePanelAnim.play();
        }
    }

    /**
     * @return the expandedSize
     */
    private double getExpandedSize() {
        return expandedSize;
    }

    /**
     * @param expandedSize the expandedSize to set
     */
    private void setExpandedSize(double expandedSize) {
        this.expandedSize = expandedSize;
    }
}
