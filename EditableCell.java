/*
 * TEAM 3 INDUSTRIAL EXPERIENCE PROJECT
 * KPMG Student Allocation System Created by Monash University South Africa's IE Team 3
 * 2016 (c) KPMG & Monash University
 */
package IE_Project;

import javafx.application.Platform;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import java.util.ArrayList;
import java.util.List;


/**
 * Created on 2016/07/28 @  11:20 AM.
 *
 * @author Kevin Gouws
 */
class EditableCell extends TableCell<ObservableList<StringProperty>, String> {

    private TextField textField;
    private TableColumn firstColumn;

    @Override
    public void startEdit() {
        if (!isEmpty()) {
            super.startEdit();
            createTextField();
            setText(null);
            setGraphic(textField);
            Platform.runLater(() -> textField.requestFocus());
        }
    }

    @Override
    public void cancelEdit() {
        super.cancelEdit();

        setText(getItem());
        setGraphic(null);
    }

    @Override
    public void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);

        if (empty) {
            setText(null);
            setGraphic(null);
        } else if (isEditing()) {
            if (textField != null) {
                textField.setText(getString());
            }
            setText(null);
            setGraphic(textField);
        } else {
            setText(getString());
            setGraphic(null);
        }
    }

    private String getString() {
        return getItem() == null ? "" : getItem();
    }

    private void createTextField() {
        firstColumn = getTableView().getColumns().get(0);
        textField = new TextField(getString());
        textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
        textField.focusedProperty().addListener(
                (ObservableValue<? extends Boolean> arg0,
                 Boolean arg1, Boolean changeListener) -> {
                    if (!changeListener) {
                        commitEdit(textField.getText());
                    }
                }
        );
        textField.setOnMousePressed(m -> textField.selectAll());
        textField.setOnKeyPressed(t -> {
            if (t.getCode() == KeyCode.ESCAPE) {
                System.out.println("Key Pressed: " + t.getCode() + " - CELL EDIT CANCELLED");
                cancelEdit();
                t.consume();
            } else if (t.getCode() == KeyCode.TAB) {
                System.out.println("Key Pressed: " + t.getCode() + " - CELL EDIT COMMITTED");
                commitEdit(textField.getText());
                enterNextColumn(t);
                t.consume();
            } else if (t.getCode() == KeyCode.ENTER) {
                System.out.println("Key Pressed: " + t.getCode() + " - CELL EDIT COMMITTED");
                commitEdit(textField.getText());
                enterNextRow();
                t.consume();
            }
        });
    }

    private void enterNextColumn(KeyEvent keyEvent) {
        TableColumn currentColumn = getTableColumn();
        int totalColumns = getTableView().getColumns().size();
        int currentColumnIndex = (getTableView().getColumns().indexOf(currentColumn)+1);
        int currentRow = getTableRow().getIndex();
        int nextRow = (getTableRow().getIndex()+1);
        if (!((nextRow == getTableView().getItems().size()) &&
                (currentColumnIndex == getTableView().getColumns().size()))) {
            if (currentColumnIndex == totalColumns) {
                getTableView().edit(nextRow, firstColumn);
                getTableView().scrollToColumn(firstColumn);
                getTableView().getSelectionModel().select(nextRow);
                System.out.println("Going to CELL { ROW : " + (nextRow+1) + ", COLUMN : " + firstColumn.getText() + " }");
            } else {
                TableColumn nextColumn = getNextColumn(!keyEvent.isShiftDown());
                if (nextColumn != null) {
                    getTableView().edit(currentRow, nextColumn);
                    getTableView().scrollToColumn(nextColumn);
                    System.out.println("Going to CELL { ROW : " + (currentRow+1) + ", COLUMN : " + nextColumn.getText() + " }");
                }
            }
        }
    }

    private void enterNextRow() {
        int totalRows;
        TableColumn currentColumn = getTableColumn();
        int nextRow = (getTableRow().getIndex()+1);
        totalRows = getTableView().getItems().size();
        if (!(nextRow == totalRows)) {
            getTableView().edit(nextRow, currentColumn);
            getTableView().getSelectionModel().select(nextRow);
            getTableView().scrollTo(nextRow);
            System.out.println("Going to CELL { ROW : " + (nextRow+1) + ", COLUMN : " + currentColumn.getText() + " }");
        }
    }

    private TableColumn<ObservableList<StringProperty>, ?> getNextColumn(boolean forward) {
        List<TableColumn<ObservableList<StringProperty>, ?>> columns = new ArrayList<>();
        for (TableColumn<ObservableList<StringProperty>, ?> column : getTableView().getColumns()) {
            columns.addAll(getLeaves(column));
        }
        // There is no other column that supports editing.
        if (columns.size() < 2) {
            return null;
        }
        int nextIndex = columns.indexOf(getTableColumn());
        if (forward) {
            nextIndex++;
            if (nextIndex > columns.size() - 1) {
                nextIndex = 0;
            }
        } else {
            nextIndex--;
            if (nextIndex < 0) {
                nextIndex = columns.size() - 1;
            }
        }
        return columns.get(nextIndex);
    }

    private List<TableColumn<ObservableList<StringProperty>, ?>> getLeaves(
            TableColumn<ObservableList<StringProperty>, ?> root) {
        List<TableColumn<ObservableList<StringProperty>, ?>> columns = new ArrayList<>();
        if (root.getColumns().isEmpty()) {
            // We only want the leaves that are editable.
            if (root.isEditable()) {
                columns.add(root);
            }
            return columns;
        } else {
            for (TableColumn<ObservableList<StringProperty>, ?> column : root.getColumns()) {
                columns.addAll(getLeaves(column));
            }
            return columns;
        }
    }
}
