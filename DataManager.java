package IE_Project;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.stage.FileChooser;
import javafx.stage.Window;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Created on 9/7/2016 @ 07:05 PM.
 *
 * @author Kevin Gouws
 */
public class DataManager {
    boolean isProjectReady = false;
    private DialogueCreator dialogueCreator;
    private TableColumnCreator tableColumnCreator;
    private File projectFile, currentlyLoadedFile, saveFile;

    public DataManager() {
        tableColumnCreator = new TableColumnCreator();
        projectFile = currentlyLoadedFile = saveFile = null;
        dialogueCreator = new DialogueCreator(new Image(getClass().getResourceAsStream("kpmg_icon.png")));
    }

    public DataManager(File projectFile) {
        this.projectFile = projectFile;
        tableColumnCreator = new TableColumnCreator();
        dialogueCreator = new DialogueCreator(new Image(getClass().getResourceAsStream("kpmg_icon.png")));
    }

    private String removeUnwantedCommas(String readLine){
        String line = readLine;
        String errorIndicator = "\"";
        int firstOccurrenceIndex, secondOccurrenceIndex;
        try {
            String firstHalf, secondHalf;
            while (line.contains(errorIndicator)) {
                firstOccurrenceIndex = line.indexOf(errorIndicator);
                firstHalf = line.substring(0,firstOccurrenceIndex);
                line = line.substring(firstHalf.length() + 1,line.length());
                secondOccurrenceIndex = line.indexOf(errorIndicator);
                secondHalf = line.substring(0,secondOccurrenceIndex);
                line = line.substring(secondHalf.length() + 1,line.length());
                if (secondHalf.contains(",")) {
                    List<String> tokens = tokenize(secondHalf,",");
                    secondHalf = "";
                    for (String token : tokens)
                        secondHalf += token;
                }
                line = firstHalf + secondHalf + line;
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return line;
    }

    private List<String> tokenize(String line, String delimiter){
        List<String> list = new ArrayList<>();

        String token;

        while(line.contains(delimiter)){
            token = line.substring(0,line.indexOf(delimiter));
            line = line.substring(token.length()+delimiter.length(),line.length());
            list.add(token);
        }
        list.add(line);

        return list;
    }

    private boolean validateProject(File theFile) {
        if (theFile == null) {
            dialogueCreator.createErrorDialogue("No Project Selected", "You didn't select a project file.\n" +
                    "\nTips:" +
                    "\n� Make sure that the project is saved as a ALLOC file" +
                    "\n� You might've accidentally clicked the \"Cancel\" button");
            return false;
        } else if (theFile.length() == 0) {
            dialogueCreator.createErrorDialogue("Empty Project Selected", "The project you selected contains no " +
                    "data.\n" +
                    "\n� Project Name\t: " + theFile.getName() +
                    "\n� Project Directory\t: " + theFile.getAbsolutePath());
            return false;
        } else {
            return true;
        }
    }

    private boolean validateFile(File theFile, TableView theTable, boolean isDataLoaded) {
        if (theFile == null) {
            if (isDataLoaded) {
                // The User perhaps cancelled their file selection choice, so the old data is being retained.
                System.out.println("[Data File Selection Cancelled - retaining old data]");
                return false;
            } else {
                loadDataError("No File Selected", "It seems that you did not select a file.\n" +
                        "\nTips:" +
                        "\n� Make sure that the data file is saved as a CSV file, " +
                        "or that the project is saved as an ALLOC file" +
                        "\n� You might've accidentally clicked the \"Cancel\" button", theTable);
                return false;
            }
        } else if (theFile.length() == 0) {
            if (isDataLoaded) {
                System.out.println("[Newly Selected Data File is Empty - retaining old data]");
                dialogueCreator.createInfoDialogue(true, "Whoops!", "The file you selected (" +
                        theFile.getName() + ") is empty, or contains no readable data; " +
                        "Therefore the old data is being retained.");
                return false;
            } else {
                loadDataError("Selected File is Empty", "The file you have selected appears to be empty.\n" +
                        "\nFile Name: " + theFile.getName() +
                        "\nFile Path\t: " + theFile.getAbsolutePath(), theTable);
                return false;
            }
        } else if (theFile.equals(currentlyLoadedFile)) {
            dialogueCreator.createErrorDialogue("File Already Loaded", "The file you selected " +
                    "(" + theFile.getName() + ") has already been loaded into the application.");
            return false;
        } else {
            return true;
        }
    }

    private void loadDataError(String errorTitle, String errorMsg, TableView theTable) {
        String errorOutput = "[ERROR: " + errorTitle.toUpperCase() + "]";
        System.err.println(errorOutput);
        theTable.getItems().clear();
        theTable.getColumns().clear();
        theTable.setPlaceholder(new Label("No Data was Found."));
        dialogueCreator.createErrorDialogue(errorTitle, errorMsg);
    }

    private void readDataIntoTable(File theFile, TableView theTable, TextArea theOutput)
            throws IOException {
        // Setup a input stream connection to the selectedFile to prepare for reading it.
        URL url = new URL(theFile.toURI().toURL().toString());
        URLConnection connection = url.openConnection();
        InputStream in = connection.getInputStream();
        InputStreamReader isr = new InputStreamReader(in);
        BufferedReader bf = new BufferedReader(isr);

        // Begin reading the data into the application and setting up the TableView
        theOutput.setText("Selected File\t\t\t\t\t\t: " + theFile.getName());
        theTable.getItems().clear();
        theTable.getColumns().clear();
        String headerLine = bf.readLine();
        String[] tableHeaders = headerLine.split(",");
        System.out.println(Arrays.toString(tableHeaders));
        for (int column = 0; column < tableHeaders.length; column++) {
            theTable.getColumns().add(tableColumnCreator.createColumn(column, tableHeaders[column]));
        }

        String dataLine;
        while ((dataLine = bf.readLine()) != null) {
            dataLine = removeUnwantedCommas(dataLine);
            List<String> tokens = tokenize(dataLine,",");
            String[] dataValues = new String[tokens.size()];
            boolean isRowEmpty = true;

            for (String token : tokens) {
                if (token.length() > 0)
                    isRowEmpty = false;
            }

            if (isRowEmpty)
                continue;

            for (int i = 0; i < tokens.size(); i++)
                dataValues[i] = tokens.get(i);

            ObservableList<StringProperty> data = FXCollections.observableArrayList();
            for (String value : dataValues) {
                data.add(new SimpleStringProperty(value));
            }

            if (!data.isEmpty()) {
                theTable.getItems().add(data);
            }
        }
        bf.close();
        currentlyLoadedFile = theFile;
    }

    private void readProjectIntoTables(File theFile, ArrayList<TableView> theTables, TextArea theOutput)
            throws IOException {
        String dataLine;
        String[] dataValues;
        ObservableList<Object> studData, buData, critData;

        for (TableView table : theTables) {
            table.getItems().clear();
            table.getColumns().clear();
        }

        URL url = new URL(theFile.toURI().toURL().toString());
        URLConnection connection = url.openConnection();
        InputStream in = connection.getInputStream();
        InputStreamReader isr = new InputStreamReader(in);
        BufferedReader bf = new BufferedReader(isr);
        theOutput.setText("Selected Project\t\t\t\t\t: " + theFile.getName());
        String headerLine = bf.readLine();
        String[] headerValues = headerLine.split(",");
        System.out.println(Arrays.toString(headerValues));

        for (int column = 0; column < headerValues.length; column++) {
            theTables.get(0).getColumns().add(tableColumnCreator.createColumn(column, headerValues[column]));
        }

        while ((dataLine = bf.readLine()) != null) {
            if (dataLine.equals("[STUDENT DATA START]")) {
                // Student Population
                System.out.println("[READING PROJECT STUDENTS: STARTED]");
                while (!(dataLine = bf.readLine()).equals("")) {
                    System.out.println(dataLine);
                    dataLine = removeUnwantedCommas(dataLine);
                    studData = FXCollections.observableArrayList();
                    List<String> tokens = tokenize(dataLine, ",");
                    dataValues = new String[tokens.size()];
                    boolean isRowEmpty = true;

                    for (String token : tokens) {
                        if (token.length() > 0)
                            isRowEmpty = false;
                    }

                    if (isRowEmpty)
                        continue;

                    for (int i = 0; i < tokens.size(); i++)
                        dataValues[i] = tokens.get(i);


                    for (String value : dataValues) {
                        studData.add(new SimpleStringProperty(value));
                    }
                    if (!studData.isEmpty()) {
                        theTables.get(0).getItems().add(studData);
                    }
                }
                System.out.println("[READING PROJECT STUDENTS: COMPLETED]");
            }
            if (dataLine.equals("[BU DATA START]")) {
                // Business Units
                System.out.println("[READING PROJECT BUS UNITS: STARTED]");
                String headerLine1 = bf.readLine();
                String[] headerValues1 = headerLine1.split(",");

                for (int column = 0; column < headerValues1.length; column++) {
                    theTables.get(1).getColumns().add(tableColumnCreator.createColumn(column, headerValues1[column]));
                }

                while (!(dataLine = bf.readLine()).equals("")) {
                    System.out.println(dataLine);
                    dataLine = removeUnwantedCommas(dataLine);
                    buData = FXCollections.observableArrayList();
                    List<String> tokens = tokenize(dataLine, ",");
                    dataValues = new String[tokens.size()];
                    boolean isRowEmpty = true;

                    for (String token : tokens) {
                        if (token.length() > 0)
                            isRowEmpty = false;
                    }

                    if (isRowEmpty)
                        continue;

                    for (int i = 0; i < tokens.size(); i++)
                        dataValues[i] = tokens.get(i);

                    for (String value : dataValues) {
                        buData.add(new SimpleStringProperty(value));
                    }
                    if (!buData.isEmpty()) {
                        theTables.get(1).getItems().add(buData);
                    }
                }
                System.out.println("[READING PROJECT BUSUNITS: COMPLETED]");
            }
            if (dataLine.equals("[CRITERIA DATA START]")) {
                // Evaluation Criteria
                System.out.println("[READING PROJECT CRITERIA: STARTED]");
                String headerLine2 = bf.readLine();
                String[] headerValues2 = headerLine2.split(",");

                for (int column = 0; column < headerValues2.length; column++) {
                    theTables.get(2).getColumns().add(tableColumnCreator.createColumn(column, headerValues2[column]));
                }

                while (!(dataLine = bf.readLine()).equals("")) {
                    System.out.println(dataLine);
                    dataLine = removeUnwantedCommas(dataLine);
                    critData = FXCollections.observableArrayList();
                    List<String> tokens = tokenize(dataLine, ",");
                    dataValues = new String[tokens.size()];
                    boolean isRowEmpty = true;

                    for (String token : tokens) {
                        if (token.length() > 0)
                            isRowEmpty = false;
                    }

                    if (isRowEmpty)
                        continue;

                    for (int i = 0; i < tokens.size(); i++)
                        dataValues[i] = tokens.get(i);

                    for (String value : dataValues) {
                        critData.add(new SimpleStringProperty(value));
                    }

                    if (!critData.isEmpty()) {
                        theTables.get(2).getItems().add(critData);
                    }
                }
                System.out.println("[READING PROJECT CRITERIA: COMPLETED]");
            }
        }
        bf.close();
        currentlyLoadedFile = theFile;
    }

    private void exportData(File theFile, TableView theTable) throws IOException {
        // Variable declaration
        String studentHeaders, studentData;
        BufferedWriter fileWriter = new BufferedWriter(new FileWriter(theFile, false));
        for (int i = 0; i < theTable.getVisibleLeafColumns().size(); i++) {
            studentHeaders = theTable.getVisibleLeafColumn(i).getText();
            fileWriter.write(studentHeaders);
            fileWriter.write(",");
        }
        fileWriter.newLine();
        fileWriter.newLine();
        for (int i = 0; i < theTable.getItems().size(); i++) {
            for (int j = 0; j < theTable.getVisibleLeafColumns().size(); j++) {
                studentData = theTable.getVisibleLeafColumn(j).getCellObservableValue(i).getValue().toString();
                fileWriter.write(studentData);
                fileWriter.write(",");
            }
            fileWriter.newLine();
        }
        fileWriter.newLine();
        fileWriter.close();
    }

    private void saveProject(File saveFile, ArrayList<TableView> theTables)
            throws IOException {
        // Variable declaration
        String studentHeader, studentValues, busHeader, busValues, critHeader, critValues;
        BufferedWriter fileWriter = new BufferedWriter(new FileWriter(saveFile, false));

        // Student Table Columns
        for (int i = 0; i < theTables.get(0).getVisibleLeafColumns().size(); i++) {
            studentHeader = theTables.get(0).getVisibleLeafColumn(i).getText();
            fileWriter.write(studentHeader);
            fileWriter.write(",");
        }
        fileWriter.newLine();

        // Student Table Records
        fileWriter.write("[STUDENT DATA START]");
        fileWriter.newLine();
        for (int i = 0; i < theTables.get(0).getItems().size(); i++) {
            for (int j = 0; j < theTables.get(0).getVisibleLeafColumns().size(); j++) {
                studentValues = theTables.get(0).getVisibleLeafColumn(j).getCellObservableValue(i).getValue().toString();
                fileWriter.write(studentValues);
                fileWriter.write(",");
            }
            fileWriter.newLine();
        }
        fileWriter.newLine();


        // Business Unit Table
        if (!(theTables.get(1).getItems().isEmpty())) {
            fileWriter.write("[BU DATA START]");
            fileWriter.newLine();
            // Column Headers
            for (int i = 0; i < theTables.get(1).getVisibleLeafColumns().size(); i++) {
                busHeader = theTables.get(1).getVisibleLeafColumn(i).getText();
                fileWriter.write(busHeader);
                fileWriter.write(",");
            }
            fileWriter.newLine();

            // BU Table Records
            for(int i = 0; i < theTables.get(1).getItems().size(); i++)
            {
                for(int j = 0; j < theTables.get(1).getVisibleLeafColumns().size(); j++)
                {
                    busValues = theTables.get(1).getVisibleLeafColumn(j).getCellObservableValue(i).getValue().toString();
                    fileWriter.write(busValues);
                    fileWriter.write(",");
                }
                fileWriter.newLine();
            }
            fileWriter.newLine();
        }


        // Criteria Table
        if (!(theTables.get(2).getItems().isEmpty())) {
            fileWriter.write("[CRITERIA DATA START]");
            fileWriter.newLine();
            // Column Headers
            for (int i = 0; i < theTables.get(2).getVisibleLeafColumns().size(); i++) {
                critHeader = theTables.get(2).getVisibleLeafColumn(i).getText();
                fileWriter.write(critHeader);
                fileWriter.write(",");
            }
            fileWriter.newLine();

            // Criteria Table Records
            for(int i = 0; i < theTables.get(2).getItems().size(); i++)
            {
                for(int j = 0; j < theTables.get(2).getVisibleLeafColumns().size(); j++)
                {
                    critValues = (String) theTables.get(2).getVisibleLeafColumn(j).getCellObservableValue(i).getValue();
                    if (critValues == null) critValues = " ";
                    fileWriter.write(critValues);
                    fileWriter.write(",");
                }
                fileWriter.newLine();
            }
            fileWriter.newLine();
        }
        fileWriter.write("[END OF DATA]");
        fileWriter.close();
        currentlyLoadedFile = saveFile;
    }

    boolean loadData(boolean dataLoaded, TableView theTable, TextArea theOutput, Window theWindow) {
        if (theTable.getItems().isEmpty())
            theTable.setPlaceholder(new Label("Loading New Data..."));

        // Setup and initialization of File Selection Dialog Box
        FileChooser fileChooser = new FileChooser();
        File selectedFile;
        fileChooser.setTitle("Load Student Data File");
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("CSV Files", "*.csv"));
        selectedFile = fileChooser.showOpenDialog(theWindow);

        // Exception handling is done because the readData method throws an IOException
        try {
            // Validate that the file is suitable for use
            if (validateFile(selectedFile, theTable, dataLoaded)) {
                if (dataLoaded) {
                    // Ask the user if they're sure about loading new data, and remind them to save
                    Optional<ButtonType> userChoice = dialogueCreator.createConfirmDialogue("You're about to " +
                            "load a new or different data file", "Any unsaved changes you've made to this " +
                            "data file will be lost.\n\nAre you sure?");
                    if (userChoice.get() == ButtonType.OK) {
                        // User chose OK
                        System.out.println("[USER CHOSE TO LOAD NEW DATA]");
                        readDataIntoTable(selectedFile, theTable, theOutput);
                        return true;
                    }
                } else {
                    // No data has been loaded yet, and can we can thus proceed as usual
                    readDataIntoTable(selectedFile, theTable, theOutput);
                    return true;
                }
            } else {
                return false;
            }
        } catch (IOException e) {
            System.err.println("[SOMETHING BAD HAPPENED WHEN LOADING THE FILE!]");
            e.printStackTrace();
        }
        return false;
    }

    boolean loadProject(File theFile, ArrayList<TableView> theTables, TextArea theOutput) {
        try {
            readProjectIntoTables(theFile, theTables, theOutput);
            return true;
        } catch (IOException e) {
            System.err.println("[SOMETHING BAD HAPPENED WHEN LOADING THE PROJECT FROM THE MAIN MENU!]");
            e.printStackTrace();
        }
        return false;
    }

    boolean loadProject(boolean dataLoaded, ArrayList<TableView> theTables, TextArea theOutput,
                               Window theWindow) {
        // Variable Setup
        File selectedFile;
        FileChooser fileChooser = new FileChooser();

        // Make the TableViews indicate that data is being loaded if it's empty
        for (TableView table : theTables) {
            table.setPlaceholder(new Label("Loading Project Data..."));
        }

        // Choose a project File using a Dialog Box
        fileChooser.setTitle("Open an Existing Project");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("ALLOC Files", "*.alloc"));
        fileChooser.setInitialDirectory(IEProjectMain.projectsDirectory);
        selectedFile = fileChooser.showOpenDialog(theWindow);

        // Validate that the file is suitable for use
        try {
            if (validateFile(selectedFile, theTables.get(0), dataLoaded)) {
                if (dataLoaded) {
                    // Ask the user if they're sure about loading different project, and remind them to save
                    Optional<ButtonType> userChoice = dialogueCreator.createConfirmDialogue("You're about to " +
                            "load different project", "Any unsaved changes you've made to this " +
                            "project will be lost.\n\nAre you sure?");
                    if (userChoice.get() == ButtonType.OK) {
                        // User chose OK
                        System.out.println("[USER CHOSE TO LOAD NEW PROJECT]");
                        readProjectIntoTables(selectedFile, theTables, theOutput);
                        return true;
                    }
                } else {
                    // No data has been loaded yet, and can we can thus proceed as usual
                    readProjectIntoTables(selectedFile, theTables, theOutput);
                    return true;
                }
            } else {
                return false;
            }
        } catch (IOException e) {
            System.err.println("[SOMETHING BAD HAPPENED WHEN LOADING THE PROJECT!]");
            e.printStackTrace();
        }
        return false;
    }

    void exportData(TableView theTable, TextArea theOutput, Window theWindow) {
        // Variable Setup
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Export the Student Data");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("CSV Files", "*.csv"));

        try {
            if (theTable.getColumns().isEmpty()) {
                // No data file is presently loaded
                dialogueCreator.createErrorDialogue("No Data Has Been Loaded", "The save process has been cancelled " +
                        "as no student data has been loaded into the application yet.");
            } else if (theTable.getItems().isEmpty()) {
                // Student table has no student records
                dialogueCreator.createErrorDialogue("No Student Records Found", "It appears that there are no student " +
                        "records which can been saved. Please ensure that at least 1 student record has been defined " +
                        "before attempting to save the project.");
            } else {
                File theFile = fileChooser.showSaveDialog(theWindow);
                if (theFile != null) {
                    exportData(theFile, theTable);
                    theOutput.appendText("\nStudent Data has been exported to " + theFile.getName() + ".");
                }
            }
        } catch (IOException e) {
            System.err.println("[SOMETHING BAD HAPPENED WHEN EXPORTING THE STUDENT DATA!]");
            e.printStackTrace();
        }
    }

    boolean saveProject(boolean hasBeenSaved, ArrayList<TableView> theTables,
                               TextArea theOutput, Window theWindow) {
        // Variable Setup
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save the Current Project");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("ALLOC Files", "*.alloc"));
        fileChooser.setInitialDirectory(IEProjectMain.projectsDirectory);

        try {
            if (theTables.get(0).getColumns().isEmpty()) {
                // No data file is presently loaded
                dialogueCreator.createErrorDialogue("No Data Has Been Loaded", "The save process has been cancelled " +
                        "as no student data has been loaded into the application yet.");
                return false;
            } else if (theTables.get(0).getItems().isEmpty()) {
                // Student table has no student records
                dialogueCreator.createErrorDialogue("No Student Records Found", "It appears that there are no student " +
                        "records which can been saved. Please ensure that at least 1 student record has been defined " +
                        "before attempting to save the project.");
                return false;
            } else {
                if (hasBeenSaved) {
                    saveProject(saveFile, theTables);
                    theOutput.appendText("\nChanges to " + saveFile.getName() + " have been saved.");
                    return true;
                } else {
                    saveFile = fileChooser.showSaveDialog(theWindow);
                    if (saveFile == null) {
                        return false;
                    } else {
                        saveProject(saveFile, theTables);
                        theOutput.appendText("\nProject " + saveFile.getName() + " has been saved.");
                        return true;
                    }
                }
            }
        } catch (IOException e) {
            System.err.println("[SOMETHING BAD HAPPENED WHEN SAVING THE PROJECT!]");
            e.printStackTrace();
        }
        return false;
    }

    boolean setProject(Window window) throws IOException {
        // Revise this method
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select a Allocation Project File");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("ALLOC Files", "*.alloc"));
        fileChooser.setInitialDirectory(IEProjectMain.projectsDirectory);
        File selectedFile = fileChooser.showOpenDialog(window);
        if (validateProject(selectedFile)) {
            projectFile = selectedFile;
            saveFile = projectFile;
            isProjectReady = true;
            return true;
        } else {
            return false;
        }
    }

    File getProject() {
        return projectFile;
    }
}
