/*
 * TEAM 3 INDUSTRIAL EXPERIENCE PROJECT
 * KPMG Student Allocation System Created by Monash University South Africa's IE Team 3
 * 2016 (c) KPMG & Monash University
 */
package IE_Project;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.File;

/**
 * @author TEAM 3
 */
public class IEProjectMain extends Application {

    static File projectsDirectory;
    static DataManager projectManager;
    static String loadScreen = "loadScreen";
    static String loadScreenFile = "FXMLLoadScreen.fxml";
    static String resultsScreen = "resultsScreen";
    static String resultsScreenFile = "FXMLResultsScreen.fxml";
    static StageController mainContainer = new StageController();
    private static String welcomeScreen = "welcomeScreen";
    private static String welcomeScreenFile = "FXMLWelcomeScreen.fxml";
    
    @Override
    public void start(Stage mainStage) throws Exception {
        projectManager = new DataManager();
        mainContainer.loadStage(welcomeScreen, welcomeScreenFile);
        mainContainer.setStage(IEProjectMain.welcomeScreen);
        mainContainer.setStyle("-fx-background-color: #E6F0FF");

        Group rootStackPane = new Group();
        rootStackPane.getChildren().addAll(mainContainer);
        Scene scene = new Scene(rootStackPane);
        mainStage = new Stage(StageStyle.DECORATED);
        mainStage.setScene(scene);
        mainStage.getIcons().add(new Image(getClass().getResourceAsStream("kpmg_icon.png")));
        mainStage.setMaximized(true);
        mainStage.setTitle("KPMG Student Allocation System");
        mainStage.show();

        projectsDirectory = new File("Student BU Allocation Projects");
        if (projectsDirectory.mkdir()) {
            // Create the Directory if it doesn't exist
            System.out.println("[Creating the Projects Directory: \"" + projectsDirectory.getAbsolutePath() + "\"]");
        } else {
            // Else use the one that has previously been created
            System.out.println("[Using the existing Projects Directory: \"" + projectsDirectory.getAbsolutePath() + "\"]");
        }
        
        mainContainer.prefWidthProperty().bind(scene.widthProperty());
        mainContainer.prefHeightProperty().bind(scene.heightProperty());
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
